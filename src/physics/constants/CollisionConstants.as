/**
 * Created by eVillain on 03/10/14.
 */
package physics.constants
{

	public class CollisionConstants
	{
		public static const COLLISION_TYPE_DEFAULT:String = "COLLISION_DEFAULT";
		public static const COLLISION_TYPE_PLAYER:String = "COLLISION_TYPE_PLAYER";
		public static const COLLISION_TYPE_ITEM:String = "COLLISION_TYPE_ITEM";
		public static const COLLISION_TYPE_BULLET:String = "COLLISION_TYPE_BULLET";
		public static const COLLISION_TYPE_NPC:String = "COLLISION_TYPE_NPC";

		public static const COLLISION_MASK_NO_COLLISIONS:int = 0;
		public static const COLLISION_MASK_ALL_COLLISIONS:int = 1;
		public static const COLLISION_MASK_PLAYER1:int = (1 << 1); 		//2
		public static const COLLISION_MASK_PLAYER2:int = (1 << 2); 		//4
		public static const COLLISION_MASK_ITEM:int = (1 << 3);			//8
		public static const COLLISION_MASK_BULLET:int = (1 << 4);	//16
		public static const COLLISION_MASK_NPC:int = (1 << 5);			//32

	}
}
