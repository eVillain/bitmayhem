/**
 * Created by eVillain on 31/08/14.
 */
package physics.constants
{

	public class PhysicsConstants
	{
		/// The body type - mimics Nape BodyType types.
		/// static: zero mass, zero velocity, may be manually moved
		/// kinematic: zero mass, non-zero velocity set by user, moved by solver
		/// dynamic: positive mass, non-zero velocity determined by forces, moved by solver
		static public const STATIC:uint = 0; 		// BodyType.STATIC;
		static public const KINEMATIC:uint = 1;		// BodyType.KINEMATIC;
		static public const DYNAMIC:uint = 2; 		// BodyType.DYNAMIC;
	}
}
