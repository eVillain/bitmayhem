/**
 * Created by eVillain on 31/08/14.
 */
package physics.vos
{

	import flash.geom.Point;

	import physics.constants.CollisionConstants;
	import physics.constants.PhysicsConstants;

	public class SpawnPhysicsBoxVO
	{
		public var center:Point;
		public var width:Number;
		public var height:Number;
		public var rotation:Number;

		public var velocity:Point;
		public var angularVelocity:Number;

		public var physicsType:uint;
		public var collisionType:String;

		public var userData:*;

		public var density:Number;
		public var friction:Number;
		public var elasticity:Number;

		public function SpawnPhysicsBoxVO( center:Point, width:Number, height:Number, rotation:Number, velocity:Point, angularVel:Number = 0.0, physicsType:uint = PhysicsConstants.DYNAMIC, collisionType:String = CollisionConstants.COLLISION_TYPE_DEFAULT, userData:* = null, density:Number = 1.0, friction:Number = 0.5, elasticity:Number = 0.5 )
		{
			this.center = center;
			this.width = width;
			this.height = height;
			this.rotation = rotation;

			this.velocity = velocity;
			this.angularVelocity = angularVel;

			this.physicsType = physicsType;
			this.collisionType = collisionType;

			this.userData = userData;

			this.density = density;
			this.friction = friction;
			this.elasticity = elasticity;
		}
	}
}
