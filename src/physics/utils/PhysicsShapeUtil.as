/**
 * Created by eVillain on 12/03/15.
 */
package physics.utils
{

	import flash.geom.Point;

	import nape.geom.Vec2;
	import nape.phys.Body;
	import nape.shape.Circle;
	import nape.shape.Polygon;
	import nape.space.Space;

	import physics.constants.CollisionConstants;
	import physics.constants.PhysicsConstants;

	public class PhysicsShapeUtil
	{
		public static function createBox( space:Space, center:Point, width:Number, height:Number, rotation:Number = 0.0, velocity:Point = null, angularVel:Number = 0.0, physicsType:uint = PhysicsConstants.DYNAMIC, collisionType:String = CollisionConstants.COLLISION_TYPE_DEFAULT, userData:* = null, density:Number = 1.0, friction:Number = 1.0, elasticity:Number = 0.0 ):Body
		{
			var box:Body = new Body( PhysicsUtil.getBodyTypeForPhysicsType( physicsType ), new Vec2( center.x, center.y ) );
			box.shapes.add( new Polygon( Polygon.box( width, height ) ) );

			if (velocity)
			{
				box.velocity = new Vec2( velocity.x, velocity.y );
			}

			box.rotation = rotation;
			box.angularVel = angularVel;
			box.userData.data = userData;

			box.shapes.at( 0 ).material.density = density;
			box.shapes.at( 0 ).material.staticFriction = friction;
			box.shapes.at( 0 ).material.dynamicFriction = friction;
			box.shapes.at( 0 ).material.elasticity = elasticity;

			box.space = space;
			box.cbTypes.add( PhysicsUtil.collisionForType( collisionType ) );

			return box;
		}

		public function createCircle( space:Space, center:Point, radius:Number, physicsType:uint = PhysicsConstants.DYNAMIC, userData:* = null, collisionType:String = CollisionConstants.COLLISION_TYPE_DEFAULT, mass:Number = 0, friction:Number = 1.0, elasticity:Number = 0.0, angle:Number = 0.0 ):Body
		{
			var ball:Body = new Body( PhysicsUtil.getBodyTypeForPhysicsType( physicsType ), new Vec2( center.x, center.y ) );
			ball.shapes.add( new Circle( radius ) );
			ball.shapes.at( 0 ).material.elasticity = elasticity;
			ball.shapes.at( 0 ).material.staticFriction = friction;
			ball.shapes.at( 0 ).material.dynamicFriction = friction;
			ball.mass = mass;
			ball.angularVel = 0;
			ball.rotation = angle;
			ball.space = space;
			ball.userData.data = userData;
			ball.cbTypes.add( PhysicsUtil.collisionForType( collisionType ) );
			return ball;
		}
	}
}
