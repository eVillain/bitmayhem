/**
 * Created by eVillain on 26/10/14.
 */
package physics.utils
{

	import entities.Entity;
	import entities.constants.EntityDataConstants;

	import flash.utils.Dictionary;

	import nape.callbacks.CbType;
	import nape.callbacks.InteractionCallback;
	import nape.phys.Body;
	import nape.phys.BodyType;

	import physics.constants.PhysicsConstants;
	import physics.contacts.ContactSet;

	public class PhysicsUtil
	{
		static private var _collisionTypes:Dictionary = new Dictionary();

		static public function onEntityCollided( collision:InteractionCallback ):void
		{
			if (collision.int1.userData.data)
			{
				var entity1:Entity = collision.int1.userData.data;
				PhysicsUtil.addEntityContact( entity1, collision.int2.castBody );
			}
//			if (collision.int2.userData.data)
//			{
//				var entity2:Entity = collision.int2.userData.data;
//				addEntityContact( entity2, collision.int1.castBody );
//			}
		}

		static public function onEntitySeparated( collision:InteractionCallback ):void
		{
			if (collision.int1.userData.data)
			{
				var entity1:Entity = collision.int1.userData.data;
				PhysicsUtil.removeEntityContact( entity1, collision.int2.castBody );
			}
//			if (collision.int2.userData.data)
//			{
//				var entity2:Entity = collision.int2.userData.data;
//				removeEntityContact( entity2, collision.int1.castBody );
//			}
		}

		static public function addEntityContact( entity:Entity, body:Body )
		{
			if (entity.hasDataObjectForKey( EntityDataConstants.ENTITY_DATA_PHYSICS_CONTACTS ))
			{
				var contacts:ContactSet = entity.getDataForKey( EntityDataConstants.ENTITY_DATA_PHYSICS_CONTACTS ) as ContactSet;
				contacts.addContact( body );
			}
		}

		static public function removeEntityContact( entity:Entity, body:Body )
		{
			if (entity.hasDataObjectForKey( EntityDataConstants.ENTITY_DATA_PHYSICS_CONTACTS ))
			{
				var contacts:ContactSet = entity.getDataForKey( EntityDataConstants.ENTITY_DATA_PHYSICS_CONTACTS ) as ContactSet;
				contacts.removeContact( body );
			}
		}

		static public function getBodyTypeForPhysicsType( type:uint ):BodyType
		{
			switch (type)
			{
				case PhysicsConstants.DYNAMIC:
					return BodyType.DYNAMIC;
					break;
				case PhysicsConstants.STATIC:
					return BodyType.STATIC;
					break;
				case PhysicsConstants.KINEMATIC:
					return BodyType.KINEMATIC;
					break;
				default:
					trace( "PhysicsModel ERROR: Unknown body type " + type.toString() );
					break;
			}
			return BodyType.STATIC;
		}

		static public function collisionForType( collisionType:String ):CbType
		{
			if (!_collisionTypes[collisionType])
			{
				_collisionTypes[collisionType] = new CbType();
			}
			return _collisionTypes[collisionType];
		}

	}
}
