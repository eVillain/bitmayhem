/**
 * Created by eVillain on 26/10/14.
 */
package physics.signals
{

	import org.osflash.signals.Signal;

	public class UpdatePhysicsSignal extends Signal
	{
		public function UpdatePhysicsSignal()
		{
			super();
		}
	}
}
