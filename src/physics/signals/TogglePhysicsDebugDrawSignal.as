/**
 * Created by eVillain on 30/08/14.
 */
package physics.signals
{

	import org.osflash.signals.Signal;

	public class TogglePhysicsDebugDrawSignal extends Signal
	{
		public function TogglePhysicsDebugDrawSignal()
		{
			super();
		}
	}
}
