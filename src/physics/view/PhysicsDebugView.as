/**
 * Created by eVillain on 30/08/14.
 */
package physics.view
{

	import camera.GameCamera;
	import camera.signals.CameraUpdatedSignal;

	import flash.display.Sprite;

	import nape.util.Debug;
	import nape.util.ShapeDebug;

	import resolution.constants.ResolutionConstants;
	import resolution.model.ResolutionModel;

	import starling.core.Starling;

	public class PhysicsDebugView extends Sprite
	{
		[Inject]
		public var cameraUpdatedSignal:CameraUpdatedSignal;

		[Inject]
		public var gameCamera:GameCamera;

		[Inject]
		public var resolutionModel:ResolutionModel;

		private var _debug:Debug;

		public function PhysicsDebugView()
		{
			trace( "------> PhysicsDebugView constructor" );
		}

		[PostConstruct]
		public function init():void
		{
			trace( "------> PhysicsDebugView post construct init" );
			cameraUpdatedSignal.add( setZoomLevel );

			// Create a new BitmapDebug screen matching stage dimensions and
			// background colour.
			//   The Debug object itself is not a DisplayObject, we add its
			//   display property to the display list.
			//_debug = new BitmapDebug( resolutionModel.currentWidth, resolutionModel.currentHeight, 0x00000000, true );
			_debug = new ShapeDebug( resolutionModel.currentWidth, resolutionModel.currentHeight, 0x00000000 );
			this.addChild( _debug.display );
		}

		public function attachToStage():void
		{
			// Add debug view to starling native overlay
			Starling.current.nativeStage.addChild( this );
		}

		public function detachFromStage():void
		{
			// Remove debug view from starling native overlay
			Starling.current.nativeStage.removeChild( this );
		}

		public function setZoomLevel():void
		{
			// Set scale according to camera zoom and render scaling
			var scaling:Number = gameCamera.zoom * resolutionModel.renderScale * ResolutionConstants.PIXELS_RENDER_SCALE;
			this.scaleX = scaling;
			this.scaleY = scaling;

			// Get center of screen
			var diffX:Number = (resolutionModel.currentWidth / 2) - (gameCamera.position.x * scaling);
			var diffY:Number = (resolutionModel.currentHeight / 2) - (gameCamera.position.y * scaling);

			// Set the position of the camera to the center of our screen
			this.x = int( diffX );
			this.y = int( diffY );
		}

		public function get debug():Debug
		{
			return _debug;
		}
	}
}
