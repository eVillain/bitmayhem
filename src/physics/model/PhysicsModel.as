/**
 * Created by eVillain on 30/08/14.
 */
package physics.model
{

	import nape.phys.Body;
	import nape.space.Space;

	public class PhysicsModel
	{
		private var _space:Space;

		private var _velocityIterations:int = 4;
		private var _positionIterations:int = 4;

		private var _timeStep:Number = 1.0 / 30.0;
		private var _timeAccumulator:Number = 0.0;
		private var _updated:Boolean = false;
		private var _drawDebug:Boolean = false;
		private var _currentPhysicsFrame:uint = 0;

		public function PhysicsModel()
		{
			trace( "------> PhysicsModel constructor called" );
		}

		[PostConstruct]
		public function init():void
		{
			trace( "------> PhysicsModel post construct init" );
		}

		public function addBody( body:Body ):void
		{
			_space.bodies.add( body );
		}

		public function removeBody( body:Body ):void
		{
			_space.bodies.remove( body );
		}

		public function get drawDebug():Boolean
		{
			return _drawDebug;
		}

		public function set drawDebug( value:Boolean ):void
		{
			_drawDebug = value;
		}

		public function get space():Space
		{
			return _space;
		}

		public function set space( space:Space ):void
		{
			_space = space;
		}

		public function get updated():Boolean
		{
			return _updated;
		}

		public function set updated( updated:Boolean ):void
		{
			_updated = updated;
		}

		public function set timeAccumulator( timeAccumulator:Number ):void
		{
			_timeAccumulator = timeAccumulator;
		}

		public function get timeStep():Number
		{
			return _timeStep;
		}

		public function get currentPhysicsFrame():uint
		{
			return _currentPhysicsFrame;
		}

		public function get velocityIterations():int
		{
			return _velocityIterations;
		}

		public function set velocityIterations( value:int ):void
		{
			_velocityIterations = value;
		}

		public function get positionIterations():int
		{
			return _positionIterations;
		}

		public function set positionIterations( value:int ):void
		{
			_positionIterations = value;
		}

		public function set timeStep( value:Number ):void
		{
			_timeStep = value;
		}

		public function get timeAccumulator():Number
		{
			return _timeAccumulator;
		}

		public function set currentPhysicsFrame( value:uint ):void
		{
			_currentPhysicsFrame = value;
		}
	}
}
