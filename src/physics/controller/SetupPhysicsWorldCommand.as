/**
 * Created by eVillain on 26/10/14.
 */
package physics.controller
{

	import nape.geom.Vec2;
	import nape.space.Space;

	import physics.model.PhysicsModel;

	import robotlegs.bender.bundles.mvcs.Command;

	public class SetupPhysicsWorldCommand extends Command
	{
		[Inject]
		public var physicsModel:PhysicsModel;

		override public function execute():void
		{
			// Creates a new simulation Space.
			// Weak Vec2 will be automatically sent to object pool.
			// when used as argument to Space constructor.
			var gravity:Vec2 = Vec2.weak( 0, 160 );
			physicsModel.space = new Space( gravity );
		}
	}
}
