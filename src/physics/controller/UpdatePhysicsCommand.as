/**
 * Created by eVillain on 26/10/14.
 */
package physics.controller
{

	import gametick.vos.GameTickVO;

	import physics.model.PhysicsModel;
	import physics.view.PhysicsDebugView;

	import robotlegs.bender.bundles.mvcs.Command;

	public class UpdatePhysicsCommand extends Command
	{
		[Inject]
		public var physicsModel:PhysicsModel;

		[Inject]
		public var physicsView:PhysicsDebugView;

		[Inject]
		public var gameTick:GameTickVO;

		override public function execute():void
		{
			physicsModel.updated = false;

			if (gameTick)
			{
				// We only want to update our physics by fixed chunks of time
				// So we accumulate the elapsed time until a full step has elapsed
				physicsModel.timeAccumulator += gameTick.deltaTime;

				// We will drop some frames thus catching up to the latest physics frame
				// Just in case we're running slower than the time step
				while (physicsModel.timeAccumulator >= physicsModel.timeStep)
				{
					// At least a full step has elapsed, we will update physics this frame
					physicsModel.updated = true;
					// Step forward and update simulation by our fixed time step
					physicsModel.space.step( physicsModel.timeStep, physicsModel.velocityIterations, physicsModel.positionIterations );
					// Remove the time we just stepped
					physicsModel.timeAccumulator -= physicsModel.timeStep;
					// We have just moved forward one frame
					physicsModel.currentPhysicsFrame++;
				}
			}

			// Render Space to the debug draw.
			if (physicsModel.drawDebug)
			{
				// We first clear the debug screen,
				physicsView.debug.clear();
				// Then draw the entire Space,
				physicsView.debug.draw( physicsModel.space );
				// And finally flush the draw calls to the screen.
				physicsView.debug.flush();
			}
		}
	}
}
