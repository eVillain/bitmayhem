/**
 * Created by eVillain on 30/08/14.
 */
package physics.controller
{

	import physics.model.PhysicsModel;
	import physics.view.PhysicsDebugView;

	import robotlegs.bender.bundles.mvcs.Command;

	public class TogglePhysicsDebugDrawCommand extends Command
	{
		[Inject]
		public var physicsModel:PhysicsModel;

		[Inject]
		public var physicsView:PhysicsDebugView;

		override public function execute():void
		{
			if (physicsModel.drawDebug)
			{
				physicsView.detachFromStage();
			}
			else
			{
				physicsView.attachToStage();
			}

			physicsModel.drawDebug = !physicsModel.drawDebug;
		}
	}
}
