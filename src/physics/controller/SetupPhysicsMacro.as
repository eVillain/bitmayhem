/**
 * Created by eVillain on 26/10/14.
 */
package physics.controller
{

	import eu.alebianco.robotlegs.utils.impl.SequenceMacro;

	public class SetupPhysicsMacro extends SequenceMacro
	{
		override public function prepare():void
		{
			add( SetupPhysicsWorldCommand );
			add( SetupCollisionHandlersCommand );
		}

		override public function execute():void
		{
			trace( "SetupPhysicsMacro.execute()" );
			super.execute();
		}
	}
}
