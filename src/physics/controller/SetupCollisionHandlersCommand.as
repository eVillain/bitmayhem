/**
 * Created by eVillain on 26/10/14.
 */
package physics.controller
{

	import nape.callbacks.CbEvent;
	import nape.callbacks.InteractionListener;
	import nape.callbacks.InteractionType;
	import nape.space.Space;

	import physics.constants.CollisionConstants;
	import physics.model.PhysicsModel;
	import physics.utils.PhysicsUtil;

	import robotlegs.bender.bundles.mvcs.Command;

	public class SetupCollisionHandlersCommand extends Command
	{
		[Inject]
		public var physicsModel:PhysicsModel;

		override public function execute():void
		{
			var space:Space = physicsModel.space;

			// Collision handlers for players colliding with items
			var interactionBeginPlayerItem:InteractionListener = new InteractionListener( CbEvent.BEGIN, InteractionType.COLLISION, PhysicsUtil.collisionForType( CollisionConstants.COLLISION_TYPE_PLAYER ), PhysicsUtil.collisionForType( CollisionConstants.COLLISION_TYPE_ITEM ), PhysicsUtil.onEntityCollided );
			space.listeners.add( interactionBeginPlayerItem );
			var interactionEndPlayerItem:InteractionListener = new InteractionListener( CbEvent.END, InteractionType.COLLISION, PhysicsUtil.collisionForType( CollisionConstants.COLLISION_TYPE_PLAYER ), PhysicsUtil.collisionForType( CollisionConstants.COLLISION_TYPE_ITEM ), PhysicsUtil.onEntitySeparated );
			space.listeners.add( interactionEndPlayerItem );

			// Collision handlers for players colliding with ground
			var interactionBeginPlayerGround:InteractionListener = new InteractionListener( CbEvent.BEGIN, InteractionType.COLLISION, PhysicsUtil.collisionForType( CollisionConstants.COLLISION_TYPE_PLAYER ), PhysicsUtil.collisionForType( CollisionConstants.COLLISION_TYPE_DEFAULT ), PhysicsUtil.onEntityCollided );
			space.listeners.add( interactionBeginPlayerGround );
			var interactionEndPlayerGround:InteractionListener = new InteractionListener( CbEvent.END, InteractionType.COLLISION, PhysicsUtil.collisionForType( CollisionConstants.COLLISION_TYPE_PLAYER ), PhysicsUtil.collisionForType( CollisionConstants.COLLISION_TYPE_DEFAULT ), PhysicsUtil.onEntitySeparated );
			space.listeners.add( interactionEndPlayerGround );

			// Collision handlers for players colliding with NPCs, same as ground so we can walk on and jump off them
			var interactionBeginPlayerNPC:InteractionListener = new InteractionListener( CbEvent.BEGIN, InteractionType.COLLISION, PhysicsUtil.collisionForType( CollisionConstants.COLLISION_TYPE_PLAYER ), PhysicsUtil.collisionForType( CollisionConstants.COLLISION_TYPE_NPC ), PhysicsUtil.onEntityCollided );
			space.listeners.add( interactionBeginPlayerNPC );
			var interactionEndPlayerNPC:InteractionListener = new InteractionListener( CbEvent.END, InteractionType.COLLISION, PhysicsUtil.collisionForType( CollisionConstants.COLLISION_TYPE_PLAYER ), PhysicsUtil.collisionForType( CollisionConstants.COLLISION_TYPE_NPC ), PhysicsUtil.onEntitySeparated );
			space.listeners.add( interactionEndPlayerNPC );

			// Collision handlers for projectiles colliding with ground
			var interactionBeginProjectileGround:InteractionListener = new InteractionListener( CbEvent.BEGIN, InteractionType.COLLISION, PhysicsUtil.collisionForType( CollisionConstants.COLLISION_TYPE_BULLET ), PhysicsUtil.collisionForType( CollisionConstants.COLLISION_TYPE_DEFAULT ), PhysicsUtil.onEntityCollided );
			space.listeners.add( interactionBeginProjectileGround );
			var interactionEndProjectileGround:InteractionListener = new InteractionListener( CbEvent.END, InteractionType.COLLISION, PhysicsUtil.collisionForType( CollisionConstants.COLLISION_TYPE_BULLET ), PhysicsUtil.collisionForType( CollisionConstants.COLLISION_TYPE_DEFAULT ), PhysicsUtil.onEntitySeparated );
			space.listeners.add( interactionEndProjectileGround );

			// Collision handlers for projectiles colliding with NPCs
			var interactionBeginProjectileNPC:InteractionListener = new InteractionListener( CbEvent.BEGIN, InteractionType.COLLISION, PhysicsUtil.collisionForType( CollisionConstants.COLLISION_TYPE_BULLET ), PhysicsUtil.collisionForType( CollisionConstants.COLLISION_TYPE_NPC ), PhysicsUtil.onEntityCollided );
			space.listeners.add( interactionBeginProjectileNPC );
			var interactionEndProjectileNPC:InteractionListener = new InteractionListener( CbEvent.END, InteractionType.COLLISION, PhysicsUtil.collisionForType( CollisionConstants.COLLISION_TYPE_BULLET ), PhysicsUtil.collisionForType( CollisionConstants.COLLISION_TYPE_NPC ), PhysicsUtil.onEntitySeparated );
			space.listeners.add( interactionEndProjectileNPC );
		}
	}
}
