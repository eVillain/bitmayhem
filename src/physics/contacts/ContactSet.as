/**
 * Created by eVillain on 07/09/14.
 *
 * This class sets up an easy way to store physics contacts.
 * It will contain references to b2Body instances which are
 * currently in contact with the owner of this set.
 *
 */
package physics.contacts
{

	import nape.phys.Body;

	public class ContactSet
	{
		private var _contacts:Vector.<Body>;

		public function ContactSet()
		{
			_contacts = new Vector.<Body>();
		}

		public function get contacts():Vector.<Body>
		{
			return _contacts;
		}

		public function get numContacts():uint
		{
			return _contacts.length;
		}

		public function addContact( contact:Body ):void
		{
			_contacts.push( contact );
		}

		public function removeContact( contact:Body ):void
		{
			var contactIndex:int = contacts.indexOf( contact );
			if (contactIndex == -1)
			{
				trace( "WARNING: Tried to remove unexisting contact from set!" );
			}
			else
			{
				_contacts.splice( contactIndex, 1 );
			}
		}

	}
}
