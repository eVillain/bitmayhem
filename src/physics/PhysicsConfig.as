/**
 * Created by eVillain on 30/08/14.
 */
package physics
{

	import physics.controller.SetupCollisionHandlersCommand;
	import physics.controller.SetupPhysicsMacro;
	import physics.controller.TogglePhysicsDebugDrawCommand;
	import physics.controller.UpdatePhysicsCommand;
	import physics.model.PhysicsModel;
	import physics.signals.SetupCollisionHandlersSignal;
	import physics.signals.SetupPhysicsSignal;
	import physics.signals.TogglePhysicsDebugDrawSignal;
	import physics.signals.UpdatePhysicsSignal;
	import physics.view.PhysicsDebugView;

	import robotlegs.bender.extensions.signalCommandMap.api.ISignalCommandMap;
	import robotlegs.bender.framework.api.IConfig;
	import robotlegs.bender.framework.api.IInjector;

	public class PhysicsConfig implements IConfig
	{
		[Inject]
		public var injector:IInjector;

		[Inject]
		public var signalCommandMap:ISignalCommandMap;

		public function configure():void
		{
			injector.map( PhysicsModel ).asSingleton();

			injector.map( PhysicsDebugView ).asSingleton( true );

			signalCommandMap.map( UpdatePhysicsSignal ).toCommand( UpdatePhysicsCommand );
			signalCommandMap.map( SetupCollisionHandlersSignal ).toCommand( SetupCollisionHandlersCommand );
			signalCommandMap.map( TogglePhysicsDebugDrawSignal ).toCommand( TogglePhysicsDebugDrawCommand );
			signalCommandMap.map( SetupPhysicsSignal ).toCommand( SetupPhysicsMacro );
		}
	}
}
