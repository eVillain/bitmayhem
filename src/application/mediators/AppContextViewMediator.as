package application.mediators
{

	import application.signals.AppContextReadySignal;
	import application.signals.ChangeAppStateSignal;
	import application.views.AppContextView;
	import application.vos.ChangeAppStateVO;

	import robotlegs.extensions.starlingViewMap.impl.StarlingMediator;

	public class AppContextViewMediator extends StarlingMediator
	{
		[Inject]
		public var view:AppContextView;

		[Inject]
		public var changeScreenSignal:ChangeAppStateSignal;

		[Inject]
		public var appContextReadySignal:AppContextReadySignal;

		public function AppContextViewMediator()
		{

		}

		[PostConstruct]
		public function postConstruct():void
		{
			changeScreenSignal.add( onChangeScreen );
			view.appContextReadySignal = appContextReadySignal;
			trace( "------> AppContextViewMediator postConstruct() complete." );
		}

		public function onChangeScreen( changeScreenVO:ChangeAppStateVO ):void
		{
			view.currentScreen = changeScreenVO.nextScreen;
		}

		override public function initialize():void
		{
			super.initialize();
			trace( "------> AppContextViewMediator initialize() complete." );
		}

		override public function destroy():void
		{
			super.initialize();
			trace( this + ", " + "destroy()" );
		}
	}
}
