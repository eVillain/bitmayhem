package application.mediators
{

	import robotlegs.extensions.starlingViewMap.impl.StarlingMediator;

	public class AppStageMediator extends StarlingMediator
	{
		public function AppStageMediator()
		{

		}

		override public function initialize():void
		{
			super.initialize();
			trace( this + ", " + "initialise()" );
		}

		override public function destroy():void
		{
			super.initialize();
			trace( this + ", " + "destroy()" );
		}
	}
}
