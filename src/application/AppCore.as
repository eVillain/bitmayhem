/**
 * Created by eVillain on 19/07/14.
 */
package application
{

	import application.signals.AppContextReadySignal;
	import application.signals.AppResizeSignal;
	import application.signals.ChangeAppStateSignal;
	import application.signals.StartSinglePlayerSignal;
	import application.states.AppStatesConfig;
	import application.states.MainMenuState;
	import application.states.SinglePlayerState;
	import application.views.AppContextView;
	import application.vos.AppResizeVO;
	import application.vos.ChangeAppStateVO;

	import camera.GameCameraConfig;

	import entities.EntitySystemConfig;

	import feathers.themes.MetalWorksDesktopTheme;

	import flash.display.Sprite;
	import flash.display.StageAlign;
	import flash.display.StageScaleMode;

	import game.GameConfig;

	import gametick.GameTickConfig;

	import gui.GUIConfig;
	import gui.signals.InitializeGUISignal;

	import input.InputSystemConfig;

	import particles.ParticlesConfig;

	import physics.PhysicsConfig;

	import resolution.ResolutionConfig;

	import robotlegs.bender.extensions.contextView.ContextView;
	import robotlegs.bender.framework.api.IContext;
	import robotlegs.bender.framework.impl.Context;

	import starling.core.Starling;
	import starling.events.ResizeEvent;

	import tilemap.TileMapConfig;

	[SWF(frameRate="60", width="1024", height="576", backgroundColor="0x333333")]
	public class AppCore extends Sprite
	{
		public static const initialWidth:uint = 1024;
		public static const initialHeight:uint = 576;
		public static const targetFPS:uint = 60;

		private var _context:IContext;
		private var _starling:Starling;
		private var _appResizeSignal:AppResizeSignal;

		public function AppCore()
		{
			stage.align = StageAlign.TOP_LEFT;
			stage.scaleMode = StageScaleMode.NO_SCALE;

			init();
		}

		private function init():void
		{
			trace( "------> AppCore init() begin..." );

			_starling = new Starling( AppContextView, stage );
			trace( "------> AppCore creating new Context, configuring AppConfig." );

			_context = new Context()
					.install( AppCoreBundle )
					.configure( AppConfig, new ContextView( this ), _starling );

			trace( "------> AppCore mapping AppCore injection." );
			_context.injector.map( AppCore ).toValue( this );

			trace( "------> AppCore adding listener to context ready signal." );
			var appContextReadySignal:AppContextReadySignal = _context.injector.getInstance( AppContextReadySignal );
			appContextReadySignal.add( configureGame );

			trace( "------> AppCore adding listener to resize signal." );
			_appResizeSignal = _context.injector.getInstance( AppResizeSignal );
			configureStarlingStage();

			trace( "------> AppCore starting Starling." );
			_starling.antiAliasing = 0;
			_starling.start();

			trace( "------> AppCore init() complete." );
		}

		protected function configureStarlingStage():void
		{
			_starling.stage.addEventListener( ResizeEvent.RESIZE, onResize );
			_starling.nativeStage.frameRate = targetFPS;
			_starling.showStats = true;
		}

		/**
		 * Create the theme. this class will automatically pass skins to any
		 * Feathers component that is added to the stage. components do not
		 * have default skins, so you must always use a theme or skin the
		 * components manually. you should always create a theme immediately
		 * when your app starts up to ensure that all components are
		 * properly skinned.
		 * see http://wiki.starling-framework.org/feathers/themes
		 */
		protected static function configureFeathers():void
		{
			// Uncomment different themes to switch here
			//new AeonDesktopTheme();
			new MetalWorksDesktopTheme();
			//new MinimalDesktopTheme();
		}

		protected function configureGame():void
		{
			configureFeathers();

			_context.configure( ResolutionConfig );

			_context.configure( GameTickConfig );

			// Networking is disabled for now, uncomment this if you want to work on the network code/test broadcast
			//_context.configure( NetworkConfig );

			_context.configure( GameCameraConfig );

			_context.configure( PhysicsConfig );

			_context.configure( ParticlesConfig );

			_context.configure( EntitySystemConfig );

			_context.configure( InputSystemConfig );

			_context.configure( TileMapConfig );

			_context.configure( GameConfig );

			_context.configure( GUIConfig );

			_context.configure( AppStatesConfig );

			// Add listener for start single player game, that will require an application state switch
			var startSinglePlayerSignal:StartSinglePlayerSignal = _context.injector.getInstance( StartSinglePlayerSignal );
			startSinglePlayerSignal.add( startGame );

			// Before we start up the first application state we set up the GUI
			var initializeGUISignal:InitializeGUISignal = _context.injector.getInstance( InitializeGUISignal );
			initializeGUISignal.dispatch();

			// Starts up main menu as current application state
			var mainMenu:MainMenuState = _context.injector.getInstance( MainMenuState );
			var changeScreenSignal:ChangeAppStateSignal = _context.injector.getInstance( ChangeAppStateSignal );

			trace( "------> AppCore game configured, dispatching ChangeScreenSignal with MainMenu as payload." );
			changeScreenSignal.dispatch( new ChangeAppStateVO( mainMenu ) );

			_appResizeSignal.dispatch( new AppResizeVO( initialWidth, initialHeight ) );
		}

		public function startGame():void
		{
			var changeScreenSignal:ChangeAppStateSignal = _context.injector.getInstance( ChangeAppStateSignal );
			var singlePlayer:SinglePlayerState = _context.injector.getInstance( SinglePlayerState );

			changeScreenSignal.dispatch( new ChangeAppStateVO( singlePlayer ) );
		}

		private function onResize( e:ResizeEvent ):void
		{
			//trace( "AppCore resized, new resolution: " + e.width + "x" + e.height );

			_appResizeSignal.dispatch( new AppResizeVO( e.width, e.height ) );
		}

		public function get context():IContext
		{
			return _context;
		}
	}
}
