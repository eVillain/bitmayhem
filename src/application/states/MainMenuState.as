package application.states
{

	import application.constants.AppStateConstants;

	import gui.signals.AddMainMenuButtonsSignal;
	import gui.signals.RemoveMainMenuButtonsSignal;

	import resolution.signals.ResolutionChangedSignal;
	import resolution.vos.ResolutionChangeVO;

	import starling.display.Image;
	import starling.events.Event;

	public class MainMenuState extends AbstractAppState
	{
		[Inject]
		public var resolutionChangedSignal:ResolutionChangedSignal;

		[Inject]
		public var addMainMenuButtonsSignal:AddMainMenuButtonsSignal;

		[Inject]
		public var removeMainMenuButtonsSignal:RemoveMainMenuButtonsSignal;

		private var backGround:Image;

		public function MainMenuState()
		{
			super();
			this.addEventListener( Event.ADDED_TO_STAGE, onAddedToStage );
		}

		private function onAddedToStage( event:Event ):void
		{
			initBackGround();
			trace( "------> Main Menu screen initialized" );
		}

		[PostConstruct]
		public function init():void
		{
			trace( "------> Main Menu post construct init" );
			resolutionChangedSignal.add( onResolutionChange );
		}

		private function onResolutionChange( resolutionVO:ResolutionChangeVO ):void
		{
			// We can scale the entire scene to make sure the UI also scales
			scaleX = resolutionVO.renderScale;
			scaleY = resolutionVO.renderScale;

			// We now need to use the relative background size because of our previous scaling
			var diffX:Number = resolutionVO.width - (backGround.width * resolutionVO.renderScale);
			var diffY:Number = resolutionVO.height - (backGround.height * resolutionVO.renderScale);

			// Set the center of the background to the center of our screen
			backGround.x = diffX / 2;
			backGround.y = diffY / 2;
		}

		private function initBackGround():void
		{
			backGround = new Image( Assets.getTexture( "BMTitle" ) );
			//backGround.y = stage.stageHeight;
			this.addChild( backGround );
		}

		public override function resume():void
		{
			super.resume();

			addMainMenuButtonsSignal.dispatch();

			this.addEventListener( Event.ENTER_FRAME, onMainMenuUpdate );
		}

		public override function pause():void
		{
			super.pause();

			removeMainMenuButtonsSignal.dispatch();

			if (this.hasEventListener( Event.ENTER_FRAME ))
			{
				this.removeEventListener( Event.ENTER_FRAME, onMainMenuUpdate );
			}
		}

		public override function getName():String
		{
			return AppStateConstants.APP_STATE_MAIN_MENU;
		}

		private function onMainMenuUpdate( event:Event ):void
		{

		}
	}
}