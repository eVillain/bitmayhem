/**
 * Created by eVillain on 20/08/14.
 */
package application.states
{

	public interface IAppState
	{
		function resume():void;

		function pause():void;

		function getName():String;
	}
}
