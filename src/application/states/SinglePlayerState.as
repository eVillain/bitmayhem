package application.states
{

	import application.AppCore;
	import application.constants.AppStateConstants;

	import flash.utils.getTimer;

	import game.signals.LoadLevelSignal;
	import game.signals.SpawnPlayerSignal;
	import game.view.GameView;

	import gametick.signals.GameTickSignal;
	import gametick.vos.GameTickVO;

	import gui.signals.AddGameButtonsSignal;
	import gui.signals.AddHelpSignal;
	import gui.signals.AddPlayerInventorySignal;
	import gui.signals.AddShopInventorySignal;
	import gui.signals.RemoveGameButtonsSignal;
	import gui.signals.RemoveHelpSignal;
	import gui.signals.RemoveInventoriesSignal;

	import input.InputSystem;

	import robotlegs.bender.framework.api.IInjector;

	import starling.events.Event;

	public class SinglePlayerState extends AbstractAppState
	{

		[Inject]
		public var addGameButtonsSignal:AddGameButtonsSignal;

		[Inject]
		public var removeGameButtonsSignal:RemoveGameButtonsSignal;

		[Inject]
		public var addShopInventorySignal:AddShopInventorySignal;

		[Inject]
		public var addPlayerInventorySignal:AddPlayerInventorySignal;

		[Inject]
		public var removeInventoriesSignal:RemoveInventoriesSignal;

		[Inject]
		public var addHelpSignal:AddHelpSignal;

		[Inject]
		public var removeHelpSignal:RemoveHelpSignal;

		[Inject]
		public var injector:IInjector;

		[Inject]
		public var appCore:AppCore;

		[Inject]
		public var inputSystem:InputSystem;

		[Inject]
		public var gameView:GameView;

		[Inject]
		public var loadLevelSignal:LoadLevelSignal;

		[Inject]
		public var spawnPlayerSignal:SpawnPlayerSignal;

		private var _started:Boolean = false;
		private var _joined:Boolean = false;

		protected var gameTickVO:GameTickVO;
		protected var gameTickSignal:GameTickSignal;

		private var timePrevious:Number = 0.0;
		private var timeCurrent:Number = 0.0;
		private var timeElapsed:Number = 0.0;

		public function SinglePlayerState()
		{
			super();
			this.addEventListener( Event.ADDED_TO_STAGE, onAddedToStage );
		}

		private function onAddedToStage():void
		{
			this.removeEventListener( Event.ADDED_TO_STAGE, onAddedToStage );
		}

		[PostConstruct]
		public function init():void
		{
			trace( "------> SinglePlayer post construct init" );

			this.addChild( gameView );

			configureGame();
		}

		private function configureGame():void
		{
			gameTickSignal = appCore.context.injector.getInstance( GameTickSignal );
			gameTickVO = appCore.context.injector.getInstance( GameTickVO );
		}

		private function startGame():void
		{
			trace( "------> SinglePlayer start game" );

			addGameButtonsSignal.dispatch();

			this.addEventListener( Event.ENTER_FRAME, onFrameTick );

			timeCurrent = getTimer();

			loadLevelSignal.dispatch();

			_started = true;
		}

		private function onFrameTick( event:Event ):void
		{
			timePrevious = timeCurrent;
			timeCurrent = getTimer();
			timeElapsed = (timeCurrent - timePrevious) * 0.001;

			gameTickVO.deltaTime = timeElapsed;
			gameTickSignal.dispatch( gameTickVO );
		}

		public function onJoinGame( event:Event ):void
		{
			removeGameButtonsSignal.dispatch();

			spawnPlayerSignal.dispatch();

			_joined = true;

			addGameButtonsSignal.dispatch();
			addShopInventorySignal.dispatch();
			addPlayerInventorySignal.dispatch();
			addHelpSignal.dispatch();
		}

		public override function resume():void
		{
			if (!_started)
			{
				startGame();
			}
			else if (joined)
			{
				addGameButtonsSignal.dispatch();
				addShopInventorySignal.dispatch();
				addPlayerInventorySignal.dispatch();
				addHelpSignal.dispatch();
			}

			this.visible = true;
		}

		public override function pause():void
		{
			this.visible = false;

			removeGameButtonsSignal.dispatch();
			removeInventoriesSignal.dispatch();
			removeHelpSignal.dispatch();

			this.removeEventListener( Event.ENTER_FRAME, onFrameTick );
		}

		public override function getName():String
		{
			return AppStateConstants.APP_STATE_SINGLE_PLAYER;
		}

		public function get joined():Boolean
		{
			return _joined;
		}
	}
}