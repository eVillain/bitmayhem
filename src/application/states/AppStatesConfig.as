/**
 * Created by eVillain on 25/10/14.
 */
package application.states
{

	import robotlegs.bender.framework.api.IConfig;
	import robotlegs.bender.framework.api.IInjector;

	public class AppStatesConfig implements IConfig
	{
		[Inject]
		public var injector:IInjector;

		public function configure():void
		{
			injector.map( MainMenuState ).asSingleton();
			injector.map( SinglePlayerState ).asSingleton();
		}
	}
}
