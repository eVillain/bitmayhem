package application.states
{

	import starling.display.Sprite;

	public class AbstractAppState extends Sprite implements IAppState
	{
		public function AbstractAppState()
		{
			super();
		}

		public function resume():void
		{
			this.visible = true;
		}

		public function pause():void
		{
			this.visible = false;
		}

		public function getName():String
		{
			return "ABSTRACT_APP_STATE_OVERRIDE_ME!";
		}
	}
}