/**
 * Created by eVillain on 20/08/14.
 */
package application.constants
{

	public class AppStateConstants
	{
		public static const APP_STATE_MAIN_MENU:String = "APP_STATE_MAIN_MENU";
		public static const APP_STATE_SINGLE_PLAYER:String = "APP_STATE_SINGLE_PLAYER";
	}
}
