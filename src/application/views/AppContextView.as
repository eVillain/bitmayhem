package application.views
{

	import application.signals.AppContextReadySignal;
	import application.states.AbstractAppState;

	import starling.display.Sprite;
	import starling.events.Event;

	public class AppContextView extends Sprite
	{
		public var appContextReadySignal:AppContextReadySignal;

		private var _currentScreen:AbstractAppState;

		public function AppContextView()
		{
			super();

			addEventListener( Event.ADDED_TO_STAGE, init );
		}

		private function init( event:Event = null ):void
		{
			removeEventListener( Event.ADDED_TO_STAGE, init );

			trace( "------> AppContextView added to stage, ready to start engine" );

			appContextReadySignal.dispatch();
		}

		[PostConstruct]
		public function postInit():void
		{
			trace( "------> AppContextView postInit() begin..." );

			trace( "------> AppContextView postInit() complete." );
		}

		public function get currentScreen():AbstractAppState
		{
			return _currentScreen;
		}

		public function set currentScreen( value:AbstractAppState ):void
		{
			if (!value)
			{
				return;
			}

			if (_currentScreen)
			{
				this.removeChild( _currentScreen );
				_currentScreen.pause();
			}

			// Save value
			_currentScreen = value;

			// Add as child
			this.addChild( _currentScreen );

			// Start/resume screen
			_currentScreen.resume();
			trace( "------> AppContextView Screen changed to: " + _currentScreen );
		}
	}
}
