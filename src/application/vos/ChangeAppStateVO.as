/**
 * Created by eVillain on 03/09/14.
 */
package application.vos
{

	import application.states.AbstractAppState;

	public class ChangeAppStateVO
	{
		public var nextScreen:AbstractAppState;

		public function ChangeAppStateVO( nextScreen:AbstractAppState )
		{
			this.nextScreen = nextScreen;
		}
	}
}
