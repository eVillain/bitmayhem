/**
 * Created by eVillain on 26/10/14.
 */
package application.vos
{

	public class AppResizeVO
	{
		private var _width:int;
		private var _height:int;

		public function AppResizeVO( width:int, height:int )
		{
			this._width = width;
			this._height = height;
		}

		public function get width():int
		{
			return _width;
		}

		public function get height():int
		{
			return _height;
		}
	}
}
