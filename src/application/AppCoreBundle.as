/**
 * Created by eVillain on 04/09/14.
 */
package application
{

	import robotlegs.bender.bundles.mvcs.MVCSBundle;
	import robotlegs.bender.extensions.signalCommandMap.SignalCommandMapExtension;
	import robotlegs.bender.framework.api.IContext;
	import robotlegs.bender.framework.api.LogLevel;
	import robotlegs.extensions.starlingViewMap.StarlingViewMapExtension;

	/**
	 * @author Hans Van den Keybus
	 * @copyright (c) 2013, dotdotcommadot
	 */
	public class AppCoreBundle extends MVCSBundle
	{
		override public function extend( context:IContext ):void
		{
			context.logLevel = LogLevel.DEBUG;

			context.install( SignalCommandMapExtension );

			context.install( StarlingViewMapExtension );

			super.extend( context );
		}
	}
}
