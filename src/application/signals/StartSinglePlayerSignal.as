/**
 * Created by eVillain on 03/09/14.
 */
package application.signals
{

	import org.osflash.signals.Signal;

	public class StartSinglePlayerSignal extends Signal
	{
		public function StartSinglePlayerSignal()
		{
			super();
		}
	}
}
