/**
 * Created by eVillain on 19/07/14.
 */
package application.signals
{

	import application.vos.ChangeAppStateVO;

	import org.osflash.signals.Signal;

	public class ChangeAppStateSignal extends Signal
	{

		public function ChangeAppStateSignal()
		{
			super( ChangeAppStateVO );
		}
	}
}
