/**
 * Created by eVillain on 07/09/14.
 */
package application.signals
{

	import application.vos.AppResizeVO;

	import org.osflash.signals.Signal;

	public class AppResizeSignal extends Signal
	{
		public function AppResizeSignal()
		{
			super( AppResizeVO );
		}
	}
}
