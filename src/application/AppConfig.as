package application
{

	import application.mediators.AppContextViewMediator;
	import application.mediators.AppStageMediator;
	import application.signals.AppContextReadySignal;
	import application.signals.AppResizeSignal;
	import application.signals.ChangeAppStateSignal;
	import application.views.AppContextView;

	import robotlegs.bender.extensions.mediatorMap.api.IMediatorMap;
	import robotlegs.bender.framework.api.IConfig;
	import robotlegs.bender.framework.api.IInjector;

	import starling.display.Stage;

	public class AppConfig implements IConfig
	{
		[Inject]
		public var mediatorMap:IMediatorMap;

		[Inject]
		public var injector:IInjector;

		public function configure():void
		{
			injector.map( AppContextReadySignal ).asSingleton( true );
			injector.map( ChangeAppStateSignal ).asSingleton( true );
			injector.map( AppResizeSignal ).asSingleton( true );

			mediatorMap.map( AppContextView ).toMediator( AppContextViewMediator );
			mediatorMap.map( Stage ).toMediator( AppStageMediator );
		}
	}
}
