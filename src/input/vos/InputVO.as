/**
 * Created by eVillain on 03/09/14.
 */
package input.vos
{

	import flash.geom.Point;

	public class InputVO
	{
		public var movement:Point;
		public var jump:Boolean;
		public var run:Boolean;
		public var focus:Boolean;

		public var aimScreenPosition:Point;
		public var fireHeldItem:Boolean;
		public var throwHeldItem:Boolean;

		public function InputVO()
		{
			movement = new Point();
			jump = false;
			run = false;
			focus = false;

			aimScreenPosition = new Point();
			fireHeldItem = false;
		}
	}
}
