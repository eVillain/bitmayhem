/**
 * Created by eVillain on 03/09/14.
 */
package input.signals
{

	import input.vos.InputVO;

	import org.osflash.signals.Signal;

	public class InputUpdatedSignal extends Signal
	{
		public function InputUpdatedSignal()
		{
			super( InputVO );
		}
	}
}
