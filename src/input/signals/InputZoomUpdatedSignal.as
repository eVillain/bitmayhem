/**
 * Created by eVillain on 06/09/14.
 */
package input.signals
{

	import org.osflash.signals.Signal;

	public class InputZoomUpdatedSignal extends Signal
	{
		public function InputZoomUpdatedSignal()
		{
			super(Number);
		}
	}
}
