/**
 * Created by eVillain on 03/09/14.
 */
package input
{

	import input.signals.InputUpdatedSignal;
	import input.signals.InputZoomUpdatedSignal;
	import input.vos.InputVO;

	import robotlegs.bender.framework.api.IConfig;
	import robotlegs.bender.framework.api.IInjector;

	public class InputSystemConfig implements IConfig
	{
		[Inject]
		public var injector:IInjector;

		public function configure():void
		{
			injector.map( InputUpdatedSignal ).asSingleton();
			injector.map( InputZoomUpdatedSignal ).asSingleton();
			injector.map( InputVO ).asSingleton();
			injector.map( InputSystem ).asSingleton();
		}
	}
}
