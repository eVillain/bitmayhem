/**
 * Created by eVillain on 03/09/14.
 */
package input
{

	import application.AppCore;

	import flash.events.MouseEvent;

	import game.signals.ToggleCameraFollowsPlayerSignal;

	import gui.signals.ToggleHelpSignal;
	import gui.signals.ToggleInventoryVisibilitySignal;
	import gui.signals.ToggleStarlingStatsSignal;

	import input.signals.InputUpdatedSignal;
	import input.signals.InputZoomUpdatedSignal;
	import input.vos.InputVO;

	import physics.signals.TogglePhysicsDebugDrawSignal;

	import starling.core.Starling;
	import starling.events.KeyboardEvent;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;

	public class InputSystem
	{
		[Inject]
		public var appCore:AppCore;

		[Inject]
		public var inputVO:InputVO;

		[Inject]
		public var inputUpdatedSignal:InputUpdatedSignal;

		[Inject]
		public var inputZoomUpdatedSignal:InputZoomUpdatedSignal;

		[Inject]
		public var togglePhysicsDebugDrawSignal:TogglePhysicsDebugDrawSignal;

		[Inject]
		public var toggleCameraFollowsPlayerSignal:ToggleCameraFollowsPlayerSignal;

		[Inject]
		public var toggleInventoryVisibilitySignal:ToggleInventoryVisibilitySignal;

		[Inject]
		public var toggleHelpSignal:ToggleHelpSignal;

		[Inject]
		public var toggleStarlingStatsSignal:ToggleStarlingStatsSignal;

		private var _inputThreshold:Number = 0.3;

		private var _debugKeyboardEvents:Boolean = true;

		public function InputSystem()
		{
		}

		[PostConstruct]
		public function init():void
		{
			trace( "-----> InputSystem PostConstruct init" )

			Starling.current.stage.addEventListener( KeyboardEvent.KEY_DOWN, onKeyboardInput );
			Starling.current.stage.addEventListener( KeyboardEvent.KEY_UP, onKeyboardInput );

			Starling.current.stage.addEventListener( TouchEvent.TOUCH, onTouchInput );
			Starling.current.stage.addEventListener( MouseEvent.MOUSE_WHEEL, onMouseWheel );
		}

		private function onMouseWheel( event:MouseEvent ):void
		{
			trace( event );
		}

		public function onKeyboardInput( event:KeyboardEvent ):void
		{
			var sendUpdate:Boolean = true;

			if (!inputVO)
			{
				trace( "InputSystem is missing InputVO injection!!!" );
				return;
			}

			if (event.type === KeyboardEvent.KEY_DOWN)
			{
				if (event.keyCode == 40)		// down arrow
				{
					inputVO.movement.y -= 1;
					//inputZoomUpdatedSignal.dispatch(-0.5);
				}
				else if (event.keyCode == 38)	// up arrow
				{
					inputVO.movement.y += 1;
					//inputZoomUpdatedSignal.dispatch(0.5);
				}
				else if (event.keyCode == 37)	// left arrow
				{
					inputVO.movement.x -= 1;
				}
				else if (event.keyCode == 39)	// right arrow
				{
					inputVO.movement.x += 1;
				}
				else if (event.keyCode == 32)	// space bar
				{
					inputVO.jump = true;
				}
				else if (event.keyCode == 84)	// t key
				{
					inputVO.throwHeldItem = true;
				}
				else if (event.keyCode == 16) // shift
				{
					inputVO.run = true;
				}
				else if (event.keyCode == 17) // control
				{
					inputVO.focus = true;
				}
			}
			else if (event.type === KeyboardEvent.KEY_UP)
			{
				if (event.keyCode == 40)		// down arrow
				{
					inputVO.movement.y += 1;
				}
				else if (event.keyCode == 38)	// up arrow
				{
					inputVO.movement.y -= 1;
				}
				else if (event.keyCode == 37)	// left arrow
				{
					inputVO.movement.x += 1;
				}
				else if (event.keyCode == 39)	// right arrow
				{
					inputVO.movement.x -= 1;
				}
				else if (event.keyCode == 32)	// space bar
				{
					inputVO.jump = false;
				}
				else if (event.keyCode == 84)	// t key
				{
					inputVO.throwHeldItem = false;
				}
				else if (event.keyCode == 16) // shift
				{
					inputVO.run = false;
				}
				else if (event.keyCode == 17) // control
				{
					inputVO.focus = false;
				}
				else if (event.keyCode == 72) // h key
				{
					sendUpdate = false;
					toggleHelpSignal.dispatch();
				}
				else if (event.keyCode == 73) // i key
				{
					sendUpdate = false;
					toggleInventoryVisibilitySignal.dispatch();
				}
				else if (event.keyCode == 188) // , character
				{
					toggleStarlingStatsSignal.dispatch();
				}
				else if (event.keyCode == 190) // . character
				{
					sendUpdate = false;
					togglePhysicsDebugDrawSignal.dispatch();
				}
				else if (event.keyCode == 191) // - character
				{
					sendUpdate = false;
					toggleCameraFollowsPlayerSignal.dispatch();
				}
				else if (_debugKeyboardEvents)
				{
					sendUpdate = false;
					trace( "key pressed, code: " + event.keyCode + ", event type: " + event.type );
				}
			}
			if (sendUpdate)
			{
				if (Math.abs( inputVO.movement.x ) < _inputThreshold)
				{
					inputVO.movement.x = 0;
				}
				if (Math.abs( inputVO.movement.y ) < _inputThreshold)
				{
					inputVO.movement.y = 0;
				}
				// Clamp movement input to range of -1 ~ 1
				inputVO.movement.x = Math.min( inputVO.movement.x, 1.0 );
				inputVO.movement.y = Math.min( inputVO.movement.y, 1.0 );
				inputVO.movement.x = Math.max( inputVO.movement.x, -1.0 );
				inputVO.movement.y = Math.max( inputVO.movement.y, -1.0 );
				inputUpdatedSignal.dispatch( inputVO );
			}
		}

		private function onTouchInput( event:TouchEvent ):void
		{
			var touchBegan:Touch = event.getTouch( Starling.current.stage, TouchPhase.BEGAN );
			if (touchBegan)
			{
				//var startPos:Point = touchBegan.getLocation(Starling.current.stage);
				inputVO.fireHeldItem = true;
			}

			var touchEnded:Touch = event.getTouch( Starling.current.stage, TouchPhase.ENDED );
			if (touchEnded)
			{
				//var endPos:Point = touchEnded.getLocation(Starling.current.stage);
				inputVO.fireHeldItem = false;
			}

			var touchMoved:Touch = event.getTouch( Starling.current.stage, TouchPhase.MOVED );
			if (touchMoved)
			{
				inputVO.aimScreenPosition = touchMoved.getLocation( Starling.current.stage );
			}

			var touchHovered:Touch = event.getTouch( Starling.current.stage, TouchPhase.HOVER );
			if (touchHovered)
			{
				inputVO.aimScreenPosition = touchHovered.getLocation( Starling.current.stage );
			}
		}

		public function get inputThreshold():Number
		{
			return _inputThreshold;
		}

		public function set inputThreshold( value:Number ):void
		{
			_inputThreshold = value;
		}
	}
}
