/**
 * Created by eVillain on 25/03/15.
 */
package audio.vos
{

	public class PlayAudioVO
	{
		public var audioID:String;

		public function PlayAudioVO( audioID:String )
		{
			this.audioID = audioID;
		}
	}
}
