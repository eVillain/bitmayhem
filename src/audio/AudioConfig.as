/**
 * Created by eVillain on 08/02/15.
 */
package audio
{

	import audio.controller.PlayAudioCommand;
	import audio.model.AudioModel;
	import audio.signals.PlayAudioSignal;

	import robotlegs.bender.extensions.signalCommandMap.api.ISignalCommandMap;
	import robotlegs.bender.framework.api.IConfig;
	import robotlegs.bender.framework.api.IInjector;

	public class AudioConfig implements IConfig
	{
		[Inject]
		public var signalCommandMap:ISignalCommandMap;

		[Inject]
		public var injector:IInjector;

		public function configure():void
		{
			injector.map( AudioModel ).asSingleton();
			signalCommandMap.map( PlayAudioSignal ).toCommand( PlayAudioCommand );
		}
	}
}
