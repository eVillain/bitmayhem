/**
 * Created by eVillain on 08/02/15.
 */
package audio.model
{

	public class AudioModel
	{
		private var _audioVolume:Number = 1.0;
		private var _playing:Boolean = false;

		public function AudioModel()
		{
		}

		public function get audioVolume():Number
		{
			return _audioVolume;
		}

		public function set audioVolume( value:Number ):void
		{
			_audioVolume = value;
		}

		public function get playing():Boolean
		{
			return _playing;
		}

		public function set playing( value:Boolean ):void
		{
			_playing = value;
		}
	}
}
