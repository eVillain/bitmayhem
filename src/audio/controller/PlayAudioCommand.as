/**
 * Created by eVillain on 08/02/15.
 */
package audio.controller
{

	import audio.model.AudioModel;
	import audio.vos.PlayAudioVO;

	import robotlegs.bender.bundles.mvcs.Command;

	public class PlayAudioCommand extends Command
	{
		[Inject]
		public var audioModel:AudioModel;

		[Inject]
		public var playAudioVO:PlayAudioVO;

		override public function execute():void
		{
			if (audioModel.playing)
			{
				trace( "We were already playing audio!" );
				return;
			}
			audioModel.playing = true;

			trace( playAudioVO.audioID );

		}
	}
}
