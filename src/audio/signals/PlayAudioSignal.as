/**
 * Created by eVillain on 08/02/15.
 */
package audio.signals
{

	import audio.vos.PlayAudioVO;

	import org.osflash.signals.Signal;

	public class PlayAudioSignal extends Signal
	{
		public function PlayAudioSignal()
		{
			super( PlayAudioVO );
		}
	}
}
