/**
 * Created by eVillain on 31/08/14.
 */
package gametick
{

	import gametick.signals.GameTickSignal;
	import gametick.vos.GameTickVO;

	import robotlegs.bender.framework.api.IConfig;
	import robotlegs.bender.framework.api.IInjector;

	public class GameTickConfig implements IConfig
	{
		[Inject]
		public var injector:IInjector;

		public function configure():void
		{
			var gameTickVO:GameTickVO = new GameTickVO( 0.0 );
			injector.map( GameTickVO ).toValue( gameTickVO );

			injector.map( GameTickSignal ).asSingleton( true );
		}
	}
}
