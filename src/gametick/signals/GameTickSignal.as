/**
 * Created by eVillain on 31/08/14.
 */
package gametick.signals
{

	import gametick.vos.GameTickVO;

	import org.osflash.signals.Signal;

	public class GameTickSignal extends Signal
	{
		public function GameTickSignal()
		{
			super( GameTickVO );
		}
	}
}
