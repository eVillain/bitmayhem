/**
 * Created by eVillain on 31/08/14.
 */
package gametick.vos
{

	public class GameTickVO
	{
		private var _deltaTime:Number;

		public function GameTickVO( deltaTime:Number )
		{
			this.deltaTime = deltaTime;
		}

		public function get deltaTime():Number
		{
			return _deltaTime;
		}

		public function set deltaTime( value:Number ):void
		{
			_deltaTime = value;
		}
	}
}
