/**
 * Created by eVillain on 13/09/14.
 */
package network.mesh
{

	import com.drudgerist.socketANE.UDPSocket;

	import flash.events.DatagramSocketDataEvent;
	import flash.utils.ByteArray;

	import network.address.NetAddress;

	public class NetMeshListener
	{
		private var _entries:Vector.<NetMeshListenerEntry>;
		private var _protocolId:uint;
		private var _timeout:Number;
		private var _running:Boolean;
		private var _socket:UDPSocket;

		public function NetMeshListener( protocolId:uint, timeout:Number = 10.0 )
		{
			_entries = new Vector.<NetMeshListenerEntry>();
			_protocolId = protocolId;
			_timeout = timeout;
			_running = false;
			_socket = new UDPSocket();
			_socket.addEventListener( DatagramSocketDataEvent.DATA, OnDataReceived );
			ClearData();
		}

		private function OnDataReceived( event:DatagramSocketDataEvent ):void
		{
			//trace("NetMeshListener OnDataReceived: " + event.data + " from " + event.srcAddress + ":" + event.srcPort + " destination " + event.dstAddress + ":" + event.dstPort);

			var packet:ByteArray = event.data;

			var sender:NetAddress;
			var bytes_read:int = event.data.length;

			if (bytes_read == 0)
			{
				return;
			}
			if (bytes_read < 13)
			{
				return;
			}

			var packet_zero:uint = packet.readUnsignedInt();
			var packet_protocolId:uint = packet.readUnsignedInt();
			var packet_serverPort:uint = packet.readUnsignedInt();
			var packet_stringLength:uint = packet.readUnsignedInt();

			if (packet_zero != 0)
			{
				return;
			}
			if (packet_protocolId != _protocolId)
			{
				return;
			}

			var name:String = packet.readUTFBytes( packet_stringLength );

			var entry:NetMeshListenerEntry = new NetMeshListenerEntry( name, new NetAddress( event.srcAddress, packet_serverPort ) );

			entry.timeoutAccumulator = 0.0;

			var existingEntry:NetMeshListenerEntry = FindEntry( entry );
			if (!existingEntry)
			{
				_entries.push( entry );
				trace( "NetMeshListener: Received new entry: " + name + " of " + _entries.length.toString() );
			}
			else
			{
				if (entry.address.equals( existingEntry.address ))
				{
					existingEntry.timeoutAccumulator = 0.0;
					//trace( "NetMeshListener: Received update for entry: " + existingEntry.name + " last update: " + existingEntry.timeoutAccumulator.toString());
				}
				else
				{
					trace( "NetMeshListener: Received entry with incorrect address: " + name + " " + entry.address.toString() + " vs " + existingEntry.address.toString() );
				}
			}
		}

		public function Start( port:int ):Boolean
		{
			if (_running)
			{
				trace( "NetMeshListener ERROR: Failed to start, was already running!" );
				return false;
			}

			trace( "NetMeshListener starting listener on port " + port.toString() );
			_socket.bind( port );

			_socket.receive();

			_running = true;
			return true;
		}

		public function Stop():void
		{
			if (!_running)
			{
				trace( "NetMeshListener ERROR: Failed to stop, was not running!" );
				return;
			}
			trace( "NetMeshListener stopping listener" );
			_socket.close();
			_running = false;
			ClearData();
		}

		public function Update( deltaTime:Number ):void
		{
			if (!_running)
			{
				trace( "NetMeshListener ERROR: Failed to update, was not running!" );
				return;
			}

			var numEntries:uint = _entries.length;
			//trace( "NetMeshListener: Updating: " + numEntries.toString() + " entries" );

			for (var i:uint = 0; i < numEntries; i++)
			{
				var entry:NetMeshListenerEntry = _entries[i];

				entry.timeoutAccumulator += deltaTime;

				if (entry.timeoutAccumulator > _timeout)
				{
					trace( "NetMeshListener: Node timed out: " + entry.name );
					_entries.splice( i, 1 );
					numEntries--;
				}
			}
		}

		private function FindEntry( entry:NetMeshListenerEntry ):NetMeshListenerEntry
		{
			var numEntries:uint = _entries.length;
			for (var i:uint = 0; i < numEntries; i++)
			{
				var previousEntry:NetMeshListenerEntry = _entries[i];
				if (entry.name == previousEntry.name)
				{
					return previousEntry;
				}
			}
			return null;
		}

		private function ClearData():void
		{
			trace( "NetMeshListener: Clearing data..." );
			_entries.splice( 0, _entries.length );
		}

		public function GetEntryCount():uint
		{
			return _entries.length;
		}

		public function GetEntry( index:uint ):NetMeshListenerEntry
		{
			if (index >= _entries.length)
			{
				return null;
			}

			return _entries[index];
		}
	}
}