/**
 * Created by eVillain on 13/09/14.
 */
package network.mesh
{

	import network.address.NetAddress;

	public class NetMeshListenerEntry
	{
		private var _name:String;
		private var _address:NetAddress;
		private var _timeoutAccumulator:Number;

		public function NetMeshListenerEntry( name:String, address:NetAddress )
		{
			_name = name;
			_address = address;
			_timeoutAccumulator = 0;
		}

		public function get name():String
		{
			return _name;
		}

		public function get address():NetAddress
		{
			return _address;
		}

		public function get timeoutAccumulator():Number
		{
			return _timeoutAccumulator;
		}

		public function set timeoutAccumulator( value:Number ):void
		{
			_timeoutAccumulator = value;
		}
	}
}
