/**
 * Created by eVillain on 11/09/14.
 */
package network.mesh
{

	import com.drudgerist.socketANE.UDPSocket;

	import flash.utils.ByteArray;

	public class NetMeshBeacon
	{

		private var _name:String;
		private var _protocolId:uint;
		private var _listenerPort:uint;
		private var _serverPort:uint
		private var _running:Boolean;
		private var _socket:UDPSocket;

		public function NetMeshBeacon( name:String, protocolId:uint, listenerPort:uint, serverPort:uint )
		{
			_name = name;
			_protocolId = protocolId;
			_listenerPort = listenerPort;
			_serverPort = serverPort;
			_running = false;
			_socket = new UDPSocket();
		}

		public function Start( port:int ):Boolean
		{
			if (_running)
			{
				trace( "NetBeacon failed to start, was already running!" );
				return false;
			}
			trace( "NetBeacon starting beacon on port " + port.toString() );
			_socket.bind( port );

			_socket.receive();

			_running = true;
			return true;
		}

		public function Stop():void
		{
			if (!_running)
			{
				trace( "NetBeacon failed to stop, was not running!" );
				return;
			}
			trace( "stop beacon\n" );
			_socket.close();
			_running = false;
		}

		public function Update( deltaTime:Number ):void
		{
			if (!_running)
			{
				trace( "NetBeacon failed to update, was not running!" );
				return;
			}

			var packet:ByteArray = new ByteArray();

			packet.writeUnsignedInt( 0 );
			packet.writeUnsignedInt( _protocolId );
			packet.writeUnsignedInt( _serverPort );
			packet.writeUnsignedInt( _name.length );

			if (_name.length > 63)
			{
				trace( "NetBeacon failed to send, name too large!" );
			}

			packet.writeUTFBytes( _name );

			//trace( "NetBeacon sending packet to " + _listenerPort.toString() );

			_socket.send( packet, "255.255.255.255", _listenerPort );
		}

		public function get name():String
		{
			return _name;
		}

		public function set name( value:String ):void
		{
			_name = value;
		}
	}
}
