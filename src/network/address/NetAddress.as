/**
 * Created by eVillain on 11/09/14.
 */
package network.address
{

	public class NetAddress
	{
		private var _port:int;
		private var _address:String;

		public function NetAddress( address:String = "0.0.0.0", port:int = 0 )
		{
			_address = address;
			_port = port;
		}

		public function get port():int
		{
			return _port;
		}

		public function get address():String
		{
			return _address;
		}

		public function equals( other:NetAddress )
		{
			return (_address == other.address && _port == other.port);
		}

		public function toString():String
		{
			return (_address + ":" + _port.toString());
		}
	}
}
