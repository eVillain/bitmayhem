/**
 * Created by eVillain on 10/09/14.
 */
package network
{

	import flash.net.DatagramSocket;

	import network.connection.signals.NetConnectionDataReceivedSignal;
	import network.model.NetworkModel;

	import robotlegs.bender.extensions.signalCommandMap.api.ISignalCommandMap;
	import robotlegs.bender.framework.api.IConfig;
	import robotlegs.bender.framework.api.IInjector;

	public class NetworkConfig implements IConfig
	{
		[Inject]
		public var injector:IInjector;

		[Inject]
		public var signalCommandMap:ISignalCommandMap;

		public function configure():void
		{
			if (!DatagramSocket.isSupported)
			{
				trace( "No UDP socket available, network disabled..." );
			}

			injector.map( NetConnectionDataReceivedSignal ).asSingleton( true );
			injector.map( NetworkModel ).asSingleton( true );

		}
	}
}
