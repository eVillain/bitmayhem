/**
 * Created by eVillain on 12/09/14.
 */
package network.connection
{

	import flash.events.DatagramSocketDataEvent;
	import flash.net.DatagramSocket;
	import flash.utils.ByteArray;

	import network.address.NetAddress;
	import network.connection.constants.NetConnectionModeConstants;
	import network.connection.constants.NetConnectionStateConstants;

	public class BaseNetConnection implements INetConnection
	{
		private var _protocolId:uint;
		private var _timeout:Number;

		private var _running:Boolean;
		private var _mode:String;
		private var _state:String;
		private var _socket:DatagramSocket;
		private var _timeoutAccumulator:Number;
		private var _address:NetAddress;

		public function BaseNetConnection( protocolId:uint, timeout:Number )
		{
			_protocolId = protocolId;
			_timeout = timeout;
			_mode = NetConnectionModeConstants.CONNECTION_MODE_NONE;
			_state = NetConnectionStateConstants.CONNECTION_STATE_DISCONNECTED;
			_running = false;
			_socket = new DatagramSocket();
			_socket.addEventListener( DatagramSocketDataEvent.DATA, OnReceivePacket );
		}

		public function Start( port:int ):Boolean
		{
			if (_running)
			{
				trace( "NetConnection ERROR: failed to start, was already running!" );
				return false;
			}
			trace( "NetConnection starting connection to port: " + port.toString() );

			try
			{
				_socket.bind( port ) // localAddress:String = "0.0.0.0"
			}
			catch (e:Error)
			{
				trace( "NetConnection ERROR: " + e.toString() );
				return false;
			}
			_running = true;
			return true;
		}

		public function Stop():void
		{
			if (!_running)
			{
				trace( "NetConnection ERROR: failed to stop, was not running!" );
				return;
			}
			trace( "NetConnection stopping connection." );

			ClearData();
			_socket.close();
			_running = false;
			if (IsConnected)
			{
				OnDisconnect();
			}
			OnStop();
		}

		public function Listen():void
		{
			trace( "NetConnection - server listening for connection" );
			ClearData();
			if (IsConnected)
			{
				OnDisconnect();
			}
			_mode = NetConnectionModeConstants.CONNECTION_MODE_SERVER;
			_state = NetConnectionStateConstants.CONNECTION_STATE_LISTENING;
		}

		public function Connect( address:NetAddress )
		{
			trace( "NetConnection - client connecting to: " + address.address + ":" + address.port.toString() );
			ClearData();
			if (IsConnected)
			{
				OnDisconnect();
			}
			_mode = NetConnectionModeConstants.CONNECTION_MODE_CLIENT;
			_state = NetConnectionStateConstants.CONNECTION_STATE_CONNECTING;
			_address = address;
		}

		private function get IsConnecting():Boolean
		{
			return _state == NetConnectionStateConstants.CONNECTION_STATE_CONNECTING;
		}

		private function get ConnectFailed():Boolean
		{
			return _state == NetConnectionStateConstants.CONNECTION_STATE_FAILED;
		}

		private function get IsConnected():Boolean
		{
			return _state == NetConnectionStateConstants.CONNECTION_STATE_CONNECTED;
		}

		private function get IsListening():Boolean
		{
			return _state == NetConnectionStateConstants.CONNECTION_STATE_LISTENING;
		}

		public function Update( deltaTime:Number ):void
		{
			if (!_running)
			{
				trace( "NetConnection ERROR: failed to update, was not running!" );
				return;
			}

			_timeoutAccumulator += deltaTime;
			if (_timeoutAccumulator > _timeout)
			{
				if (IsConnecting)
				{
					trace( "NetConnection: connect timed out." );
					ClearData();
					_state = NetConnectionStateConstants.CONNECTION_STATE_FAILED;
					OnDisconnect();
				}
				else if (IsConnected)
				{
					trace( "NetConnection: connection timed out." );
					ClearData();
					if (IsConnecting)
					{
						_state = NetConnectionStateConstants.CONNECTION_STATE_FAILED;
					}
					OnDisconnect();
				}
			}
		}

		public function SendPacket( packet:ByteArray ):Boolean
		{
			if (!_running)
			{
				trace( "NetConnection ERROR: failed to send packet, was not running!" );
				return false;
			}

			if (!_address)
			{
				trace( "NetConnection ERROR: failed to send packet, didn't have an address!" );
				return false;
			}

			var bytes:ByteArray = new ByteArray();
			bytes.writeUnsignedInt( _protocolId );
			bytes.writeBytes( packet );

			_socket.send( bytes );
			return true;
		}

		protected function OnReceivePacket( e:DatagramSocketDataEvent ):void
		{
			if (!_running)
			{
				trace( "NetConnection ERROR: failed to receive packet, was not running!" );
				return;
			}

			var bytes_read:int = e.data.length;
			if (bytes_read == 0)
			{
				return;
			}
			if (bytes_read <= 4)
			{
				return;
			}

			var protocolId = e.data.readUnsignedInt();
			if (protocolId != _protocolId)
			{
				return;
			}

			var sender:NetAddress = new NetAddress( e.srcAddress, e.srcPort );

			if (_mode == NetConnectionModeConstants.CONNECTION_MODE_SERVER && IsConnected)
			{
				trace( "NetConnection: Server accepts connection from client " + sender.toString() );
				_state = NetConnectionStateConstants.CONNECTION_STATE_CONNECTED;
				_address = sender;
				OnConnect();
			}
			if (sender == _address)
			{
				if (mode == NetConnectionModeConstants.CONNECTION_MODE_CLIENT && IsConnecting)
				{
					trace( "NetConnection: Client completes connection with server" );
					_state = NetConnectionStateConstants.CONNECTION_STATE_CONNECTED;
					OnConnect();
				}
				_timeoutAccumulator = 0.0;
			}
		}

		public function OnStart():void
		{
			// Override in extending class
		}

		public function OnStop():void
		{
			// Override in extending class
		}

		public function OnConnect():void
		{
			// Override in extending class
		}

		public function OnDisconnect():void
		{
			// Override in extending class
		}

		public function get running():Boolean
		{
			return _running;
		}

		public function get mode():String
		{
			return _mode;
		}

		private function ClearData():void
		{
			_state = NetConnectionStateConstants.CONNECTION_STATE_DISCONNECTED;
			_timeoutAccumulator = 0.0;
			_address = new NetAddress();
		}
	}
}
