/**
 * Created by eVillain on 12/09/14.
 */
package network.connection
{

	import flash.utils.ByteArray;

	public interface INetConnection
	{
		function OnStart():void;

		function OnStop():void;

		function OnConnect():void;

		function OnDisconnect():void;

		function SendPacket( packet:ByteArray ):Boolean;
	}
}
