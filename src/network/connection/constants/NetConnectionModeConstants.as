/**
 * Created by eVillain on 12/09/14.
 */
package network.connection.constants
{

	public class NetConnectionModeConstants
	{
		public static const CONNECTION_MODE_NONE = "CONNECTION_MODE_NONE";
		public static const CONNECTION_MODE_CLIENT = "CONNECTION_MODE_CLIENT";
		public static const CONNECTION_MODE_SERVER = "CONNECTION_MODE_SERVER";
	}
}
