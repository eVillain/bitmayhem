/**
 * Created by eVillain on 12/09/14.
 */
package network.connection.constants
{

	public class NetConnectionStateConstants
	{
		public static const CONNECTION_STATE_DISCONNECTED = "CONNECTION_STATE_DISCONNECTED";
		public static const CONNECTION_STATE_LISTENING = "CONNECTION_STATE_LISTENING";
		public static const CONNECTION_STATE_CONNECTING = "CONNECTION_STATE_CONNECTING";
		public static const CONNECTION_STATE_FAILED = "CONNECTION_STATE_FAILED";
		public static const CONNECTION_STATE_CONNECTED = "CONNECTION_STATE_CONNECTED";
	}
}
