/**
 * Created by eVillain on 21/09/14.
 */
package network.connection.reliability
{

	public class PacketUtils
	{
		static public function IsSequenceMoreRecent( s1:uint, s2:uint, max_sequence:uint )
		{
			return ( s1 > s2 ) && ( s1 - s2 <= max_sequence / 2 ) || ( s2 > s1 ) && ( s2 - s1 > max_sequence / 2 );
		}

		public static function GetBitIndexForSequence( sequence:uint, ack:uint, max_sequence:uint ):uint
		{
			if (sequence == ack)
			{
				throw ("ERROR: Sequence == ack");
			}
			if (IsSequenceMoreRecent( sequence, ack, max_sequence ))
			{
				throw ("ERROR: Sequence is more recent than ack");
			}
			if (sequence > ack)
			{
				if (ack > 33)
				{
					throw ("ERROR: ack is over max ack");
				}
				if (max_sequence < sequence)
				{
					throw ("ERROR: sequence is larger than max");
				}
				return ack + ( max_sequence - sequence );
			}
			else
			{
				if (ack < 1)
				{
					throw ("ERROR: ack is too small");
				}
				if (sequence > ack - 1)
				{
					throw ("ERROR: ack is not next in sequence");
				}
				return ack - 1 - sequence;
			}
		}

		static public function GenerateAckBits( ack:uint, received_queue:PacketQueue, max_sequence:uint ):uint
		{
			var ack_bits:uint = 0;

			var len:int = received_queue.length;
			for (var i:int = 0; i < len; i++)
			{
				var queuedPacket:PacketData = received_queue[i];
				if (queuedPacket.sequence == ack || IsSequenceMoreRecent( queuedPacket.sequence, ack, max_sequence ))
				{
					break;
				}

				var bit_index:uint = GetBitIndexForSequence( queuedPacket.sequence, ack, max_sequence );
				if (bit_index <= 31)
				{
					ack_bits |= 1 << bit_index;
				}
			}
			return ack_bits;
		}

		static public function ProcessAck( ack:uint, ack_bits:uint, pending_ack_queue:PacketQueue, acked_queue:PacketQueue, acks:Vector.<uint>, acked_packets:uint, rtt:Number, max_sequence:uint ):void
		{
			if (pending_ack_queue.queue.length == 0)
			{
				return;
			}

			var len:int = pending_ack_queue.queue.length;
			for (var i:int = 0; i < len; i++)
			{
				var queuedPacket:PacketData = pending_ack_queue.queue[i];
				var acked:Boolean = false;

				if (queuedPacket.sequence == ack)
				{
					acked = true;
				}
				else if (!IsSequenceMoreRecent( queuedPacket.sequence, ack, max_sequence ))
				{
					var bit_index:uint = GetBitIndexForSequence( queuedPacket.sequence, ack, max_sequence );
					if (bit_index <= 31)
					{
						acked = ( ack_bits >> bit_index ) & 1;
					}
				}

				if (acked)
				{
					rtt += ( queuedPacket.time - rtt ) * 0.1;

					acked_queue.InsertSorted( queuedPacket, max_sequence );
					acks.push( queuedPacket.sequence );
					acked_packets++;
					pending_ack_queue.Erase( i );
					len--;
					i--;
				}
			}
		}
	}
}
