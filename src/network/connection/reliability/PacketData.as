/**
 * Created by eVillain on 21/09/14.
 * Based on code by Glenn Fiedler
 */
package network.connection.reliability
{

	public class PacketData
	{
		public var sequence:uint;	// packet sequence number
		public var time:Number;		// time offset since packet was sent or received (depending on context)
		public var size:uint;		// packet size in bytes

		public function PacketData( sequence:uint, time:Number, size:uint )
		{
			this.sequence = sequence;
			this.time = time;
			this.size = size;
		}
	}
}
