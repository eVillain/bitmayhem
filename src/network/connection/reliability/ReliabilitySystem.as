/**
 * Created by eVillain on 21/09/14.
 * Based on code by Glenn Fiedler
 * Reliability system to support reliable connection
 * Manages sent, received, pending ack and acked packet queues
 */
package network.connection.reliability
{

	public class ReliabilitySystem
	{
		private var max_sequence:uint = 0xFFFFFFFF;			// maximum sequence value before wrap around (used to test sequence wrap at low # values)
		private var _local_sequence:uint;		// local sequence number for most recently sent packet
		private var _remote_sequence:uint;		// remote sequence number for most recently received packet

		private var sent_packets:uint;			// total number of packets sent
		private var recv_packets:uint;			// total number of packets received
		private var lost_packets:uint;			// total number of packets lost
		private var acked_packets:uint;			// total number of packets acked

		private var sent_bandwidth:Number;				// approximate sent bandwidth over the last second
		private var acked_bandwidth:Number;				// approximate acked bandwidth over the last second
		private var rtt:Number;							// estimated round trip time
		private var rtt_maximum:Number;					// maximum expected round trip time (hard coded to one second for the moment)

		private var acks:Vector.<uint>;		// acked packets from last set of packet receives. cleared each update!

		private var sentQueue:PacketQueue;				// sent packets used to calculate sent bandwidth (kept until rtt_maximum)
		private var pendingAckQueue:PacketQueue;		// sent packets which have not been acked yet (kept until rtt_maximum * 2 )
		private var receivedQueue:PacketQueue;			// received packets for determining acks to send (kept up to most recent recv sequence - 32)
		private var ackedQueue:PacketQueue;				// acked packets (kept until rtt_maximum * 2)

		public function ReliabilitySystem()
		{
			Reset();
		}

		public function Reset():void
		{
			_local_sequence = 0;
			_remote_sequence = 0;
			sentQueue.Clear();
			receivedQueue.Clear();
			pendingAckQueue.Clear();
			ackedQueue.Clear();
			sent_packets = 0;
			recv_packets = 0;
			lost_packets = 0;
			acked_packets = 0;
			sent_bandwidth = 0.0;
			acked_bandwidth = 0.0;
			rtt = 0.0;
			rtt_maximum = 1.0;
		}

		public function PacketSent( size:uint ):void
		{
			if (sentQueue.Exists( _local_sequence ))
			{
				var sq:Vector.<PacketData> = sentQueue.queue;
				trace( "local sequence " + _local_sequence + "exists" );

				var len:int = sq.length;
				for (var i:int = 0; i < len; i++)
				{
					var queuedPacket:PacketData = sq[i];
					trace( queuedPacket.sequence );
				}
			}

			if (sentQueue.Exists( _local_sequence ))
			{
				trace( "ReliabilitySystem ERROR on PacketSent: local sequence already in sent queue!" )
				return;
			}

			if (pendingAckQueue.Exists( _local_sequence ))
			{
				trace( "ReliabilitySystem ERROR on PacketSent: local sequence already in pending ack queue!" )
				return;
			}

			var data:PacketData = new PacketData( _local_sequence, 0.0, size );

			sentQueue.queue.push( data );
			pendingAckQueue.queue.push( data );

			sent_packets++;
			_local_sequence++;

			if (_local_sequence > max_sequence)
			{
				_local_sequence = 0;
			}
		}

		public function PacketReceived( sequence:uint, size:uint ):void
		{
			recv_packets++;

			if (receivedQueue.Exists( sequence ))
			{
				return;
			}
			var data:PacketData = new PacketData( sequence, 0.0, size );

			receivedQueue.queue.push( data );
			if (PacketUtils.IsSequenceMoreRecent( sequence, _remote_sequence, max_sequence ))
			{
				_remote_sequence = sequence;
			}
		}

		public function GenerateAckBits():uint
		{
			return PacketUtils.GenerateAckBits( _remote_sequence, receivedQueue, max_sequence );
		}

		public function ProcessAck( ack:uint, ack_bits:uint ):void
		{
			PacketUtils.ProcessAck( ack, ack_bits, pendingAckQueue, ackedQueue, acks, acked_packets, rtt, max_sequence );
		}

		public function Update( deltaTime:Number ):void
		{
			acks.splice( 0, acks.length );
			AdvanceQueueTime( deltaTime );
			UpdateQueues();
			UpdateStats();
		}

		public function Validate():void
		{
			sentQueue.VerifySorted( max_sequence );
			receivedQueue.VerifySorted( max_sequence );
			pendingAckQueue.VerifySorted( max_sequence );
			ackedQueue.VerifySorted( max_sequence );
		}

		protected function AdvanceQueueTime( deltaTime:Number ):void
		{
			sentQueue.Update( deltaTime );
			receivedQueue.Update( deltaTime );
			pendingAckQueue.Update( deltaTime );
			ackedQueue.Update( deltaTime );
		}

		protected function UpdateQueues():void
		{
			const epsilon:Number = 0.001;

			while (sentQueue.length && sentQueue.queue[0].time > rtt_maximum + epsilon)
			{
				sentQueue.queue.splice( 0, 1 );	// Pop front
			}

			if (receivedQueue.length)
			{
				const latest_sequence:uint = receivedQueue.queue[receivedQueue.length - 1].sequence;
				const minimum_sequence:uint = latest_sequence >= 34 ? ( latest_sequence - 34 ) : max_sequence - ( 34 - latest_sequence );
				while (receivedQueue.length && !PacketUtils.IsSequenceMoreRecent( receivedQueue.queue[0].sequence, minimum_sequence, max_sequence ))
				{
					receivedQueue.queue.splice( 0, 1 );	// Pop front
				}
			}

			while (ackedQueue.length && ackedQueue.queue[0].time > rtt_maximum * 2 - epsilon)
			{
				ackedQueue.queue.splice( 0, 1 );	// Pop front
			}

			while (pendingAckQueue.length && pendingAckQueue.queue[0].time > rtt_maximum + epsilon)
			{
				pendingAckQueue.queue.splice( 0, 1 );	// Pop front
				lost_packets++;
			}
		}

		protected function UpdateStats():void
		{
			var sent_bytes_per_second:int = 0;

			var sq:Vector.<PacketData> = sentQueue.queue;
			var len:int = sq.length;
			for (var i:int = 0; i < len; i++)
			{
				var sentPakcet:PacketData = sq[i];
				sent_bytes_per_second += sentPakcet.size;
			}

			var acked_packets_per_second:int = 0;
			var acked_bytes_per_second:int = 0;

			var aq:Vector.<PacketData> = ackedQueue.queue;
			var len:int = aq.length;
			for (var i:int = 0; i < len; i++)
			{
				var ackedPacket:PacketData = aq[i];
				if (ackedPacket.time >= rtt_maximum)
				{
					acked_packets_per_second++;
					acked_bytes_per_second += ackedPacket.size;
				}
			}

			sent_bytes_per_second /= rtt_maximum;
			acked_bytes_per_second /= rtt_maximum;
			sent_bandwidth = sent_bytes_per_second * ( 8 / 1000.0 );
			acked_bandwidth = acked_bytes_per_second * ( 8 / 1000.0 );
		}

		public function get local_sequence():uint
		{
			return _local_sequence;
		}

		public function get remote_sequence():uint
		{
			return _remote_sequence;
		}
	}
}
