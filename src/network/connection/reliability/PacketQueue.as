/**
 * Created by eVillain on 21/09/14.
 * Based on C++ code by Glenn Fiedler
 * Packet queue to store information about sent and received packets sorted in sequence order
 */
package network.connection.reliability
{

	public class PacketQueue
	{
		static const MAX_SEQUENCE:uint = 0xFFFFFFFF;
		private var _queue:Vector.<PacketData>;

		public function PacketQueue()
		{
			_queue = new Vector.<PacketData>();
		}

		public function Exists( sequence:uint ):Boolean
		{
			var len:int = _queue.length;
			for (var i:int = 0; i < len; i++)
			{
				if (_queue[i].sequence == sequence)
				{
					return true;
				}
			}
			return false;
		}

		public function InsertSorted( packet:PacketData, max_sequence:uint ):void
		{
			if (_queue.length == 0)
			{
				_queue.push( packet );
			}
			else
			{
				if (!PacketUtils.IsSequenceMoreRecent( packet.sequence, _queue[_queue.length - 1].sequence, max_sequence ))
				{
					_queue.unshift( packet );
				}
				else if (PacketUtils.IsSequenceMoreRecent( packet.sequence, _queue[0].sequence, max_sequence ))
				{
					_queue.push( packet );
				}
				else
				{
					var len:int = _queue.length;
					for (var i:int = 0; i < len; i++)
					{
						var queuedPacket:PacketData = _queue[i];
						if (queuedPacket.sequence == packet.sequence)
						{
							trace( "PacketQueue ERROR: A packet was already in queue with that sequence." );
							return;
						}

						if (PacketUtils.IsSequenceMoreRecent( queuedPacket.sequence, packet.sequence, max_sequence ))
						{
							_queue.splice( i, 0, packet );
							break;
						}
					}
				}
			}
		}

		public function VerifySorted( max_sequence:uint ):void
		{
			var len:int = _queue.length;
			var previousPacket:PacketData = _queue[len - 1];

			for (var i:int = 0; i < len; i++)
			{
				var queuedPacket:PacketData = _queue[i];
				if (queuedPacket.sequence > max_sequence)
				{
					trace( "PacketQueue ERROR: Packet in queue had sequence > max_sequence!" );
					return;
				}
				if (previousPacket != _queue[len - 1])
				{
					if (!PacketUtils.IsSequenceMoreRecent( queuedPacket.sequence, previousPacket.sequence, max_sequence ))
					{
						trace( "PacketQueue ERROR: Packets in queue are not ordered!" );
						return;
					}
					previousPacket = queuedPacket;
				}
			}
		}

		public function Erase( index:uint )
		{
			if (index >= _queue.length)
			{
				trace( "PacketQueue ERROR: Couldn't remove packet " + index.toString() + " of " + _queue.length.toString() );
				return;
			}
			_queue.splice( index, 1 );
		}

		public function Clear()
		{
			_queue.splice( 0, _queue.length );
		}

		public function Update( deltaTime:Number )
		{
			var len:int = _queue.length;
			for (var i:int = 0; i < len; i++)
			{
				var queuedPacket:PacketData = _queue[i];
				queuedPacket.time += deltaTime;
			}
		}

		public function get queue():Vector.<PacketData>
		{
			return _queue;
		}
	}
}
