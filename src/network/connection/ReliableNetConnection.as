/**
 * Created by eVillain on 21/09/14.
 */
package network.connection
{

	import flash.events.DatagramSocketDataEvent;
	import flash.utils.ByteArray;

	import network.connection.reliability.ReliabilitySystem;

	public class ReliableNetConnection extends BaseNetConnection
	{
		private var _reliabilitySystem:ReliabilitySystem;

		public function ReliableNetConnection( protocolId:uint, timeout:Number )
		{
			super( protocolId, timeout );
			_reliabilitySystem = new ReliabilitySystem();
			ClearData();
		}

		override public function SendPacket( data:ByteArray ):Boolean
		{
			const header:int = 12;
			var packet:ByteArray = new ByteArray();
			packet.length = data.length + header;
			var seq:uint = _reliabilitySystem.local_sequence;
			var ack:uint = _reliabilitySystem.remote_sequence;
			var ack_bits:uint = _reliabilitySystem.GenerateAckBits();

			// Write packet header
			data.writeUnsignedInt( seq );
			data.writeUnsignedInt( ack );
			data.writeUnsignedInt( ack_bits );

			// Write packet data
			packet.writeBytes( data );

			if (!super.SendPacket( packet ))
			{
				return false;
			}
			_reliabilitySystem.PacketSent( packet.length );
			return true;
		}

		override protected function OnReceivePacket( e:DatagramSocketDataEvent ):void
		{
			const header:int = 12;
			var size:int = e.data.length;

			if (size <= header)
			{
				return;
			}

			var received_bytes = e.data.length;

			if (received_bytes == 0)
			{
				return;
			}
			if (received_bytes <= header)
			{
				return;
			}

			var packet_sequence:uint = e.data.readUnsignedInt();
			var packet_ack:uint = e.data.readUnsignedInt();
			var packet_ack_bits:uint = e.data.readUnsignedInt();

			_reliabilitySystem.PacketReceived( packet_sequence, received_bytes - header );
			_reliabilitySystem.ProcessAck( packet_ack, packet_ack_bits );

			// TODO: Pack a VO with the data for the game
			//memcpy( data, packet + header, received_bytes - header );

			// TODO: Dispatch signal with data
		}

		override public function Update( deltaTime:Number ):void
		{
			super.Update( deltaTime );
			_reliabilitySystem.Update( deltaTime );
		}

		override public function OnStop():void
		{
			ClearData();
		}

		override public function OnDisconnect():void
		{
			ClearData();
		}

		private function ClearData():void
		{
			_reliabilitySystem.Reset();
		}
	}
}
