/**
 * Created by eVillain on 13/09/14.
 */
package network.connection.signals
{

	import flash.utils.ByteArray;

	import org.osflash.signals.Signal;

	public class NetConnectionDataReceivedSignal extends Signal
	{
		public function NetConnectionDataReceivedSignal()
		{
			super( ByteArray );
		}
	}
}
