/**
 * Created by eVillain on 11/09/14.
 */
package network.model
{

	import flash.desktop.NativeApplication;
	import flash.events.Event;
	import flash.filesystem.File;
	import flash.net.InterfaceAddress;
	import flash.net.NetworkInfo;
	import flash.net.NetworkInterface;

	import gametick.signals.GameTickSignal;
	import gametick.vos.GameTickVO;

	import network.mesh.NetMeshBeacon;
	import network.mesh.NetMeshListener;
	import network.mesh.NetMeshListenerEntry;

	public class NetworkModel
	{
		private var _serverPort:uint = 30000;
		private var _clientPort:uint = 30001;
		private var _beaconPort:uint = 40000;
		private var _listenerPort:uint = 40001;
		private var _protocolId:uint = 0x31337;
		private var _deltaTime:Number = 0.25;
		private var _timeOut:Number = 10.0;
		private var _accumulator:Number = 0.0;
		private var _userName:String;
		private var _localAddress:String;
		private var _beacon:NetMeshBeacon;
		private var _listener:NetMeshListener;
		private var _debugOutput:Boolean = false;

		[Inject]
		public var gameTickSignal:GameTickSignal;

		public static function GetClientIPAddress( version:String = "IPv4" ):String
		{
			var ni:NetworkInfo = NetworkInfo.networkInfo;
			var interfaceVector:Vector.<NetworkInterface> = ni.findInterfaces();
			var currentNetwork:NetworkInterface;

			for each (var networkInt:NetworkInterface in interfaceVector)
			{
				if (networkInt.active)
				{
					for each (var address:InterfaceAddress in networkInt.addresses)
					{
						if (address.ipVersion == version)
						{
							return address.address;
						}
					}
				}
			}
			return "";
		}

		public static function GetCurrentOSUser():String
		{
			var userDir:String = File.userDirectory.nativePath;
			var userName:String = userDir.substr( userDir.lastIndexOf( File.separator ) + 1 );
			return userName;
		}

		public function NetworkModel()
		{
		}

		[PostConstruct]
		public function init():void
		{
			trace( "NetworkModel PostConstruct init..." );

			// Initialize username and address variables
			if (!_userName || _userName == "")
			{
				_userName = GetCurrentOSUser();
			}

			if (!_localAddress || _localAddress == "")
			{
				_localAddress = GetClientIPAddress();
			}

			// Create beacon (sends broadcast packets to the LAN...)
			_beacon = new NetMeshBeacon( _userName, _protocolId, _listenerPort, _serverPort );

			if (!_beacon.Start( _beaconPort ))
			{
				trace( "NetworkModel: Could not start beacon" );
			}

			// Create listener (listens for packets sent from beacons on the LAN...)
			_listener = new NetMeshListener( _protocolId, _timeOut );

			if (!_listener.Start( _listenerPort ))
			{
				trace( "NetworkModel: could not start listener" );
			}

			gameTickSignal.add( Update );

			NativeApplication.nativeApplication.addEventListener( Event.EXITING, OnAppExit );
		}

		private function OnAppExit( event:Event ):void
		{
			trace( "NetworkModel: cleaning up and closing up sockets." );
			_beacon.Stop();
			_listener.Stop();
			_beacon = null;
			_listener = null;
		}

		public function Update( gameTickVO:GameTickVO ):void
		{
			_accumulator += gameTickVO.deltaTime;

			if (_accumulator >= 1.5)
			{
				_accumulator -= 1.5;
				var entryCount:uint = _listener.GetEntryCount();
				if (entryCount > 0)
				{
					if (_debugOutput)
					{
						trace( "---------------------------------------------" );
						for (var i:uint = 0; i < entryCount; ++i)
						{
							var entry:NetMeshListenerEntry = _listener.GetEntry( i );
							if (entry.name == _userName && entry.address.address == _localAddress)
							{
								//trace( "NetworkModel: loopback is working fine, I'm aware of my own network broadcasts." );
								trace( "Local " + i.toString() + " - " + entry.name + "@" + entry.address.address + ":" + entry.address.port.toString() );
								continue;
							}
							trace( "Client " + i.toString() + " - " + entry.name + "@" + entry.address.address + ":" + entry.address.port.toString() );
						}
						trace( "---------------------------------------------" );
					}
				}
//				else
//				{
//					trace( "NetworkModel update, no entries in listener,,," );
//				}
			}

			_beacon.Update( gameTickVO.deltaTime );
			_listener.Update( gameTickVO.deltaTime );
		}

		public function get beacon():NetMeshBeacon
		{
			return _beacon;
		}

		public function set beacon( value:NetMeshBeacon ):void
		{
			_beacon = value;
		}

		public function get listener():NetMeshListener
		{
			return _listener;
		}

		public function set listener( value:NetMeshListener ):void
		{
			_listener = value;
		}

		public function get userName():String
		{
			return _userName;
		}

		public function set userName( value:String ):void
		{
			_userName = value;
			if (_beacon)
			{
				_beacon.name = _userName;
			}
		}
	}
}
