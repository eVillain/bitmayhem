package background
{

	import starling.display.Image;
	import starling.display.Sprite;
	import starling.events.Event;

	public class BackgroundLayer extends Sprite
	{
		private var image1:Image;
		private var image2:Image;

		private var m_layer:int;
		private var m_parallax:Number;

		public function BackgroundLayer( layer:int )
		{
			super();
			this.m_layer = layer;
			this.addEventListener( Event.ADDED_TO_STAGE, onAddedToStage );

		}

		private function onAddedToStage( event:Event ):void
		{
			this.removeEventListener( Event.ADDED_TO_STAGE, onAddedToStage );

			if (m_layer == 1)
			{
				image1 = new Image( Assets.getAtlasGame().getTexture( "BitBackGroundGradient1" ) );
				image2 = new Image( Assets.getAtlasGame().getTexture( "BitBackGroundGradient2" ) );
			}
			else
			{
				image1 = new Image( Assets.getAtlasGame().getTexture( "BitBackGroundCloud1" ) );
				image2 = new Image( Assets.getAtlasGame().getTexture( "BitBackGroundCloud2" ) );
			}

			image1.x = 0;
			image1.y = 0;

			image2.x = image1.x + image2.width;
			image2.y = image1.y;

			this.addChild( image1 );
			this.addChild( image2 );
		}

		public function setCoords( x:int, y:int ):void
		{
//			image1.x = Math.ceil(x*m_parallax);
//			image1.y = Math.ceil(y*m_parallax);
			image1.x = (x * m_parallax);
			image1.y = (y * m_parallax);
			image2.x = image1.x + image2.width;
			image2.y = image1.y;
		}

		public function get parallax():Number
		{
			return m_parallax;
		}

		public function set parallax( value:Number ):void
		{
			m_parallax = value;
		}
	}
}