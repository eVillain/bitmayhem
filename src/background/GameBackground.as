package background
{

	import starling.display.Sprite;
	import starling.events.Event;

	public class GameBackground extends Sprite
	{
		private var bgLayer1:BackgroundLayer;
		private var bgLayer2:BackgroundLayer;
		private var bgLayer3:BackgroundLayer;
		private var bgLayer4:BackgroundLayer;

		private var m_speed:Number;

		public function GameBackground()
		{
			super();
			this.addEventListener( Event.ADDED_TO_STAGE, onAddedToStage );

		}

		private function onAddedToStage():void
		{
			this.removeEventListener( Event.ADDED_TO_STAGE, onAddedToStage );
			bgLayer1 = new BackgroundLayer( 1 );
			bgLayer1.parallax = 0.02;
			this.addChild( bgLayer1 );

			bgLayer2 = new BackgroundLayer( 2 );
			bgLayer2.parallax = 0.2;
			this.addChild( bgLayer2 );

			//this.addEventListener(Event.ENTER_FRAME, onEnterFrame);
		}

		private function onEnterFrame():void
		{
			var date:Date = new Date();
			bgLayer1.setCoords( Math.cos( date.getTime() * 0.0002 ) * 1000, 0 );

			bgLayer2.setCoords( Math.cos( date.getTime() * 0.0002 ) * 1000, 0 );
		}

		public function get speed():Number
		{
			return m_speed;
		}

		public function set speed( value:Number ):void
		{
			m_speed = value;
		}
	}
}