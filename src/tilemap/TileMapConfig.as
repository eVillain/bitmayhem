/**
 * Created by eVillain on 06/09/14.
 */
package tilemap
{

	import robotlegs.bender.extensions.signalCommandMap.api.ISignalCommandMap;
	import robotlegs.bender.framework.api.IConfig;
	import robotlegs.bender.framework.api.IInjector;

	import thirdparty.tmxloader.TMXTileMap;

	import tilemap.controller.CreatePhysicsFromTileDataCommand;
	import tilemap.controller.LoadTileMapCommand;
	import tilemap.model.TileMapModel;
	import tilemap.signals.CreatePhysicsFromTileDataSignal;
	import tilemap.signals.LoadTileMapSignal;
	import tilemap.view.TileMapView;

	public class TileMapConfig implements IConfig
	{

		[Inject]
		public var injector:IInjector;

		[Inject]
		public var signalCommandMap:ISignalCommandMap;

		public function configure():void
		{
			signalCommandMap.map( LoadTileMapSignal ).toCommand( LoadTileMapCommand );
			signalCommandMap.map( CreatePhysicsFromTileDataSignal ).toCommand( CreatePhysicsFromTileDataCommand );

			injector.map( TMXTileMap ).asSingleton();

			injector.map( TileMapView ).asSingleton( true );
			injector.map( TileMapModel ).asSingleton( true );

		}
	}
}
