/**
 * Created by eVillain on 06/09/14.
 */
package tilemap.controller
{

	import flash.geom.Point;

	import physics.constants.CollisionConstants;
	import physics.constants.PhysicsConstants;
	import physics.model.PhysicsModel;
	import physics.utils.PhysicsShapeUtil;

	import robotlegs.bender.bundles.mvcs.Command;

	import tilemap.model.TileMapModel;
	import tilemap.vos.TileMetaDataVO;

	public class CreatePhysicsFromTileDataCommand extends Command
	{
		[Inject]
		public var tileMetaDataVO:TileMetaDataVO;

		[Inject]
		public var tileMapModel:TileMapModel;

		[Inject]
		public var physicsModel:PhysicsModel;

		override public function execute():void
		{
			//trace( "CreatePhysicsFromTileDataCommand: " + tileMetaDataVO + tileMapModel + physicsModel );
			var tileWidth:uint = tileMetaDataVO.tileWidth;
			var tileHeight:uint = tileMetaDataVO.tileHeight;
			var layerWidth:uint = tileMetaDataVO.layerWidth;
			var layerHeight:uint = tileMetaDataVO.layerHeight;

			//trace( tileMetaDataVO.tileWidth + "x" + tileMetaDataVO.tileHeight );
			// Run through all the tiles in this layer
			for (var ly:uint = 0; ly < layerHeight; ly++)
			{
				var startSolidTile:int = -1;

				for (var lx:uint = 0; lx < layerWidth; lx++)
				{
					// Get the data array index for current tile
					var tileIndex:int = (ly * layerWidth) + lx;
					// Get the tile type for the current tile from data
					var gid:int = tileMetaDataVO.data[tileIndex];

					// A gid of 0 means the tile in this layer is empty
					if (gid == 37)	// 37 is the magical number for solid physics collisions
					{
						if (startSolidTile < 0)
						{
							// We have a new solid tile in this row
							startSolidTile = lx;
						}
						else
						{
							// We have successive solid tiles, continue
						}
					}

					if (gid == 0 || lx == layerWidth - 1)
					{
						// The current tile is empty, check if we had a succession of solid tiles
						if (startSolidTile >= 0)
						{
							if (lx == layerWidth - 1)
							{
								lx = layerWidth;
							}
							var numSolidTiles:uint = lx - startSolidTile;

							var boxWidth:Number = numSolidTiles * (tileWidth);
							var boxPosX:Number = (startSolidTile * tileWidth) + (boxWidth / 2);
							// We have some solid tiles we need to add
							var ty:Number = ( ly * tileHeight ) + tileHeight / 2;

							PhysicsShapeUtil.createBox(
									physicsModel.space, new Point( boxPosX, ty ),
									boxWidth,
									tileHeight,
									0.0,
									new Point( 0, 0 ),
									0.0,
									PhysicsConstants.STATIC,
									CollisionConstants.COLLISION_TYPE_DEFAULT,
									null,
									0.0,
									0.5,
									0.1
							);
							startSolidTile = -1;
						}
					}
				}
			}
		}
	}
}
