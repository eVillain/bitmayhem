/**
 * Created by eVillain on 26/10/14.
 */
package tilemap.controller
{

	import robotlegs.bender.bundles.mvcs.Command;

	import starling.events.Event;

	import thirdparty.tmxloader.TMXTileMap;

	import tilemap.model.TileMapModel;
	import tilemap.view.TileMapView;
	import tilemap.vos.LoadTileMapVO;

	public class LoadTileMapCommand extends Command
	{
		[Inject]
		public var tileMapModel:TileMapModel;

		[Inject]
		public var tileMapView:TileMapView;

		[Inject]
		public var tileMap:TMXTileMap;

		[Inject]
		public var vo:LoadTileMapVO;

		override public function execute():void
		{
			if (tileMapModel.isLoaded)
			{
				tileMapModel.unload();
			}

			tileMap.addEventListener( Event.COMPLETE, tileMapModel.addDrawLayers );
			tileMap.load( vo.fileName, vo.folder );
		}
	}
}
