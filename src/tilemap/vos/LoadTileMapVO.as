/**
 * Created by eVillain on 26/10/14.
 */
package tilemap.vos
{

	public class LoadTileMapVO
	{
		private var _fileName:String;
		private var _folder:String;

		public function LoadTileMapVO( fileName:String, folder:String = "" )
		{
			this._fileName = fileName;
			this._folder = folder;
		}

		public function get fileName():String
		{
			return _fileName;
		}

		public function get folder():String
		{
			return _folder;
		}
	}
}
