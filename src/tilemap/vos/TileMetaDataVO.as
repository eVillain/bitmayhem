/**
 * Created by eVillain on 06/09/14.
 */
package tilemap.vos
{

	public class TileMetaDataVO
	{
		public var tileWidth:uint;
		public var tileHeight:uint;

		public var layerWidth:uint;
		public var layerHeight:uint;

		public var data:Array;

		public function TileMetaDataVO( data:Array, tileWidth:uint, tileHeight:uint, layerWidth:uint, layerHeight:uint )
		{
			this.data = data;
			this.tileWidth = tileWidth;
			this.tileHeight = tileHeight;
			this.layerWidth = layerWidth;
			this.layerHeight = layerHeight;
		}
	}
}
