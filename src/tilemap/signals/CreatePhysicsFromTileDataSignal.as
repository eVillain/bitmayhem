/**
 * Created by eVillain on 06/09/14.
 */
package tilemap.signals
{

	import org.osflash.signals.Signal;

	import tilemap.vos.TileMetaDataVO;

	public class CreatePhysicsFromTileDataSignal extends Signal
	{
		public function CreatePhysicsFromTileDataSignal()
		{
			super (TileMetaDataVO);
		}
	}
}
