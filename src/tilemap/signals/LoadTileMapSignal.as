/**
 * Created by eVillain on 26/10/14.
 */
package tilemap.signals
{

	import org.osflash.signals.Signal;

	import tilemap.vos.LoadTileMapVO;

	public class LoadTileMapSignal extends Signal
	{
		public function LoadTileMapSignal()
		{
			super( LoadTileMapVO );
		}
	}
}
