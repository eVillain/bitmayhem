/**
 * Created by eVillain on 06/09/14.
 */
package tilemap.model
{

	import starling.events.Event;

	import thirdparty.tmxloader.TMXTileMap;

	import tilemap.view.TileMapView;

	public class TileMapModel
	{
		[Inject]
		public var tileMapView:TileMapView;

		[Inject]
		public var tileMap:TMXTileMap;

		private var _isLoaded:Boolean = false;

		public function TileMapModel()
		{
		}

		public function addDrawLayers( event:Event ):void
		{
			for (var i:int = 0; i < tileMap.layers().length; i++)
			{
				tileMapView.addChild( tileMap.layers()[i].getHolder() );
			}
			_isLoaded = true;
		}

		public function unload():void
		{
			for (var i:int = 0; i < tileMap.layers().length; i++)
			{
				tileMapView.removeChild( tileMap.layers()[i].getHolder() );
			}
			_isLoaded = false;
		}

		public function get isLoaded():Boolean
		{
			return _isLoaded;
		}
	}
}
