/**
 * Based on work by shaun.mitchell
 */
package thirdparty.tmxloader
{

	import flash.display.Bitmap;
	import flash.events.Event;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.utils.ByteArray;

	import starling.display.Image;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.textures.TextureSmoothing;

	import tilemap.signals.CreatePhysicsFromTileDataSignal;
	import tilemap.vos.TileMetaDataVO;

	public class TMXTileMap extends Sprite
	{
		[Inject]
		public var createPhysicsFromTileDataSignal:CreatePhysicsFromTileDataSignal;

		private var _drawMetaLayer:Boolean = false;

		// The TMX file to load
		private var _fileName:String;
		private var _folderName:String;
		private var _loader:URLLoader;
		private var _mapLoaded:Boolean;
		// XML of TMX file
		private var _TMX:XML;
		// Layers and tilesheet holders
		private var _layers:Vector.<TMXLayer>;
		private var _tilesheets:Vector.<TMXTileSheet>;
		// variables pertaining to map description
		private var _numLayers:uint;
		private var _numTilesets:uint;
		private var _tilelistCount:uint;
		private var _mapWidth:uint;
		private var _tileHeight:uint;
		private var _tileWidth:uint;
		// used to get the correct tile from various tilesheets
		private var _gidLookup:Vector.<uint>;
		private var _embedTilesets:Vector.<Bitmap>;

		public function TMXTileMap():void
		{
			_mapLoaded = false;
			_fileName = "";
			_folderName = "";
			_loader = new URLLoader();
			_numLayers = 0;
			_numTilesets = 0;
			_tilelistCount = 0;
			_mapWidth = 0;
			_tileHeight = 0;
			_tileWidth = 0;

			_layers = new Vector.<TMXLayer>();
			_tilesheets = new Vector.<TMXTileSheet>();
			_gidLookup = new Vector.<uint>();
		}

		public function load( file:String, folder:String = "" ):void
		{
			_fileName = file;
			_folderName = folder;
			trace( "TMXTileMap Loading: " + _folderName + _fileName );

			_loader.addEventListener( flash.events.Event.COMPLETE, loadTilesets );
			_loader.load( new URLRequest( _folderName + _fileName ) );
		}

		public function loadFromEmbed( tmx:XML, tilesets:Vector.<Bitmap> ):void
		{
			_TMX = tmx;
			_embedTilesets = tilesets;

			loadEmbedTilesets();
		}

		// Getters ------------------------------------------
		public function layers():Vector.<TMXLayer>
		{
			return _layers;
		}

		public function tilesheets():Vector.<TMXTileSheet>
		{
			return _tilesheets;
		}

		public function numLayers():uint
		{
			return _numLayers;
		}

		public function numTilesets():uint
		{
			return _numTilesets;
		}

		public function mapWidth():uint
		{
			return _mapWidth;
		}

		public function tileHeight():uint
		{
			return _tileHeight;
		}

		public function tileWidth():uint
		{
			return _tileWidth;
		}

		// End getters --------------------------------------
		// get the number of tilsets from the TMX XML
		private function getNumTilesets():uint
		{
			if (_mapLoaded)
			{
				var count:uint = 0;
				for (var i:int = 0; i < _TMX.children().length(); i++)
				{
					if (_TMX.tileset[i] != null)
					{
						count++;
					}
				}

				trace( count );
				return count;
			}

			return 0;
		}

		// get the number of layers from the TMX XML
		private function getNumLayers():uint
		{
			if (_mapLoaded)
			{
				var count:uint = 0;
				for (var i:int = 0; i < _TMX.children().length(); i++)
				{
					if (_TMX.layer[i] != null)
					{
						count++;
					}
				}

				trace( count );
				return count;
			}
			return 0;
		}

		private function loadTilesets( event:flash.events.Event ):void
		{
			_loader.removeEventListener( flash.events.Event.COMPLETE, loadTilesets );

			trace( "loading tilesets from file" );
			_mapLoaded = true;

			_TMX = new XML( _loader.data );

			if (_TMX)
			{
				_mapWidth = _TMX.@width;
				_tileHeight = _TMX.@tileheight;
				_tileWidth = _TMX.@tilewidth;


				_numLayers = getNumLayers();
				_numTilesets = getNumTilesets();
				// _TMX.properties.property[1].@value;

				var tileSheet:TMXTileSheet = new TMXTileSheet();
				tileSheet.loadTileSheet(
						_TMX.tileset[_tilelistCount].@name,
						_folderName + _TMX.tileset[_tilelistCount].image.@source,
						_TMX.tileset[_tilelistCount].@tilewidth,
						_TMX.tileset[_tilelistCount].@tileheight,
						_TMX.tileset[_tilelistCount].@spacing,
						_TMX.tileset[_tilelistCount].@margin,
						_TMX.tileset[_tilelistCount].@firstgid - 1
				);
				tileSheet.addEventListener( starling.events.Event.COMPLETE, loadRemainingTilesets );
				_tilesheets.push( tileSheet );
				_gidLookup.push( _TMX.tileset[_tilelistCount].@firstgid );
			}
		}

		private function loadEmbedTilesets():void
		{
			trace( "loading embedded tilesets" );
			_mapLoaded = true;

			if (_TMX)
			{
				_mapWidth = _TMX.@width;
				_tileHeight = _TMX.@tileheight;
				_tileWidth = _TMX.@tilewidth;

				trace( "map width" + _mapWidth );

				_numLayers = getNumLayers();
				_numTilesets = getNumTilesets();
				trace( _numTilesets );
				// _TMX.properties.property[1].@value;

				for (var i:int = 0; i < _numTilesets; i++)
				{
					var tileSheet:TMXTileSheet = new TMXTileSheet();
					trace( _TMX.tileset[i].@name, _embedTilesets[i], _TMX.tileset[i].@tilewidth, _TMX.tileset[i].@tileheight, _TMX.tileset[i].@firstgid - 1 );
					tileSheet.loadEmbedTileSheet( _TMX.tileset[i].@name, _embedTilesets[i], _TMX.tileset[i].@tilewidth, _TMX.tileset[i].@tileheight, _TMX.tileset[i].@firstgid - 1 );
					_tilesheets.push( tileSheet );
					_gidLookup.push( _TMX.tileset[i].@firstgid );
				}

				loadMapData();
			}
		}

		private function loadRemainingTilesets( event:starling.events.Event ):void
		{
			event.target.removeEventListener( starling.events.Event.COMPLETE, loadRemainingTilesets );

			_tilelistCount++;
			if (_tilelistCount >= _numTilesets)
			{
				trace( "done loading tilelists" );
				loadMapData();
			}
			else
			{
				trace( _TMX.tileset[_tilelistCount].@name );
				var tileSheet:TMXTileSheet = new TMXTileSheet();
				tileSheet.loadTileSheet( _TMX.tileset[_tilelistCount].@name,
										 _folderName + _TMX.tileset[_tilelistCount].image.@source,
										 _TMX.tileset[_tilelistCount].@tilewidth,
										 _TMX.tileset[_tilelistCount].@tileheight,
										 _TMX.tileset[_tilelistCount].@spacing,
										 _TMX.tileset[_tilelistCount].@margin,
										 _TMX.tileset[_tilelistCount].@firstgid - 1 );
				tileSheet.addEventListener( starling.events.Event.COMPLETE, loadRemainingTilesets );
				_gidLookup.push( _TMX.tileset[_tilelistCount].@firstgid );
				_tilesheets.push( tileSheet );
			}
		}

		private function loadMapData():void
		{
			if (_mapLoaded)
			{
				for (var i:int = 0; i < _numLayers; i++)
				{
					var ba:ByteArray = Base64.decode( _TMX.layer[i].data );
					ba.uncompress();

					var data:Array = new Array();

					for (var j:int = 0; j < ba.length; j += 4)
					{
						// Get the grid ID

						var a:int = ba[j];
						var b:int = ba[j + 1];
						var c:int = ba[j + 2];
						var d:int = ba[j + 3];

						var gid:int = a | b << 8 | c << 16 | d << 24;
						data.push( gid );
					}

					var layerName:String = _TMX.layer[i].@name;
					var layerWidth:uint = _TMX.layer[i].@width;
					var layerHeight:uint = _TMX.layer[i].@height;

					var tmxLayer:TMXLayer = new TMXLayer( data );


					if ( layerName == "Meta" ) // Meta layer contains physics etc.
					{
						trace( "loading Meta layer...");
						// Hide meta layer
						tmxLayer.visible = _drawMetaLayer;

						// Dispatch VO with data to physics factory
						createPhysicsFromTileDataSignal.dispatch( new TileMetaDataVO(data, _tileWidth, _tileHeight, layerWidth, layerHeight) );
					}
					else	// Regular graphics layer, load data
					{
						trace( "loading map layer: " + layerName );
					}

					_layers.push( tmxLayer );
				}

				drawLayers();
			}
		}

		// draw the layers into a holder contained in a TMXLayer object
		private function drawLayers():void
		{
			for (var i:int = 0; i < _numLayers; i++)
			{
				if (_layers[i].visible == false)
				{
					continue;
				}
				var row:int = 0;
				var col:int = 0;
				for (var j:int = 0; j < _layers[i].getData().length; j++)
				{
					if (col > (_mapWidth - 1) * _tileWidth)
					{
						col = 0;
						row += _tileHeight;
					}

					if (_layers[i].getData()[j] != 0)
					{
						var img:Image = new Image( _tilesheets[findTileSheet( _layers[i].getData()[j] )].textureAtlas.getTexture( String( _layers[i].getData()[j] ) ) );
						img.smoothing = TextureSmoothing.NONE;
						img.x = col;
						img.y = row;
						_layers[i].getHolder().addChild( img );
					}

					col += _tileWidth;
				}
			}

			// notify that the load is complete
			dispatchEvent( new starling.events.Event( starling.events.Event.COMPLETE ) );
		}

		private function findTileSheet( id:uint ):int
		{
			var value:int = 0;
			var theOne:int;
			for (var i:int = 0; i < _tilesheets.length; i++)
			{
				if (_tilesheets[i].textureAtlas.getTexture( String( id ) ) != null)
				{
					theOne = i;
				}
				else
				{
					value = i;
				}
			}
			return theOne;
		}
	}
}
