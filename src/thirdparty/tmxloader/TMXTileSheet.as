/**
 * Based on work by shaun.mitchell
 *
 * edited by The Drudgerist to include margin and spacing
 */
package thirdparty.tmxloader
{

	import flash.display.Bitmap;
	import flash.display.DisplayObject;
	import flash.display.Loader;
	import flash.events.Event;
	import flash.net.URLRequest;

	import starling.display.Sprite;
	import starling.events.Event;
	import starling.textures.Texture;
	import starling.textures.TextureAtlas;
	import starling.textures.TextureSmoothing;


	public class TMXTileSheet extends Sprite
	{
		// the name and file paths
		private var _name:String;
		private var _sheetFilename:String;
		// texture, atlas and loader
		private var _sheet:Bitmap;
		private var _textureAtlas:TextureAtlas;
		private var _imageLoader:Loader = new Loader();
		private var _startID:uint;
		private var _tileHeight:uint;
		private var _tileWidth:uint;
		private var _tileMargin:uint;
		private var _tileSpacing:uint;
		private var _embedded:Boolean;

		public function TMXTileSheet():void
		{
		}

		public function loadTileSheet(name:String, sheetFile:String, tileWidth:uint, tileHeight:uint, tileSpacing:uint, tileMargin:uint, startID:uint):void
		{
			_embedded = false;
			_name = name;
			_sheetFilename = sheetFile;
			_startID = startID;

			_tileHeight = tileHeight;
			_tileWidth = tileWidth;
			_tileSpacing = tileSpacing ? tileSpacing : 0;
			_tileMargin = tileMargin ? tileMargin : 0;

			trace("creating TMX tilesheet: " + _sheetFilename + " spacing: " + _tileSpacing.toString() + " margin: "+  tileMargin.toString());

			_imageLoader.contentLoaderInfo.addEventListener(flash.events.Event.COMPLETE, loadSheet);
			_imageLoader.load(new URLRequest(_sheetFilename));
		}

		public function loadEmbedTileSheet(name:String, img:Bitmap, tileWidth:uint, tileHeight:uint, startID:uint):void
		{
			trace("creating TMX tilesheet");
			_embedded = true;
			_name = name;
			_startID = startID;

			_sheet = img;

			_tileHeight = tileHeight;
			_tileWidth = tileWidth;

			loadAtlas();
		}

		/*
		Load the image file needed for this tilesheet
		 */
		private function loadSheet(event:flash.events.Event):void
		{
			var sprite:DisplayObject = _imageLoader.content;
			_sheet = Bitmap(sprite);
			_sheet.smoothing = TextureSmoothing.NONE;

			loadAtlas();
		}

		/*
		dynamically create a texture atlas to look up tiles
		 */
		private function loadAtlas():void
		{
			var tilePixelsX:uint = _tileWidth + _tileSpacing;
			var tilePixelsY:uint = _tileHeight + _tileSpacing;
			var imagePixelsY:uint = (_sheet.height - (_tileMargin));
			var imagePixelsX:uint = (_sheet.width - (_tileMargin));
			var numRows:uint = imagePixelsY / tilePixelsY;
			var numCols:uint = imagePixelsX / tilePixelsX;
			trace("loading atlas: " + numRows.toString() + "x" + numCols.toString() + "tiles");

			var id:int = _startID;

			var xml:XML = <Atlas></Atlas>;

			xml.appendChild(<TextureAtlas imagePath={_sheetFilename}></TextureAtlas>);

			for (var i:int = 0; i < numRows; i++)
			{
				for (var j:int = 0; j < numCols; j++)
				{
					var tileX:uint = (_tileMargin + (j * (_tileWidth + _tileSpacing)) );
					var tileY:uint = (_tileMargin + (i * (_tileHeight + _tileSpacing)) );

					id++;
					xml.child("TextureAtlas").appendChild(<SubTexture name={id} x = {tileX} y={tileY} width={_tileWidth} height={_tileHeight}/>);
				}
			}

			var newxml:XML = XML(xml.TextureAtlas);

			//trace(newxml);

			_textureAtlas = new TextureAtlas(Texture.fromBitmap(_sheet), newxml);

			trace("done with atlas, dispatching");
			dispatchEvent(new starling.events.Event(starling.events.Event.COMPLETE));
		}

		public function get sheet():Bitmap
		{
			return _sheet;
		}

		public function get textureAtlas():TextureAtlas
		{
			return _textureAtlas;
		}
	}
}
