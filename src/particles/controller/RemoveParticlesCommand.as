/**
 * Created by eVillain on 22/03/15.
 */
package particles.controller
{

	import game.view.GameView;

	import particles.mediator.ParticlesMediator;

	import robotlegs.bender.bundles.mvcs.Command;

	import starling.core.Starling;
	import starling.extensions.PDParticleSystem;

	public class RemoveParticlesCommand extends Command
	{
		[Inject]
		public var particlesModel:ParticlesMediator;

		[Inject]
		public var particleSystem:PDParticleSystem;

		[Inject]
		public var gameView:GameView;

		override public function execute():void
		{
			particleSystem.stop();
			Starling.juggler.remove( particleSystem );
			gameView.removeChild( particleSystem, true );
		}
	}
}
