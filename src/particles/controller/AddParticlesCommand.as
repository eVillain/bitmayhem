/**
 * Created by eVillain on 22/03/15.
 */
package particles.controller
{

	import game.view.GameView;

	import particles.constants.ParticleConstants;
	import particles.mediator.ParticlesMediator;
	import particles.vos.AddParticleSystemVO;

	import robotlegs.bender.bundles.mvcs.Command;

	import starling.core.Starling;
	import starling.extensions.PDParticleSystem;
	import starling.textures.Texture;

	public class AddParticlesCommand extends Command
	{
		[Inject]
		public var particlesMediator:ParticlesMediator;

		[Inject]
		public var addParticleSystemVO:AddParticleSystemVO;

		[Inject]
		public var gameView:GameView;

		override public function execute():void
		{
			// Instantiate embedded objects
			var configClass:Class = configForType( addParticleSystemVO.type );
			var psConfig:XML = XML( new configClass );
			var psTexture:Texture = Texture.fromBitmap( new Assets.ParticleTexture() );

			// We can get the duration from our XML
			var duration:Number = parseFloat( psConfig.duration.attribute( "value" ) );

			// Create FFParticleSystem options
			//var sysOpt:SystemOptions = SystemOptions.fromXML(psConfig, psTexture);

			// create particle system
			//var ps:FFParticleSystem = new FFParticleSystem( sysOpt );
			var ps:PDParticleSystem = new PDParticleSystem( psConfig, psTexture );
			ps.x = addParticleSystemVO.positionX;
			ps.y = addParticleSystemVO.positionY;

			// Set scaling and flip Y axis due to different coordinate systems
			ps.scaleX = ParticleConstants.PARTICLES_SCALING;
			ps.scaleY = -ParticleConstants.PARTICLES_SCALING;

			// Put the reference into the VO in case you need to access it from outside
			addParticleSystemVO.system = ps;

			// Pass the system to the mediator to hook up removal event
			particlesMediator.addParticleSystem( ps );

			// add it to the stage and the juggler
			gameView.addChild( ps );
			//Starling.current.stage.addChild( ps );
			Starling.juggler.add( ps );

			// change position where particles are emitted (offset)
			ps.emitterX = addParticleSystemVO.offsetX;
			ps.emitterY = addParticleSystemVO.offsetY;
			ps.emitAngle = -addParticleSystemVO.rotation;

			// start emitting particles
			ps.start( duration );
		}

		private function configForType( type:String ):Class
		{
			switch (type)
			{
				case ParticleConstants.PARTICLES_MUZZLE_FLASH:
					return Assets.ParticleMuzzleFlashConfig;
					break;
				case ParticleConstants.PARTICLES_BLOOD:
					return Assets.ParticleBloodConfig;
					break;
				case ParticleConstants.PARTICLES_SPARKS:
					return Assets.ParticleSparksConfig;
					break;
				default:
					return Assets.ParticleBloodConfig;
					break;
			}
			return Assets.ParticleBloodConfig;
		}
	}
}
