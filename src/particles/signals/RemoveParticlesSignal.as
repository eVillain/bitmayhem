/**
 * Created by eVillain on 22/03/15.
 */
package particles.signals
{

	import org.osflash.signals.Signal;

	import starling.extensions.PDParticleSystem;

	public class RemoveParticlesSignal extends Signal
	{
		public function RemoveParticlesSignal()
		{
			super( PDParticleSystem );
		}
	}
}
