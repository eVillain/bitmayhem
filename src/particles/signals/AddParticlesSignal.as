/**
 * Created by eVillain on 22/03/15.
 */
package particles.signals
{

	import org.osflash.signals.Signal;

	import particles.vos.AddParticleSystemVO;

	public class AddParticlesSignal extends Signal
	{
		public function AddParticlesSignal()
		{
			super( AddParticleSystemVO );
		}
	}
}
