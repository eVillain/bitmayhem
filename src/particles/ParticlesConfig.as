/**
 * Created by eVillain on 22/03/15.
 */
package particles
{

	import particles.controller.AddParticlesCommand;
	import particles.controller.RemoveParticlesCommand;
	import particles.mediator.ParticlesMediator;
	import particles.signals.AddParticlesSignal;
	import particles.signals.RemoveParticlesSignal;

	import robotlegs.bender.extensions.signalCommandMap.api.ISignalCommandMap;
	import robotlegs.bender.framework.api.IConfig;
	import robotlegs.bender.framework.api.IInjector;

	public class ParticlesConfig implements IConfig
	{
		[Inject]
		public var injector:IInjector;

		[Inject]
		public var signalCommandMap:ISignalCommandMap;

		public function configure():void
		{
			injector.map( ParticlesMediator ).asSingleton();

			signalCommandMap.map( AddParticlesSignal ).toCommand( AddParticlesCommand );
			signalCommandMap.map( RemoveParticlesSignal ).toCommand( RemoveParticlesCommand );
		}
	}
}
