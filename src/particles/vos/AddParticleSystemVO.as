/**
 * Created by eVillain on 22/03/15.
 */
package particles.vos
{

	import starling.extensions.PDParticleSystem;

	public class AddParticleSystemVO
	{
		public var type:String;	// Use ParticleConstants
		public var positionX:Number;
		public var positionY:Number;
		public var offsetX:Number;
		public var offsetY:Number;
		public var rotation:Number;

		public var system:PDParticleSystem;

		public function AddParticleSystemVO( type:String, positionX:Number, positionY:Number, offsetX:Number = 0.0, offsetY:Number = 0.0, rotation:Number = 0.0 )
		{
			this.type = type;
			this.positionX = positionX;
			this.positionY = positionY;
			this.offsetX = offsetX;
			this.offsetY = offsetY;
			this.rotation = rotation;
		}
	}
}
