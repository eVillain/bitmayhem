/**
 * Created by eVillain on 22/03/15.
 */
package particles.constants
{

	public class ParticleConstants
	{
		public static const PARTICLES_SCALING:Number = 0.25;

		public static const PARTICLES_MUZZLE_FLASH:String = "PARTICLES_MUZZLE_FLASH";
		public static const PARTICLES_BLOOD:String = "PARTICLES_BLOOD";
		public static const PARTICLES_SPARKS:String = "PARTICLES_SPARKS";
	}
}
