/**
 * Created by eVillain on 22/03/15.
 */
package particles.mediator
{

	import particles.signals.RemoveParticlesSignal;

	import starling.events.Event;
	import starling.extensions.PDParticleSystem;

	public class ParticlesMediator
	{
		[Inject]
		public var removeParticlesSignal:RemoveParticlesSignal;

		[PostConstruct]
		public function init():void
		{
			trace( "------> ParticlesModel post construct init" );
			// Only if using FFParticleSystem
			// Init particle systems once before creating the first instance
			// creates a particle pool of 1024
			// creates four vertex buffers which can batch up to 512 particles each
			//FFParticleSystem.init(1024, false, 512, 4);
		}

		public function addParticleSystem( sys:PDParticleSystem ):void
		{
			sys.addEventListener( Event.COMPLETE, onParticlesFinished );
		}

		private function onParticlesFinished( event:Event ):void
		{
			var ps:PDParticleSystem = event.target as PDParticleSystem;
			removeParticlesSignal.dispatch( ps );
		}
	}
}
