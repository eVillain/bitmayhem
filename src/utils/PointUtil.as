/**
 * Created by eVillain on 07/09/14.
 */
package utils
{

	import flash.geom.Point;

	public class PointUtil
	{
		public static function scalePoint( point:Point, scalar:Number ):void
		{
			point.x *= scalar;
			point.y *= scalar;
		}
	}
}
