/**
 * Created by eVillain on 07/09/14.
 */
package resolution
{

	import application.signals.AppResizeSignal;

	import resolution.controller.UpdateResolutionCommand;
	import resolution.model.ResolutionModel;
	import resolution.signals.ResolutionChangedSignal;

	import robotlegs.bender.extensions.signalCommandMap.api.ISignalCommandMap;
	import robotlegs.bender.framework.api.IConfig;
	import robotlegs.bender.framework.api.IInjector;

	public class ResolutionConfig implements IConfig
	{
		[Inject]
		public var injector:IInjector;

		[Inject]
		public var signalCommandMap:ISignalCommandMap;

		public function configure():void
		{
			signalCommandMap.map( AppResizeSignal ).toCommand( UpdateResolutionCommand );
			injector.map( ResolutionChangedSignal ).asSingleton( true );
			injector.map( ResolutionModel ).asSingleton( true );
		}
	}
}
