/**
 * Created by eVillain on 26/10/14.
 */
package resolution.controller
{

	import application.vos.AppResizeVO;

	import flash.geom.Rectangle;

	import resolution.model.ResolutionModel;
	import resolution.signals.ResolutionChangedSignal;
	import resolution.vos.ResolutionChangeVO;

	import robotlegs.bender.bundles.mvcs.Command;

	import starling.core.Starling;

	public class UpdateResolutionCommand extends Command
	{
		[Inject]
		public var vo:AppResizeVO;

		[Inject]
		public var resolutionModel:ResolutionModel;

		[Inject]
		public var resolutionChangedSignal:ResolutionChangedSignal;

		override public function execute():void
		{
			// Get resolution scaling based on initial resolution
			var scaleX:Number = vo.width / resolutionModel.initialWidth;
			var scaleY:Number = vo.height / resolutionModel.initialHeight;

			// Pick biggest scale
			resolutionModel.renderScale = scaleX > scaleY ? scaleX : scaleY;

			// Save new resolution width and height values
			resolutionModel.currentWidth = vo.width;
			resolutionModel.currentHeight = vo.height;

			//trace("ResolutionModel - current: " + _currentWidth + "x" + _currentHeight + ", event: " + width + "x" + height);

			applyStarlingResolution( vo.width, vo.height );

			// Dispatch new resolution in signal
			resolutionChangedSignal.dispatch( new ResolutionChangeVO( resolutionModel.currentWidth, resolutionModel.currentHeight, resolutionModel.renderScale ) );
		}

		private static function applyStarlingResolution( width:int, height:int ):void
		{
			// Set rectangle dimensions to resize the Starling viewport:
			Starling.current.viewPort = new Rectangle( 0, 0, width, height );
			// Assign the new stage width and height:
			Starling.current.stage.stageWidth = width;
			Starling.current.stage.stageHeight = height;
		}
	}
}
