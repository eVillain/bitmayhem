/**
 * Created by eVillain on 07/09/14.
 */
package resolution.vos
{

	public class ResolutionChangeVO
	{
		public var width:uint;
		public var height:uint;
		public var renderScale:Number;

		public function ResolutionChangeVO( width:uint, height:uint, renderScale:Number )
		{
			this.width = width;
			this.height = height;
			this.renderScale = renderScale;
		}
	}
}
