/**
 * Created by eVillain on 07/09/14.
 */
package resolution.signals
{

	import org.osflash.signals.Signal;

	import resolution.vos.ResolutionChangeVO;

	public class ResolutionChangedSignal extends Signal
	{
		public function ResolutionChangedSignal()
		{
			super( ResolutionChangeVO );
		}
	}
}
