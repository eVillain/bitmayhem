/**
 * Created by eVillain on 07/09/14.
 */
package resolution.model
{

	import application.AppCore;

	public class ResolutionModel
	{
		private var _initialWidth:uint;
		private var _initialHeight:uint;
		private var _currentWidth:uint;
		private var _currentHeight:uint;

		private var _renderScale:Number = 1.0;

		public function ResolutionModel()
		{
		}

		[PostConstruct]
		public function init():void
		{
			_currentWidth = AppCore.initialWidth;
			_currentHeight = AppCore.initialHeight;
			_initialWidth = AppCore.initialWidth;
			_initialHeight = AppCore.initialHeight;
		}

		public function get currentWidth():uint
		{
			return _currentWidth;
		}

		public function get currentHeight():uint
		{
			return _currentHeight;
		}

		public function get renderScale():Number
		{
			return _renderScale;
		}

		public function get initialWidth():uint
		{
			return _initialWidth;
		}

		public function get initialHeight():uint
		{
			return _initialHeight;
		}

		public function set renderScale( renderScale:Number ):void
		{
			_renderScale = renderScale;
		}

		public function set currentWidth( currentWidth:uint ):void
		{
			_currentWidth = currentWidth;
		}

		public function set currentHeight( currentHeight:uint ):void
		{
			_currentHeight = currentHeight;
		}
	}
}
