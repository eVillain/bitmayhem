package
{

	import flash.display.Bitmap;
	import flash.utils.Dictionary;

	import starling.textures.Texture;
	import starling.textures.TextureAtlas;

	public class Assets
	{
		[Embed(source="../media/gfx/BitMayhemTitle.png")]
		public static const BMTitle:Class;

		[Embed(source="../media/gfx/BitMayhemUI.png")]
		public static const BMUIAtlas:Class;

		[Embed(source="../media/gfx/BitMayhemUI.xml", mimeType="application/octet-stream")]
		public static const BMUIXML:Class;

		[Embed(source="../media/gfx/BitMayhemSprites.png")]
		public static const BMSpriteAtlas:Class;

		[Embed(source="../media/gfx/BitMayhemSprites.xml", mimeType="application/octet-stream")]
		public static const BMSpriteXML:Class;

		// Embed particle system configuration XMLs
		[Embed(source="../media/particles/MuzzleFlash.pex", mimeType="application/octet-stream")]
		public static const ParticleMuzzleFlashConfig:Class;

		[Embed(source="../media/particles/Blood.pex", mimeType="application/octet-stream")]
		public static const ParticleBloodConfig:Class;

		[Embed(source="../media/particles/Sparks.pex", mimeType="application/octet-stream")]
		public static const ParticleSparksConfig:Class;

		// Embed particle textures
		[Embed(source="../media/sfx/Pistol.mp3")]
		public static const PistolSound:Class;

		// Embed sound effects
		[Embed(source="../media/particles/particleTexture.png")]
		public static const ParticleTexture:Class;

		private static var gameTextures:Dictionary = new Dictionary;
		private static var gameTextureAtlas:TextureAtlas;
		private static var uiTextureAtlas:TextureAtlas;

		public static function getAtlasGame():TextureAtlas
		{
			if (gameTextureAtlas == null)
			{
				var texture:Texture = getTexture( "BMSpriteAtlas" );
				var xml:XML = XML( new BMSpriteXML() );
				gameTextureAtlas = new TextureAtlas( texture, xml );
			}
			return gameTextureAtlas;
		}

		public static function getAtlasUI():TextureAtlas
		{
			if (uiTextureAtlas == null)
			{
				var texture:Texture = getTexture( "BMUIAtlas" );
				var xml:XML = XML( new BMUIXML() );
				uiTextureAtlas = new TextureAtlas( texture, xml );
			}
			return uiTextureAtlas;
		}

		public static function getTexture( name:String ):Texture
		{
			if (gameTextures[name] == undefined)
			{
				var bitmap:Bitmap = new Assets[name]();
				gameTextures[name] = Texture.fromBitmap( bitmap );
			}
			return gameTextures[name];
		}
	}
}