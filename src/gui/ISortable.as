/**
 * Created by Kevin on 21.10.2014.
 */
package gui
{

	public interface ISortable
	{
		function get name():String;

		function get sortOrder():int;
	}
}
