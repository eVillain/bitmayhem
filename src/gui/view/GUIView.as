/**
 * ...
 * @author KS
 */
package gui.view
{

	import application.signals.StartSinglePlayerSignal;

	import camera.signals.ToggleCameraElasticitySignal;

	import feathers.controls.Button;
	import feathers.controls.Check;

	import gui.signals.HideGUISignal;
	import gui.signals.SetDebugTextSignal;
	import gui.signals.ShowGUISignal;
	import gui.signals.UpdateInventoryAutoSortSignal;

	import starling.display.Sprite;
	import starling.events.Event;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.text.TextField;

	public class GUIView extends Sprite
	{
		[Inject]
		public var showGUISignal:ShowGUISignal;

		[Inject]
		public var hideGUISignal:HideGUISignal;

		[Inject]
		public var updateInventoryAutoSortSignal:UpdateInventoryAutoSortSignal;

		[Inject]
		public var startSinglePlayerSignal:StartSinglePlayerSignal;

		[Inject]
		public var setDebugTextSignal:SetDebugTextSignal;

		[Inject]
		public var toggleCameraElasticitySignal:ToggleCameraElasticitySignal;

		// Main menu UI
		private var _playButton:Button;

		// In-Game UI
		private var _cameraButton:Button;
		private var _playerInventorySortBtn:Button;
		private var _autoSortPlayerInventoryCheckbox:Check;

		private var _textField:TextField;
		private var _joinGameButton:Button;

		public function GUIView()
		{
		}

		[PostConstruct]
		public function init():void
		{
			showGUISignal.add( show );
			hideGUISignal.add( hide );
			setDebugTextSignal.add( onDebugTextChange );
		}

		public function onTouch( event:TouchEvent ):void
		{
			var touch:Touch = event.getTouch( this );

			if (touch && touch.phase == TouchPhase.ENDED)
			{
				switch (touch.target)
				{
					case _autoSortPlayerInventoryCheckbox:
						updateInventoryAutoSortSignal.dispatch();
						break;
				}
			}
		}

		public function onButtonClicked( event:Event ):void
		{
			var clickedButton:Button = event.target as Button;

			if (clickedButton == _playButton)
			{
				startSinglePlayerSignal.dispatch();
			}
			else if (clickedButton == _cameraButton)
			{
				toggleCameraElasticitySignal.dispatch();
			}

			// Remove focus from button otherwise space bar key will toggle it again
			if (clickedButton.focusManager)
			{
				clickedButton.focusManager.focus = null;
			}
		}

		public function show():void
		{
			this.visible = true;
		}

		public function hide():void
		{
			this.visible = false;
		}

		public function get playerInventorySortBtn():Button
		{
			return _playerInventorySortBtn;
		}

		public function set playerInventorySortBtn( value:Button ):void
		{
			_playerInventorySortBtn = value;
		}

		public function get autoSortPlayerInventoryCheckbox():Check
		{
			return _autoSortPlayerInventoryCheckbox;
		}

		public function set autoSortPlayerInventoryCheckbox( value:Check ):void
		{
			_autoSortPlayerInventoryCheckbox = value;
		}

		public function get playButton():Button
		{
			return _playButton;
		}

		public function set playButton( value:Button ):void
		{
			_playButton = value;
		}

		public function get textField():TextField
		{
			return _textField;
		}

		public function set textField( value:TextField ):void
		{
			_textField = value;
		}

		public function get joinGameButton():Button
		{
			return _joinGameButton;
		}

		public function set joinGameButton( value:Button ):void
		{
			_joinGameButton = value;
		}

		private function onDebugTextChange( text:String ):void
		{
			if (_textField)
			{
				_textField.text = text;
			}
		}

		public function get cameraButton():Button
		{
			return _cameraButton;
		}

		public function set cameraButton( value:Button ):void
		{
			_cameraButton = value;
		}
	}
}
