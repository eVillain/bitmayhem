/**
 * Created by eVillain on 25/10/14.
 */
package gui.controller
{

	import gui.help.HelpScreen;
	import gui.model.GUIModel;
	import gui.view.GUIView;

	import robotlegs.bender.bundles.mvcs.Command;

	public class AddHelpCommand extends Command
	{
		[Inject]
		public var guiModel:GUIModel;

		[Inject]
		public var guiView:GUIView;

		override public function execute():void
		{
			guiModel.help = new HelpScreen();
			guiModel.help.x = 100;
			guiModel.help.y = 10;
			guiView.addChild( guiModel.help );
			guiModel.help.init();
		}
	}
}
