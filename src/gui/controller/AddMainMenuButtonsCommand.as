/**
 * Created by eVillain on 25/10/14.
 */
package gui.controller
{

	import feathers.controls.Button;

	import gui.view.GUIView;

	import robotlegs.bender.bundles.mvcs.Command;

	import starling.events.Event;

	public class AddMainMenuButtonsCommand extends Command
	{
		[Inject]
		public var guiView:GUIView;

		override public function execute():void
		{
			guiView.playButton = new Button();
			guiView.playButton.label = "Click Me";

			guiView.playButton.addEventListener( Event.TRIGGERED, guiView.onButtonClicked );

			guiView.addChild( guiView.playButton );

			//the button won't have a width and height until it "validates". it
			//will validate on its own before the next frame is rendered by
			//Starling, but we want to access the dimension immediately, so tell
			//it to validate right now.
			guiView.playButton.validate();

			//center the button on screen
			guiView.playButton.x = (guiView.stage.stageWidth - guiView.playButton.width) / 2;
			guiView.playButton.y = (guiView.stage.stageHeight - guiView.playButton.height) / 2;
		}
	}
}
