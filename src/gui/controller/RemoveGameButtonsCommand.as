/**
 * Created by eVillain on 25/10/14.
 */
package gui.controller
{

	import application.states.SinglePlayerState;

	import gui.model.GUIModel;
	import gui.view.GUIView;

	import robotlegs.bender.bundles.mvcs.Command;

	import starling.events.Event;
	import starling.events.TouchEvent;

	public class RemoveGameButtonsCommand extends Command
	{
		[Inject]
		public var guiModel:GUIModel;

		[Inject]
		public var guiView:GUIView;

		[Inject]
		public var singlePlayer:SinglePlayerState;

		override public function execute():void
		{
			if (!singlePlayer.joined)
			{
				guiView.joinGameButton.removeEventListener( Event.TRIGGERED, singlePlayer.onJoinGame );
				guiView.removeChild( guiView.joinGameButton );
				guiView.joinGameButton = null;
			}
			else
			{
				guiView.playerInventorySortBtn = null;
				guiView.autoSortPlayerInventoryCheckbox.removeEventListener( TouchEvent.TOUCH, guiView.onTouch );
				guiView.autoSortPlayerInventoryCheckbox = null;

				guiView.removeChild( guiView.cameraButton );
				guiView.cameraButton = null;
			}
		}

	}
}
