/**
 * Created by eVillain on 26/10/14.
 */
package gui.controller
{

	import gui.SortControl;
	import gui.constants.ColorConstants;
	import gui.inventory.Inventory;
	import gui.inventory.InventoryItemVO;
	import gui.model.GUIModel;
	import gui.view.GUIView;

	import robotlegs.bender.bundles.mvcs.Command;
	import robotlegs.bender.framework.api.IInjector;

	public class AddPlayerInventoryCommand extends Command
	{
		[Inject]
		public var guiModel:GUIModel;

		[Inject]
		public var guiView:GUIView;

		[Inject]
		public var injector:IInjector;

		override public function execute():void
		{
			var itemsPerRow:int = 5;
			var columnWidth:Number = 30;
			var rowHeight:Number = 80;
			var backgroundColor:Number = ColorConstants.COLOR_DARK_GREY;

			// We are manually creating the inventories so RobotLegs has no knowledge of them
			guiModel.playerInventory = new Inventory( new Vector.<InventoryItemVO>(), itemsPerRow, columnWidth, rowHeight, "_playerInventory", backgroundColor, guiModel.autoSortPlayerInventory );
			// Manually fulfill injections in inventory
			injector.injectInto( guiModel.playerInventory );

			guiModel.playerInventory.addSorter( guiView.playerInventorySortBtn, SortControl.sortBySortOrder );

			guiModel.playerInventory.addChild( guiView.playerInventorySortBtn );
			guiModel.playerInventory.addChild( guiView.autoSortPlayerInventoryCheckbox );

			guiView.playerInventorySortBtn.y = -30;
			guiView.autoSortPlayerInventoryCheckbox.y = -30;

			guiModel.playerInventory.y = 400;
			guiView.addChild( guiModel.playerInventory );
		}
	}
}
