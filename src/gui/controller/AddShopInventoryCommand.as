/**
 * Created by eVillain on 25/10/14.
 */
package gui.controller
{

	import gui.constants.ColorConstants;
	import gui.inventory.Inventory;
	import gui.inventory.InventoryItemFactory;
	import gui.inventory.InventoryItemVO;
	import gui.inventory.fakeEnum.InventoryItemEnumType;
	import gui.model.GUIModel;
	import gui.view.GUIView;

	import robotlegs.bender.bundles.mvcs.Command;
	import robotlegs.bender.framework.api.IInjector;

	public class AddShopInventoryCommand extends Command
	{
		[Inject]
		public var guiModel:GUIModel;

		[Inject]
		public var guiView:GUIView;

		[Inject]
		public var injector:IInjector;

		override public function execute():void
		{
			var itemsPerRow:int = 5;
			var columnWidth:Number = 30;
			var rowHeight:Number = 80;
			var backgroundColor:Number = ColorConstants.COLOR_DARK_GREY;
			var inventoryItemVOs:Vector.<InventoryItemVO> = getShopInventoryItemVOs();

			// We are manually creating the inventories so RobotLegs has no knowledge of them
			guiModel.shopInventory = new Inventory( inventoryItemVOs, itemsPerRow, columnWidth, rowHeight, "_shopInventory", backgroundColor, guiModel.autoSortShopInventory );
			// Manually fulfill injections in inventory
			injector.injectInto( guiModel.shopInventory );

			guiModel.shopInventory.y = 200;
			guiView.addChild( guiModel.shopInventory );
		}

		private function getShopInventoryItemVOs():Vector.<InventoryItemVO>
		{
			var vos:Vector.<InventoryItemVO> = new Vector.<InventoryItemVO>();
			var vo:InventoryItemVO;
			for (var i:uint = 0; i < guiModel.numButtons; i++)
			{
				vo = InventoryItemFactory.getInventoryButtonVO( InventoryItemEnumType.Inventory_Item_Weapon, "Weapon" + (i + 1), i );
				vos.push( vo );
			}
			return vos;
		}
	}
}
