/**
 * Created by eVillain on 25/10/14.
 */
package gui.controller
{

	import gui.model.GUIModel;
	import gui.view.GUIView;

	import robotlegs.bender.bundles.mvcs.Command;

	public class UpdateInventoryAutoSortCommand extends Command
	{
		[Inject]
		public var guiModel:GUIModel;

		[Inject]
		public var guiView:GUIView;

		override public function execute():void
		{
			guiModel.playerInventory.autoSortOnUpdate = guiView.autoSortPlayerInventoryCheckbox.isSelected;
		}
	}
}
