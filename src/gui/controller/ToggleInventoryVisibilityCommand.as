/**
 * Created by eVillain on 25/10/14.
 */
package gui.controller
{

	import gui.model.GUIModel;

	import robotlegs.bender.bundles.mvcs.Command;

	public class ToggleInventoryVisibilityCommand extends Command
	{
		[Inject]
		public var guiModel:GUIModel;

		override public function execute():void
		{
			if (!guiModel.shopInventory.visible)
			{
				guiModel.shopInventory.show();
				guiModel.playerInventory.show();
			}
			else
			{
				guiModel.shopInventory.hide();
				guiModel.playerInventory.hide();
			}
		}
	}
}
