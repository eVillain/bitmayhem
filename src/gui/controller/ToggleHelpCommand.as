/**
 * Created by eVillain on 25/10/14.
 */
package gui.controller
{

	import gui.model.GUIModel;

	import robotlegs.bender.bundles.mvcs.Command;

	public class ToggleHelpCommand extends Command
	{
		[Inject]
		public var guiModel:GUIModel;

		override public function execute():void
		{
			guiModel.help.visible = !guiModel.help.visible;
		}
	}
}
