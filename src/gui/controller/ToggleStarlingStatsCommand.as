/**
 * Created by eVillain on 26/10/14.
 */
package gui.controller
{

	import robotlegs.bender.bundles.mvcs.Command;

	import starling.core.Starling;

	public class ToggleStarlingStatsCommand extends Command
	{
		override public function execute():void
		{
			Starling.current.showStats = !Starling.current.showStats;
		}
	}
}
