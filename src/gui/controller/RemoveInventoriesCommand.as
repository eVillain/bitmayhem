/**
 * Created by eVillain on 25/10/14.
 */
package gui.controller
{

	import gui.SortControl;
	import gui.model.GUIModel;
	import gui.view.GUIView;

	import robotlegs.bender.bundles.mvcs.Command;

	public class RemoveInventoriesCommand extends Command
	{
		[Inject]
		public var guiModel:GUIModel;

		[Inject]
		public var guiView:GUIView;

		override public function execute():void
		{
			guiModel.removeInventoryListener();

			guiView.removeChild( guiModel.shopInventory );
			guiModel.shopInventory = null;

			guiView.removeChild( guiModel.playerInventory );
			guiModel.playerInventory.removeSorter( guiView.playerInventorySortBtn, SortControl.sortBySortOrder );
			guiModel.playerInventory = null;

			guiModel.playerInventory.removeChild( guiView.playerInventorySortBtn );
			guiModel.playerInventory.removeChild( guiView.autoSortPlayerInventoryCheckbox );
		}

	}
}
