/**
 * Created by eVillain on 25/10/14.
 */
package gui.controller
{

	import application.states.SinglePlayerState;

	import feathers.controls.Button;
	import feathers.controls.Check;

	import gui.model.GUIModel;
	import gui.view.GUIView;

	import robotlegs.bender.bundles.mvcs.Command;

	import starling.core.Starling;
	import starling.events.Event;
	import starling.events.TouchEvent;
	import starling.text.TextField;
	import starling.utils.Color;
	import starling.utils.HAlign;
	import starling.utils.VAlign;

	public class AddGameButtonsCommand extends Command
	{
		[Inject]
		public var guiModel:GUIModel;

		[Inject]
		public var guiView:GUIView;

		[Inject]
		public var singlePlayer:SinglePlayerState;

		override public function execute():void
		{
			if (!singlePlayer.joined)
			{
				guiView.joinGameButton = new Button();
				guiView.joinGameButton.label = "JOIN GAME";

				guiView.joinGameButton.addEventListener( Event.TRIGGERED, singlePlayer.onJoinGame );

				guiView.addChild( guiView.joinGameButton );

				guiView.joinGameButton.validate();

				//center the button
				guiView.joinGameButton.x = (Starling.current.stage.stageWidth - guiView.joinGameButton.width) / 2;
				guiView.joinGameButton.y = (Starling.current.stage.stageHeight - guiView.joinGameButton.height) / 2;
			}
			else
			{
				var cameraButton:Button = new Button();
				cameraButton.label = "Camera Elasticity";
				cameraButton.addEventListener( Event.TRIGGERED, guiView.onButtonClicked );
				guiView.addChild( cameraButton );
				guiView.cameraButton = cameraButton;
				cameraButton.validate();

				//center the button
				cameraButton.x = (guiView.stage.stageWidth - cameraButton.width);
				cameraButton.y = (guiView.stage.stageHeight - cameraButton.height);

				guiView.playerInventorySortBtn = new Button();
				guiView.playerInventorySortBtn.label = "Sort By Name";

				guiView.autoSortPlayerInventoryCheckbox = new Check();
				guiView.autoSortPlayerInventoryCheckbox.isSelected = guiModel.autoSortPlayerInventory;
				guiView.autoSortPlayerInventoryCheckbox.x = 100;
				guiView.autoSortPlayerInventoryCheckbox.label = "autoSort player inventory";
				guiView.autoSortPlayerInventoryCheckbox.addEventListener( TouchEvent.TOUCH, guiView.onTouch );

				guiView.textField = new TextField( 600, 20, "Debug text goes here, update with signal...", "Arial", 12, Color.GREEN );
				guiView.addChild( guiView.textField );
				guiView.textField.hAlign = HAlign.LEFT;  // horizontal alignment
				guiView.textField.vAlign = VAlign.BOTTOM; // vertical alignment
				guiView.textField.border = true;
				guiView.textField.x = 100;
			}
		}
	}
}
