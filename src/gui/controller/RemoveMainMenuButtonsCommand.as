/**
 * Created by eVillain on 25/10/14.
 */
package gui.controller
{

	import gui.view.GUIView;

	import robotlegs.bender.bundles.mvcs.Command;

	import starling.events.Event;

	public class RemoveMainMenuButtonsCommand extends Command
	{
		[Inject]
		public var guiView:GUIView;

		override public function execute():void
		{
			guiView.playButton.removeEventListener( Event.TRIGGERED, guiView.onButtonClicked );
			guiView.removeChild( guiView.playButton );
			guiView.playButton = null;
		}
	}
}
