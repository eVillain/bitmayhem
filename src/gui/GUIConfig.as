/**
 * Created by eVillain on 25/10/14.
 */
package gui
{

	import gui.controller.AddGameButtonsCommand;
	import gui.controller.AddHelpCommand;
	import gui.controller.AddMainMenuButtonsCommand;
	import gui.controller.AddPlayerInventoryCommand;
	import gui.controller.AddShopInventoryCommand;
	import gui.controller.InitializeGUICommand;
	import gui.controller.RemoveGameButtonsCommand;
	import gui.controller.RemoveHelpCommand;
	import gui.controller.RemoveInventoriesCommand;
	import gui.controller.RemoveMainMenuButtonsCommand;
	import gui.controller.ToggleHelpCommand;
	import gui.controller.ToggleInventoryVisibilityCommand;
	import gui.controller.ToggleStarlingStatsCommand;
	import gui.controller.UpdateInventoryAutoSortCommand;
	import gui.inventory.signals.InventoryItemClickedSignal;
	import gui.model.GUIModel;
	import gui.signals.AddGameButtonsSignal;
	import gui.signals.AddHelpSignal;
	import gui.signals.AddMainMenuButtonsSignal;
	import gui.signals.AddPlayerInventorySignal;
	import gui.signals.AddShopInventorySignal;
	import gui.signals.HideGUISignal;
	import gui.signals.InitializeGUISignal;
	import gui.signals.RemoveGameButtonsSignal;
	import gui.signals.RemoveHelpSignal;
	import gui.signals.RemoveInventoriesSignal;
	import gui.signals.RemoveMainMenuButtonsSignal;
	import gui.signals.SetDebugTextSignal;
	import gui.signals.ShowGUISignal;
	import gui.signals.ToggleHelpSignal;
	import gui.signals.ToggleInventoryVisibilitySignal;
	import gui.signals.ToggleStarlingStatsSignal;
	import gui.signals.UpdateInventoryAutoSortSignal;
	import gui.view.GUIView;

	import robotlegs.bender.extensions.eventCommandMap.api.IEventCommandMap;
	import robotlegs.bender.extensions.signalCommandMap.api.ISignalCommandMap;
	import robotlegs.bender.framework.api.IConfig;
	import robotlegs.bender.framework.api.IInjector;

	public class GUIConfig implements IConfig
	{
		[Inject]
		public var injector:IInjector;

		[Inject]
		public var signalCommandMap:ISignalCommandMap;

		[Inject]
		public var eventCommandMap:IEventCommandMap;

		public function configure():void
		{
			// Gui signals
			injector.map( ShowGUISignal ).asSingleton();
			injector.map( HideGUISignal ).asSingleton();

			injector.map( SetDebugTextSignal ).asSingleton( true );

			// Inventory controller
			injector.map( InventoryItemClickedSignal ).asSingleton();

			// GUI view and model
			injector.map( GUIModel ).asSingleton();
			injector.map( GUIView ).asSingleton();

			// GUI controller
			signalCommandMap.map( InitializeGUISignal ).toCommand( InitializeGUICommand );
			signalCommandMap.map( ToggleStarlingStatsSignal ).toCommand( ToggleStarlingStatsCommand );

			signalCommandMap.map( AddMainMenuButtonsSignal ).toCommand( AddMainMenuButtonsCommand );
			signalCommandMap.map( RemoveMainMenuButtonsSignal ).toCommand( RemoveMainMenuButtonsCommand );

			signalCommandMap.map( AddGameButtonsSignal ).toCommand( AddGameButtonsCommand );
			signalCommandMap.map( RemoveGameButtonsSignal ).toCommand( RemoveGameButtonsCommand );

			signalCommandMap.map( AddHelpSignal ).toCommand( AddHelpCommand );
			signalCommandMap.map( RemoveHelpSignal ).toCommand( RemoveHelpCommand );

			signalCommandMap.map( AddShopInventorySignal ).toCommand( AddShopInventoryCommand );
			signalCommandMap.map( AddPlayerInventorySignal ).toCommand( AddPlayerInventoryCommand );
			signalCommandMap.map( RemoveInventoriesSignal ).toCommand( RemoveInventoriesCommand );

			signalCommandMap.map( ToggleHelpSignal ).toCommand( ToggleHelpCommand );
			signalCommandMap.map( ToggleInventoryVisibilitySignal ).toCommand( ToggleInventoryVisibilityCommand );

			signalCommandMap.map( UpdateInventoryAutoSortSignal ).toCommand( UpdateInventoryAutoSortCommand );
		}
	}
}
