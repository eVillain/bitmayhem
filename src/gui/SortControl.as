/**
 * Created by Kevin on 21.10.2014.
 */
package gui
{

	public class SortControl
	{
		private static var _inverted:int = 1;

		public static function get inverted():Boolean
		{
			return _inverted;
		}

		public static function set inverted( value:Boolean ):void
		{
			_inverted = value ? 1 : -1;
		}

		public static function invert():void
		{
			_inverted = _inverted == 1 ? -1 : 1;
		}

		public static function sortByName( a:ISortable, b:ISortable ):int
		{
			return sort( a, b, "name" );
		}

		public static function sortBySortOrder( a:ISortable, b:ISortable ):int
		{
			return sort( a, b, "sortOrder" );
		}

		private static function sort( a:ISortable, b:ISortable, sortBy:String ):int
		{
			if (a[sortBy] < b[sortBy])
			{
				return 1 * _inverted;
			}
			else
			{
				if (a[sortBy] > b[sortBy])
				{
					return -1 * _inverted;
				}
			}
			return 0;
		}

		public function SortControl()
		{
		}
	}
}
