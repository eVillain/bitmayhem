/**
 * Created by Kevin on 21.10.2014.
 */
package gui.help
{

	import feathers.controls.text.TextFieldTextRenderer;

	import gui.constants.ColorConstants;
	import gui.inventory.GUIBackground;

	import starling.display.Sprite;

	public class HelpScreen extends Sprite
	{
		public function HelpScreen()
		{
		}

		private var _tfTitle:TextFieldTextRenderer;
		private var _tfContent:TextFieldTextRenderer;
		private var _background:GUIBackground;

		public function init():void
		{
			createTitle( ColorConstants.COLOR_GREY );
			createContent( ColorConstants.COLOR_GREY );
			_background = createBackground( ColorConstants.COLOR_LIGHT_GREY );
			_background.width = this.width;
			_background.height = this.height;
			addChildAt( _background, 0 );
		}

		public function cleanup():void
		{
			removeChild( _tfContent );
			_tfContent = null;

			removeChild( _tfTitle );
			_tfTitle = null;

			removeChildAt( 0, true );
			_background = null;
		}

		private function createContent( color:Number ):void
		{
			_tfContent = new TextFieldTextRenderer();
			var helpText:String;

			// toggle hud items
			helpText = "Show / hide help___________'h'\n";
			helpText += "Show / hide inventory_____'i'\n";
			helpText += "Show / hide physics debug_'.'\n";
			helpText += "Toggle free camera mode___'-'\n";

			// HERO ACTIONS
			helpText += "\n\n";
			helpText += "********** THE HERO! *********\n";
			helpText += "movement:  left & right arrows\n";
			helpText += "jump:      space bar\n";
			helpText += "grab:      down arrow\n";
			helpText += "shoot:     left mouse button\n";
			helpText += "******************************\n";

			_tfContent.text = helpText;
			_tfContent.background = true;
			_tfContent.backgroundColor = color;
			_tfContent.y = 50;
			addChild( _tfContent );
			_tfContent.validate();
		}

		private function createTitle( color:Number ):void
		{
			_tfTitle = new TextFieldTextRenderer();
			_tfTitle.text = "Help!";
			_tfTitle.background = true;
			_tfTitle.backgroundColor = color;

			addChild( _tfTitle );
			_tfTitle.validate();
		}

		private function createBackground( color:Number ):GUIBackground
		{
			var w:Number = 500;
			var h:Number = 500;

			return new GUIBackground( color, w, h );
		}
	}
}
