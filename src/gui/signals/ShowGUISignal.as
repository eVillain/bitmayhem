/**
 * Created by eVillain on 25/10/14.
 */
package gui.signals
{

	import org.osflash.signals.Signal;

	public class ShowGUISignal extends Signal
	{
		public function ShowGUISignal()
		{
			super();
		}
	}
}
