/**
 * Created by eVillain on 03/10/14.
 */
package gui.signals
{

	import org.osflash.signals.Signal;

	public class SetDebugTextSignal extends Signal
	{
		public function SetDebugTextSignal()
		{
			super( String );
		}
	}
}
