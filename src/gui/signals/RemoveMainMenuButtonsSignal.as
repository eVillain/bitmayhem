/**
 * Created by eVillain on 25/10/14.
 */
package gui.signals
{

	import org.osflash.signals.Signal;

	public class RemoveMainMenuButtonsSignal extends Signal
	{
		public function RemoveMainMenuButtonsSignal()
		{
			super();
		}
	}
}
