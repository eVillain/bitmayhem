/**
 * Created by eVillain on 25/10/14.
 */
package gui.signals
{

	import org.osflash.signals.Signal;

	public class UpdateInventoryAutoSortSignal extends Signal
	{
		public function UpdateInventoryAutoSortSignal()
		{
			super();
		}
	}
}
