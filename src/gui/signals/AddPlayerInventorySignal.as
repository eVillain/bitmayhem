/**
 * Created by eVillain on 26/10/14.
 */
package gui.signals
{

	import org.osflash.signals.Signal;

	public class AddPlayerInventorySignal extends Signal
	{
		public function AddPlayerInventorySignal()
		{
			super();
		}
	}
}
