/**
 * thank you, Scott!
 * http://scottbilas.com/blog/ultimate-as3-fake-enums/
 */
package gui.inventory.fakeEnum
{

	public class InventoryItemEnumType extends Enum
	{
		// static constructor
		{
			initEnum( InventoryItemEnumType );
		}
		public static const Inventory_Item_Weapon:InventoryItemEnumType = new InventoryItemEnumType();
		public static const Inventory_Item_Other:InventoryItemEnumType = new InventoryItemEnumType(); // static ctor

		public function InventoryItemEnumType()
		{
			Enum.ParseConstant( InventoryItemEnumType, "connecting", true )
		}

		public static function traceTypes():void
		{
			for each (var state:InventoryItemEnumType in Enum.GetConstants( InventoryItemEnumType ))
			{
				trace( state.Index + ": " + state.Name );
			}
		}
	}
}
