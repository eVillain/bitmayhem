/**
 * Created by eVillain on 26/10/14.
 */
package gui.inventory.vos
{

	import feathers.controls.Button;

	public class SorterVO
	{
		public var btn:Button;
		public var sortFunction:Function;

		public function SorterVO( btn:Button, sortFunction:Function )
		{
			this.btn = btn;
			this.sortFunction = sortFunction;
		}
	}
}
