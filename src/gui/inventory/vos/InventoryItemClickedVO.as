/**
 * Created by eVillain on 26/10/14.
 */
package gui.inventory.vos
{

	import gui.inventory.Inventory;
	import gui.inventory.InventoryItemVO;

	public class InventoryItemClickedVO
	{
		private var _eventType:String;
		private var _clickedItemVO:InventoryItemVO;
		private var _sourceInventory:Inventory;

		public function InventoryItemClickedVO( eventType:String, itemVO:InventoryItemVO, inventory:Inventory )
		{
			this._eventType = eventType;
			this._clickedItemVO = itemVO;
			this._sourceInventory = inventory;
		}

		public function get eventType():String
		{
			return _eventType;
		}

		public function get clickedItemVO():InventoryItemVO
		{
			return _clickedItemVO;
		}

		public function get sourceInventory():Inventory
		{
			return _sourceInventory;
		}
	}
}
