/**
 * Created by Kevin on 19.10.2014.
 */
package gui.inventory
{

	import starling.display.Image;
	import starling.display.Sprite;

	public class GUIBackground extends Sprite
	{
		private var _image:Image;

		public function GUIBackground( color:Number, w:Number, h:Number )
		{
			init( color, w, h );
		}

		private function init( color:Number, w:Number, h:Number ):void
		{
			_image = RectangularBackgroundFactory.makeRectangle( color, w, h );
			addChild( _image );
		}

		public function get image():Image
		{
			return _image;
		}
	}
}
