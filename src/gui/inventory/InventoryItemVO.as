/**
 * Created by Kevin on 20.10.2014.
 */
package gui.inventory
{

	import feathers.controls.Button;

	import game.view.DisplayHelper;

	import gui.ISortable;
	import gui.inventory.fakeEnum.InventoryItemEnumType;

	import starling.display.Sprite;

	public class InventoryItemVO implements ISortable
	{
		public function InventoryItemVO( type:InventoryItemEnumType, data:Object = null )
		{
			_type = type;

			if (data)
			{
				parseData( data );
			}
		}

		private var _disp:Sprite;

		public function get disp():Sprite
		{
			return _disp;
		}

		private var _type:InventoryItemEnumType;

		public function get type():InventoryItemEnumType
		{
			return _type;
		}

		private var _sortOrder:int;

		public function get sortOrder():int
		{
			return _sortOrder;
		}

		public function get name():String
		{
			// todo: use property
			return "Weapon";
		}

		public function parseData( data:Object ):void
		{
			_disp = data.disp;
			_sortOrder = data.sortOrder;
			trace( "_sortOrder: " + _sortOrder );
		}

		public function validate():void
		{
			if (_disp is Button)
			{
				Button( _disp ).validate();
			}
		}

		public function clone():InventoryItemVO
		{
			var clonedDisp:Sprite = DisplayHelper.cloneDisplayObject( disp );
			var vo:InventoryItemVO = new InventoryItemVO( type, {disp: clonedDisp, sortOrder: sortOrder} );
			return vo;
		}
	}
}
