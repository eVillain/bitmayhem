package gui.inventory
{

	import feathers.controls.Button;

	import flash.geom.Point;

	import game.view.DisplayHelper;

	import gui.SortControl;
	import gui.inventory.constants.InventoryEventConstants;
	import gui.inventory.signals.InventoryItemClickedSignal;
	import gui.inventory.vos.InventoryItemClickedVO;
	import gui.inventory.vos.SorterVO;

	import starling.display.DisplayObject;
	import starling.display.Sprite;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;

	/**
	 * ...
	 * @author KS
	 */
	public class Inventory extends Sprite
	{
		// This has to be manually injected on creation
		[Inject]
		public var inventoryItemClickSignal:InventoryItemClickedSignal;

		public var inventoryName:String;	// Should this really be public? We can change the name?
		protected var _itemContainer:Sprite;
		protected var items:Vector.<InventoryItemVO>;
		protected var itemsPerRow:uint;
		protected var columnWidth:Number;
		protected var rowHeight:Number;
		private var _sorters:Vector.<SorterVO>;

		private var _background:GUIBackground;
		private var _currentSorterVO:SorterVO;                  //TODO: select default sorter

		// sorts on update with the first added SorterVO
		private var _autoSortOnUpdate:Boolean;

		public function Inventory( items:Vector.<InventoryItemVO>, itemsPerRow:uint = 1, columnWidth:Number = 0, rowHeight:Number = 0, inventoryName:String = "", backgroundColor:Number = 0xCCCCCC, autoSortOnUpdate:Boolean = false )
		{
			super();

			this.items = items;
			this.itemsPerRow = itemsPerRow;
			this.columnWidth = columnWidth;
			this.rowHeight = rowHeight;
			this.inventoryName = inventoryName;
			this._autoSortOnUpdate = autoSortOnUpdate;
			_itemContainer = new Sprite();
			_background = createBackground( backgroundColor );

			addChild( _background );
			addChild( _itemContainer );

			_sorters = new Vector.<SorterVO>();

			hide();
		}

		public function set autoSortOnUpdate( value:Boolean ):void
		{
			_autoSortOnUpdate = value;

			if (_autoSortOnUpdate)
			{
				sort();
				update();
			}
		}

		public function show():void
		{
			this.visible = true;
			update();
		}

		public function hide():void
		{
			this.visible = false;
		}

		public function addItem( item:InventoryItemVO ):void
		{
			var index:int = items.indexOf( item );

			if (index == -1)
			{
				items.push( item );
				enableTouch( item );

				if (_autoSortOnUpdate && _sorters.length > 0)
				{
					_currentSorterVO = _sorters[0];
					sort();
				}
				update();
			}
		}

		public function removeItem( item:InventoryItemVO ):void
		{
			var index:int = items.indexOf( item );

			if (index != -1)
			{
				items.splice( index, 1 );

				if (_autoSortOnUpdate && _sorters.length > 0)
				{
					_currentSorterVO = _sorters[0];
					sort();
				}
				update();
			}
		}

		public function addSorter( btnSort:Button, sortFunction:Function ):void
		{
			var sorterVO:SorterVO = getSorterVOByBtn( btnSort );

			if (!sorterVO)
			{
				btnSort.addEventListener( TouchEvent.TOUCH, onBtnTouch );

				_sorters.push( new SorterVO( btnSort, sortFunction ) );
			}
		}

		public function removeSorter( btnSort:Button, sortFunction:Function ):void
		{
			for (var i:uint = 0; i < _sorters.length; i++)
			{
				if (_sorters[i].btn == btnSort)
				{
					btnSort.removeEventListener( TouchEvent.TOUCH, onBtnTouch );
					_sorters.splice( i, 1 );
					return;
				}
			}
		}

		// *** PROTECTED METHODS

		protected function addListeners( items:Vector.<InventoryItemVO> ):void
		{
			var item:InventoryItemVO;

			for (var i:uint = 0; i < items.length; i++)
			{
				item = items[i];
				enableTouch( item );
			}
		}

		protected function draw( items:Vector.<InventoryItemVO> ):void
		{
			DisplayHelper.removeChildren( _itemContainer );

			var item:InventoryItemVO;
			var pos:Point;
			var numItems:int = items.length;
			var rowID:int = 0;
			var columnID:int = 0;

			columnWidth = 100;
			rowHeight = 30;

			for (var i:uint = 0; i < numItems; i++)
			{
				item = items[i];
				rowID = Math.floor( i / itemsPerRow );

				columnID = i % itemsPerRow;
				if (item.disp.parent == null)
				{
					_itemContainer.addChild( item.disp );
					item.validate();
				}
				pos = new Point( columnID * columnWidth, rowID * rowHeight );
				item.disp.x = pos.x;
				item.disp.y = pos.y;
			}
			addListeners( items );
		}

		// *** PRIVATE METHODS
		private function getSorterVOByBtn( btn:Button ):SorterVO
		{
			for (var i:uint = 0; i < _sorters.length; i++)
			{
				if (_sorters[i].btn == btn)
				{
					return _sorters[i];
				}
			}
			return null;
		}

		private function enableTouch( item:InventoryItemVO ):void
		{
			item.disp.touchable = true;
			item.disp.addEventListener( TouchEvent.TOUCH, onItemTouch );
		}

		private function update():void
		{
			draw( items );
			updateBackground();
		}

		private function updateBackground():void
		{
			_background.width = _itemContainer.width;
			_background.height = _itemContainer.height;
		}

		private function getButtonByTouch( touch:Touch ):InventoryItemVO
		{
			var target:DisplayObject = touch.target;

			for (var i:uint = 0; i < items.length; i++)
			{
				if (items[i].disp == target)
				{
					return items[i];
				}
			}
			return null;
		}

		private function createBackground( color:Number ):GUIBackground
		{
			var w:Number = 500;
			var h:Number = 500;

			return new GUIBackground( color, w, h );
		}

		private function sort( btn:Button = null ):void
		{
			var sorterVO:SorterVO;

			if (!btn)
			{       // sort by last selected sorting method, otherwise use last (or to do: default ) sorting method
				if (_currentSorterVO)
				{
					items.sort( _currentSorterVO.sortFunction );
				}
			}
			// Why not else here and move sorterVO var below???
			if (btn)
			{
				sorterVO = getSorterVOByBtn( btn );

				_currentSorterVO = sorterVO;

				if (sorterVO)
				{
					items.sort( sorterVO.sortFunction );
				}
			}
			update();
		}

		private function onBtnTouch( event:TouchEvent ):void
		{
			var touch:Touch = event.getTouch( this );

			if (touch && touch.phase == TouchPhase.ENDED)
			{
				if (touch.target is Button)	// What does this do?
				{
					SortControl.invert();
					sort( event.target as Button )
				}
			}
		}

		private function onItemTouch( e:TouchEvent ):void
		{
			var touch:Touch = e.getTouch( this );

			if (touch && touch.phase == TouchPhase.ENDED)
			{
				var itemVO:InventoryItemVO = getButtonByTouch( touch );
				//dispatchEvent( new InventoryEvent( InventoryEvent.ITEM_CLICKED, itemVO, this ) );
				inventoryItemClickSignal.dispatch( new InventoryItemClickedVO( InventoryEventConstants.ITEM_CLICKED, itemVO, this ) );
			}
		}

	}
}
