/**
 * Created by Kevin on 21.10.2014.
 */
package gui.inventory
{

	import feathers.controls.Button;

	import gui.inventory.fakeEnum.InventoryItemEnumType;

	public class InventoryItemFactory
	{
		public static function getInventoryButtonVO( itemType:InventoryItemEnumType, label:String, sortOrder:int = 0 ):InventoryItemVO
		{
			// Prepare button
			var btn:Button = new Button();
			btn.label = label;

			// Prepare VO
			var itemVO:InventoryItemVO = new InventoryItemVO( itemType, {disp: btn, sortOrder: sortOrder} );

			return itemVO;
		}
	}
}
