/**
 * Created by Kevin on 19.10.2014.
 */
package gui.inventory
{

	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.Sprite;

	import starling.display.Image;

	public class RectangularBackgroundFactory
	{
		public static function makeRectangle( color:Number, width:Number, height:Number ):Image
		{
			var vectorBackground = new Sprite();
			vectorBackground.graphics.beginFill( color, 1 );
			vectorBackground.graphics.drawRect( 0, 0, width, height )
			vectorBackground.graphics.endFill();

			var backgroundBitmapData:BitmapData = new BitmapData( 500, 500, true, 0xFF0000 );
			backgroundBitmapData.draw( vectorBackground );
			var bmp:Bitmap = new Bitmap( backgroundBitmapData );

			return Image.fromBitmap( bmp );
		}
	}
}
