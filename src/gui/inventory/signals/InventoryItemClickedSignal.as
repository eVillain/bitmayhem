/**
 * Created by eVillain on 26/10/14.
 */
package gui.inventory.signals
{

	import gui.inventory.vos.InventoryItemClickedVO;

	import org.osflash.signals.Signal;

	public class InventoryItemClickedSignal extends Signal
	{
		public function InventoryItemClickedSignal()
		{
			super( InventoryItemClickedVO );
		}
	}
}
