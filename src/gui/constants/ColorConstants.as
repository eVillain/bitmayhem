/**
 * Created by Kevin on 21.10.2014.
 */
package gui.constants
{

	public class ColorConstants
	{
		public static const COLOR_RED:Number = 0xFF0000;
		public static const COLOR_GREEN:Number = 0x00FF00;
		public static const COLOR_BLUE:Number = 0x0000FF;

		public static const COLOR_LIGHT_GREY:Number = 0xa2a2a2;
		public static const COLOR_GREY:Number = 0x888888;
		public static const COLOR_DARK_GREY:Number = 0x333333;
	}
}
