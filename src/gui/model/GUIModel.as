/**
 * ...
 * @author KS
 */
package gui.model
{

	import gui.help.HelpScreen;
	import gui.inventory.Inventory;
	import gui.inventory.InventoryItemVO;
	import gui.inventory.signals.InventoryItemClickedSignal;
	import gui.inventory.vos.InventoryItemClickedVO;

	public class GUIModel
	{
		[Inject]
		public var inventoryItemClickedSignal:InventoryItemClickedSignal;

		private var _shopInventory:Inventory;           // shop inventory: creates clones of clicked items and adds them to the playerInventory
		private var _playerInventory:Inventory;         // player inventory: clicked items are removed from the player inventory

		private var _help:HelpScreen;
		private var _numButtons:uint = 5;                // amount of items per row. to do: set fixed width and let inventory calculate numButtons
		private var _autoSortShopInventory:Boolean = false;
		private var _autoSortPlayerInventory:Boolean = true;

		public function GUIModel()
		{
		}

		[PostConstruct]
		public function postConstruct():void
		{
			inventoryItemClickedSignal.add( onItemClick );
		}

		public function removeInventoryListener():void
		{
			inventoryItemClickedSignal.remove( onItemClick );
		}

		public function onItemClick( vo:InventoryItemClickedVO ):void
		{
			var inventory:Inventory = vo.sourceInventory;

			switch (inventory)
			{
				case _playerInventory:
					_playerInventory.removeItem( vo.clickedItemVO );
					break;
				case _shopInventory:
					var clone:InventoryItemVO = vo.clickedItemVO.clone();
					_playerInventory.addItem( clone );
					break;
			}
		}

		public function get shopInventory():Inventory
		{
			return _shopInventory;
		}

		public function set shopInventory( value:Inventory ):void
		{
			_shopInventory = value;
		}

		public function get playerInventory():Inventory
		{
			return _playerInventory;
		}

		public function set playerInventory( value:Inventory ):void
		{
			_playerInventory = value;
		}

		public function get help():HelpScreen
		{
			return _help;
		}

		public function set help( value:HelpScreen ):void
		{
			_help = value;
		}

		public function get numButtons():uint
		{
			return _numButtons;
		}

		public function set numButtons( value:uint ):void
		{
			_numButtons = value;
		}

		public function get autoSortShopInventory():Boolean
		{
			return _autoSortShopInventory;
		}

		public function set autoSortShopInventory( value:Boolean ):void
		{
			_autoSortShopInventory = value;
		}

		public function get autoSortPlayerInventory():Boolean
		{
			return _autoSortPlayerInventory;
		}

		public function set autoSortPlayerInventory( value:Boolean ):void
		{
			_autoSortPlayerInventory = value;
		}
	}
}
