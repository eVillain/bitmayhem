/**
 * Created by eVillain on 07/10/14.
 */
package camera.controller
{

	import camera.GameCamera;

	import robotlegs.bender.bundles.mvcs.Command;

	public class ToggleCameraElasticityCommand extends Command
	{
		[Inject]
		public var gameCamera:GameCamera;

		override public function execute():void
		{
			gameCamera.elasticPanEnabled = !gameCamera.elasticPanEnabled;
			gameCamera.elasticZoomEnabled = !gameCamera.elasticZoomEnabled;
		}
	}
}
