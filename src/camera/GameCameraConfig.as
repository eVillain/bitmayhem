/**
 * Created by eVillain on 06/09/14.
 */
package camera
{

	import camera.controller.ToggleCameraElasticityCommand;
	import camera.signals.CameraUpdatedSignal;
	import camera.signals.ToggleCameraElasticitySignal;

	import robotlegs.bender.extensions.signalCommandMap.api.ISignalCommandMap;
	import robotlegs.bender.framework.api.IConfig;
	import robotlegs.bender.framework.api.IInjector;

	public class GameCameraConfig implements IConfig
	{
		[Inject]
		public var injector:IInjector;

		[Inject]
		public var signalCommandMap:ISignalCommandMap;

		public function configure():void
		{
			signalCommandMap.map( ToggleCameraElasticitySignal ).toCommand( ToggleCameraElasticityCommand );

			injector.map( CameraUpdatedSignal ).asSingleton( true );
			injector.map( GameCamera ).asSingleton( true );
		}
	}
}
