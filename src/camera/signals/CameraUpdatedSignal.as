/**
 * Created by eVillain on 06/09/14.
 */
package camera.signals
{

	import org.osflash.signals.Signal;

	public class CameraUpdatedSignal extends Signal
	{
		public function CameraUpdatedSignal()
		{
			super();
		}
	}
}
