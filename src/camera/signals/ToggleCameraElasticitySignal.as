/**
 * Created by eVillain on 07/10/14.
 */
package camera.signals
{

	import org.osflash.signals.Signal;

	public class ToggleCameraElasticitySignal extends Signal
	{
		public function ToggleCameraElasticitySignal()
		{
			super();
		}
	}
}
