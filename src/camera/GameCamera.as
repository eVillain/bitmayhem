/**
 * Created by eVillain on 06/09/14.
 */
package camera
{

	import application.AppCore;

	import camera.signals.CameraUpdatedSignal;

	import flash.geom.Point;

	import utils.PointUtil;

	public class GameCamera
	{
		[Inject]
		public var cameraUpdatedSignal:CameraUpdatedSignal;

		private var _position:Point;
		private var _targetPosition:Point;
		private var _zoom:Number;
		private var _targetZoom:Number;
		private var _panElasticity:Number = 0.01;	// Value is out of 1.0 (1.0 being no elasticity) this has to be rethought
		private var _zoomElasticity:Number = 0.01;
		private var _elasticPanEnabled:Boolean = true;
		private var _elasticZoomEnabled:Boolean = true;

		public function GameCamera()
		{
			_position = new Point( 0, 0 );
			_targetPosition = new Point( 0, 0 );
			_zoom = 1.0;
			_targetZoom = 1.0;
		}

		[PostConstruct]
		public function init():void
		{
			trace( "------> Camera PostConstruct init." );
		}

		public function get position():Point
		{
			return _position;
		}

		public function set position( value:Point ):void
		{
			_position = value;
		}

		public function get zoom():Number
		{
			return _zoom;
		}

		public function set zoom( value:Number ):void
		{
			_zoom = value;
		}

		public function get targetPosition():Point
		{
			return _targetPosition;
		}

		public function set targetPosition( value:Point ):void
		{
			_targetPosition = value;
		}

		public function get targetZoom():Number
		{
			return _targetZoom;
		}

		public function set targetZoom( value:Number ):void
		{
			_targetZoom = value;
		}

		public function update( deltaTime:Number ):void
		{
			// Update position based on target position
			var positionDifference:Point = _targetPosition.subtract( _position );
			const posDiffThreshold:Number = 0.1;
			if (positionDifference.length > posDiffThreshold && _elasticPanEnabled)
			{
				PointUtil.scalePoint( positionDifference, _panElasticity * deltaTime * AppCore.targetFPS );
				_position = _position.add( positionDifference );
			}
			else
			{
				_position = _targetPosition;
			}

			// Update zoom based on target zoom
			var zoomDifference:Number = _targetZoom - _zoom;
			const zoomDiffThreshold:Number = 0.01;
			if (zoomDifference > zoomDiffThreshold && _elasticZoomEnabled)
			{
				zoomDifference *= _zoomElasticity * deltaTime * AppCore.targetFPS;
				_zoom += zoomDifference;
			}
			else
			{
				_zoom = _targetZoom;
			}

			cameraUpdatedSignal.dispatch();
		}

		public function get elasticPanEnabled():Boolean
		{
			return _elasticPanEnabled;
		}

		public function set elasticPanEnabled( value:Boolean ):void
		{
			_elasticPanEnabled = value;
		}

		public function get elasticZoomEnabled():Boolean
		{
			return _elasticZoomEnabled;
		}

		public function set elasticZoomEnabled( value:Boolean ):void
		{
			_elasticZoomEnabled = value;
		}

		public function get panElasticity():Number
		{
			return _panElasticity;
		}

		public function set panElasticity( value:Number ):void
		{
			_panElasticity = value;
		}

		public function get zoomElasticity():Number
		{
			return _zoomElasticity;
		}

		public function set zoomElasticity( value:Number ):void
		{
			_zoomElasticity = value;
		}
	}
}
