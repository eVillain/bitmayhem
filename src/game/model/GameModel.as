/**
 * Created by eVillain on 03/09/14.
 */
package game.model
{

	import background.GameBackground;

	import camera.GameCamera;
	import camera.signals.CameraUpdatedSignal;

	import entities.Entity;
	import entities.EntitySystem;
	import entities.signals.CreateItemSignal;
	import entities.signals.CreateNPCSignal;
	import entities.signals.CreatePlayerSignal;

	import game.signals.SpawnPlayerSignal;
	import game.view.GameView;

	import input.signals.InputUpdatedSignal;
	import input.signals.InputZoomUpdatedSignal;

	import physics.model.PhysicsModel;

	import resolution.constants.ResolutionConstants;
	import resolution.model.ResolutionModel;

	import tilemap.model.TileMapModel;
	import tilemap.signals.LoadTileMapSignal;
	import tilemap.view.TileMapView;

	public class GameModel
	{
		[Inject]
		public var gameView:GameView;

		[Inject]
		public var resolutionModel:ResolutionModel;

		[Inject]
		public var gameCamera:GameCamera;

		[Inject]
		public var physicsModel:PhysicsModel;

		[Inject]
		public var entitySystem:EntitySystem;

		[Inject]
		public var tileMapModel:TileMapModel;

		[Inject]
		public var tileMapView:TileMapView;

		[Inject]
		public var loadTileMapSignal:LoadTileMapSignal;

		[Inject]
		public var cameraUpdatedSignal:CameraUpdatedSignal;

		[Inject]
		public var createPlayerSignal:CreatePlayerSignal;

		[Inject]
		public var createItemSignal:CreateItemSignal;

		[Inject]
		public var createNPCSignal:CreateNPCSignal;

		[Inject]
		public var inputUpdatedSignal:InputUpdatedSignal;

		[Inject]
		public var inputZoomUpdatedSignal:InputZoomUpdatedSignal;

		[Inject]
		public var spawnPlayerSignal:SpawnPlayerSignal;

		private var _currentPlayer:Entity;
		private var _cameraFollowsPlayer:Boolean = false;
		private var _backGround:GameBackground;
		private var _localTimeScale:Number = 1.0;

		public function GameModel()
		{
			trace( "-------> GameModel constructed" );
		}

		[PostConstruct]
		public function init():void
		{
			trace( "-------> GameModel PostConstruct init" );

			// TODO: Move this logic into commands
			inputZoomUpdatedSignal.add( onZoom );

			cameraUpdatedSignal.add( updateCamera );
		}

		private function onZoom( value:Number ):void
		{
			if (value > 0)
			{
				gameCamera.targetZoom *= 2;
			}
			else
			{
				gameCamera.targetZoom *= 0.5;
			}
			gameCamera.targetZoom = Math.max( 0.25, gameCamera.targetZoom );
			gameCamera.targetZoom = Math.min( 8.0, gameCamera.targetZoom );
		}

		private function updateCamera():void
		{
			// Set scale according to camera zoom and render scaling
			var scaling:Number = gameCamera.zoom * resolutionModel.renderScale * ResolutionConstants.PIXELS_RENDER_SCALE;
			gameView.scaleX = scaling;
			gameView.scaleY = scaling;

			//gameView.width = resolutionModel.currentWidth / ResolutionConstants.PIXELS_PER_PIXEL;
			//gameView.height = resolutionModel.currentHeight / ResolutionConstants.PIXELS_PER_PIXEL;

			// Get center of screen
			var centerX:Number = (resolutionModel.currentWidth / 2) - (gameCamera.position.x * scaling);
			var centerY:Number = (resolutionModel.currentHeight / 2) - (gameCamera.position.y * scaling);

			// Set the position of the camera to the center of our screen
			gameView.x = Math.ceil( centerX );
			gameView.y = Math.ceil( centerY );

			//trace( "GameModel -> View size: " + gameView.width + "x" + gameView.height);
			//trace( "GameModel -> Camera Pos: " + gameCamera.position.x + ", " + gameCamera.position.y );
		}

		public function setCurrentPlayer( newPlayer1:Entity ):void
		{
			_currentPlayer = newPlayer1;
		}

		public function get currentPlayer():Entity
		{
			return _currentPlayer;
		}

		public function get backGround():GameBackground
		{
			return _backGround;
		}

		public function set backGround( value:GameBackground ):void
		{
			_backGround = value;
		}

		public function get cameraFollowsPlayer():Boolean
		{
			return _cameraFollowsPlayer;
		}

		public function set cameraFollowsPlayer( value:Boolean ):void
		{
			_cameraFollowsPlayer = value;
		}

		public function get localTimeScale():Number
		{
			return _localTimeScale;
		}

		public function set localTimeScale( value:Number ):void
		{
			_localTimeScale = value;
		}
	}
}
