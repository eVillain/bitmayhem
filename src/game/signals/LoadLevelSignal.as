/**
 * Created by eVillain on 03/10/14.
 */
package game.signals
{

	import org.osflash.signals.Signal;

	public class LoadLevelSignal extends Signal
	{
		public function LoadLevelSignal()
		{
			super();
		}
	}
}
