/**
 * Created by eVillain on 27/10/14.
 */
package game.signals
{

	import org.osflash.signals.Signal;

	public class ToggleCameraFollowsPlayerSignal extends Signal
	{
		public function ToggleCameraFollowsPlayerSignal()
		{
			super();
		}
	}
}
