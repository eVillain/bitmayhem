/**
 * Created by eVillain on 05/10/14.
 */
package game.signals
{

	import org.osflash.signals.Signal;

	public class SpawnPlayerSignal extends Signal
	{
		public function SpawnPlayerSignal()
		{
			super();
		}
	}
}
