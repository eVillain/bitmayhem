package game.view
{
import feathers.controls.Button;

import starling.display.DisplayObjectContainer;
import starling.display.Sprite;

/**
 * ...
 * @author KS
 */
public class DisplayHelper
{

	public static function removeChildren(disp:DisplayObjectContainer):void
	{
		while (disp.numChildren > 0)
		{
			disp.removeChildAt(0);
		}
	}

	public static function cloneDisplayObject(disp:Sprite):Sprite
	{
		var clone:Sprite;

		//TODO: add cloning for more object types
		if (disp is Button)
		{
			clone = cloneButton(disp as Button);
		}

		return clone;
	}

	// HELPER
	private static function cloneButton(btn:Button):Sprite
	{
		var clone:Button = new Button();
		clone.label = btn.label;
		return clone;
	}

}

}