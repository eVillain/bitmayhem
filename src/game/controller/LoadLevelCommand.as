/**
 * Created by eVillain on 26/10/14.
 */
package game.controller
{

	import background.GameBackground;

	import flash.filesystem.File;

	import game.model.GameModel;
	import game.view.GameView;

	import physics.signals.SetupPhysicsSignal;

	import robotlegs.bender.bundles.mvcs.Command;

	import tilemap.signals.LoadTileMapSignal;
	import tilemap.view.TileMapView;
	import tilemap.vos.LoadTileMapVO;

	public class LoadLevelCommand extends Command
	{
		[Inject]
		public var gameModel:GameModel;

		[Inject]
		public var gameView:GameView;

		[Inject]
		public var tileMapView:TileMapView;

		[Inject]
		public var loadTileMapSignal:LoadTileMapSignal;

		[Inject]
		public var setupPhysicsSignal:SetupPhysicsSignal;

		override public function execute():void
		{
			// Setup physics
			setupPhysicsSignal.dispatch();

			// Load game background
			gameModel.backGround = new GameBackground();
			gameModel.backGround.speed = 10;
			gameView.addChild( gameModel.backGround );

			var file:String = "gfx/tileMaps/";
			var url:String = File.applicationDirectory.resolvePath( file ).url + "/";

			// Load tilemap
			loadTileMapSignal.dispatch( new LoadTileMapVO( "BitTileMap.tmx", url ) );

			gameView.addChild( tileMapView );
		}
	}
}
