/**
 * Created by eVillain on 27/10/14.
 */
package game.controller
{

	import game.model.GameModel;

	import robotlegs.bender.bundles.mvcs.Command;

	public class ToggleCameraFollowsPlayerCommand extends Command
	{
		[Inject]
		public var gameModel:GameModel;

		override public function execute():void
		{
			gameModel.cameraFollowsPlayer = !gameModel.cameraFollowsPlayer;
		}
	}
}
