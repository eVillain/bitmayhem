/**
 * Created by eVillain on 26/10/14.
 */
package game.controller
{

	import camera.GameCamera;

	import entities.Entity;
	import entities.EntitySystem;
	import entities.constants.EntityDataConstants;

	import flash.geom.Point;

	import game.model.GameModel;

	import gametick.vos.GameTickVO;

	import input.vos.InputVO;

	import physics.signals.UpdatePhysicsSignal;

	import resolution.constants.ResolutionConstants;
	import resolution.model.ResolutionModel;

	import robotlegs.bender.bundles.mvcs.Command;

	import utils.PointUtil;

	public class GameUpdateCommand extends Command
	{
		[Inject]
		public var gameTick:GameTickVO;

		[Inject]
		public var gameModel:GameModel;

		[Inject]
		public var updatePhysicsSignal:UpdatePhysicsSignal;

		[Inject]
		public var entitySystem:EntitySystem;

		[Inject]
		public var gameCamera:GameCamera;

		[Inject]
		public var inputVO:InputVO;

		[Inject]
		public var resolutionModel:ResolutionModel;

		override public function execute():void
		{
			if (gameTick.deltaTime != 0)
			{
				if (inputVO.focus)
				{
					gameModel.localTimeScale = 0.25;
				}
				else
				{
					gameModel.localTimeScale = 1.0;
				}

				updatePhysicsSignal.dispatch();

				if (gameModel.cameraFollowsPlayer)
				{
					updateInput();
				}

				entitySystem.update( gameTick );

				if (gameModel.currentPlayer && gameModel.cameraFollowsPlayer)
				{
					// Get player position and give it to camera as target position
					gameCamera.targetPosition = gameModel.currentPlayer.getDataForKey( EntityDataConstants.ENTITY_DATA_POSITION ) as Point;
				}
				else
				{
					var cameraTarget:Point = inputVO.movement.clone();
					cameraTarget.y *= -1; // Flip y for camera movement
					if (cameraTarget.x != 0 || cameraTarget.y != 0)
					{
						var moveSpeed:Number = 4 * (inputVO.run ? 2.0 : 1.0) * 60 * gameTick.deltaTime;
						PointUtil.scalePoint( cameraTarget, moveSpeed );
						// Update camera position instead
						gameCamera.targetPosition = gameCamera.targetPosition.add( cameraTarget );
					}
				}

				// Refresh camera position and scale
				gameCamera.update( gameTick.deltaTime );
			}
		}

		private function updateInput():void
		{
			if (inputVO)
			{
				var currentPlayer:Entity = gameModel.currentPlayer;
				if (currentPlayer)
				{
					currentPlayer.setDataForKey( inputVO.movement.clone(), EntityDataConstants.ENTITY_DATA_INPUT_MOVEMENT );
					currentPlayer.setDataForKey( inputVO.jump, EntityDataConstants.ENTITY_DATA_INPUT_JUMP );
					currentPlayer.setDataForKey( inputVO.run, EntityDataConstants.ENTITY_DATA_INPUT_RUN );

					var scaling:Number = gameCamera.zoom * resolutionModel.renderScale * ResolutionConstants.PIXELS_RENDER_SCALE;
					// Get center of screen
					var diffX:Number = (inputVO.aimScreenPosition.x) - (resolutionModel.currentWidth / 2);
					var diffY:Number = (inputVO.aimScreenPosition.y) - (resolutionModel.currentHeight / 2);
					var worldAimPos:Point = new Point( diffX / scaling, diffY / scaling );
					worldAimPos = worldAimPos.add( gameCamera.position );
					currentPlayer.setDataForKey( worldAimPos, EntityDataConstants.ENTITY_DATA_INPUT_WORLD_AIM_POSITION );
					currentPlayer.setDataForKey( inputVO.fireHeldItem, EntityDataConstants.ENTITY_DATA_INPUT_FIRE );
					currentPlayer.setDataForKey( inputVO.throwHeldItem, EntityDataConstants.ENTITY_DATA_INPUT_THROW );
				}
			}
		}
	}
}
