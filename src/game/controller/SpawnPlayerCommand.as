/**
 * Created by eVillain on 22/03/15.
 */
package game.controller
{

	import entities.constants.EntityItemConstants;
	import entities.vos.CreateItemVO;
	import entities.vos.CreateNPCVO;
	import entities.vos.CreatePlayerVO;

	import flash.geom.Point;

	import game.model.GameModel;

	import robotlegs.bender.bundles.mvcs.Command;

	public class SpawnPlayerCommand extends Command
	{
		[Inject]
		public var gameModel:GameModel;

		override public function execute():void
		{
			if (!gameModel.currentPlayer)
			{
				gameModel.createPlayerSignal.dispatch( new CreatePlayerVO( new Point( 200, 10 ) ) );
				gameModel.createItemSignal.dispatch( new CreateItemVO( EntityItemConstants.ITEM_ID_SHOTGUN, new Point( 230, 10 ), 0.0 ) );
				gameModel.createItemSignal.dispatch( new CreateItemVO( EntityItemConstants.ITEM_ID_MACHINEGUN, new Point( 220, 10 ), 0.0 ) );
				gameModel.createItemSignal.dispatch( new CreateItemVO( EntityItemConstants.ITEM_ID_PISTOL, new Point( 210, 10 ), 0.0 ) );
				gameModel.createItemSignal.dispatch( new CreateItemVO( EntityItemConstants.ITEM_ID_RPG, new Point( 200, 10 ), 0.0 ) );
				gameModel.createItemSignal.dispatch( new CreateItemVO( EntityItemConstants.ITEM_ID_BATON, new Point( 190, 10 ), 0.0 ) );
				gameModel.createItemSignal.dispatch( new CreateItemVO( EntityItemConstants.ITEM_ID_RIOT_SHIELD, new Point( 180, 10 ), 0.0 ) );
				gameModel.createItemSignal.dispatch( new CreateItemVO( EntityItemConstants.ITEM_ID_DAGGER, new Point( 170, 10 ), 0.0 ) );
				gameModel.createItemSignal.dispatch( new CreateItemVO( EntityItemConstants.ITEM_ID_GRENADE, new Point( 160, 10 ), 0.0 ) );
				gameModel.cameraFollowsPlayer = true;

				// Add a debug NPC
				gameModel.createNPCSignal.dispatch( new CreateNPCVO( new Point( 250, 10 ) ) );
			}
		}
	}
}
