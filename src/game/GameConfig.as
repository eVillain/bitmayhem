/**
 * Created by eVillain on 03/09/14.
 */
package game
{

	import application.signals.StartSinglePlayerSignal;

	import game.controller.GameUpdateCommand;
	import game.controller.LoadLevelCommand;
	import game.controller.SpawnPlayerCommand;
	import game.controller.ToggleCameraFollowsPlayerCommand;
	import game.model.GameModel;
	import game.signals.LoadLevelSignal;
	import game.signals.SpawnPlayerSignal;
	import game.signals.ToggleCameraFollowsPlayerSignal;
	import game.view.GameView;

	import gametick.signals.GameTickSignal;

	import robotlegs.bender.extensions.signalCommandMap.api.ISignalCommandMap;
	import robotlegs.bender.framework.api.IConfig;
	import robotlegs.bender.framework.api.IInjector;

	public class GameConfig implements IConfig
	{
		[Inject]
		public var injector:IInjector;

		[Inject]
		public var signalCommandMap:ISignalCommandMap;

		public function configure():void
		{
			injector.map( StartSinglePlayerSignal ).asSingleton( true );
			injector.map( SpawnPlayerSignal ).asSingleton( true );

			injector.map( GameView ).asSingleton();
			injector.map( GameModel ).asSingleton( true );

			// Here's where we hook up the game tick to the game update logic
			signalCommandMap.map( GameTickSignal ).toCommand( GameUpdateCommand );
			signalCommandMap.map( LoadLevelSignal ).toCommand( LoadLevelCommand );
			signalCommandMap.map( ToggleCameraFollowsPlayerSignal ).toCommand( ToggleCameraFollowsPlayerCommand );
			signalCommandMap.map( SpawnPlayerSignal ).toCommand( SpawnPlayerCommand );
		}
	}
}
