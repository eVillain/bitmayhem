/**
 * Created by eVillain on 25/10/14.
 */
package entities.controller
{

	import entities.Entity;
	import entities.constants.EntityDataConstants;

	import nape.phys.Body;

	import physics.model.PhysicsModel;

	import robotlegs.bender.bundles.mvcs.Command;

	public class DetachPhysicsCommand extends Command
	{
		[Inject]
		public var physicsModel:PhysicsModel;

		[Inject]
		public var entity:Entity;	// Signal payload

		override public function execute():void
		{
			if (entity.hasDataObjectForKey( EntityDataConstants.ENTITY_DATA_PHYSICS_BODY ))
			{
				var body:Body = entity.getDataForKey( EntityDataConstants.ENTITY_DATA_PHYSICS_BODY ) as Body;
				physicsModel.removeBody( body );
			}
		}
	}
}
