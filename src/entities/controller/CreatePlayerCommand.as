/**
 * Created by eVillain on 07/09/14.
 */
package entities.controller
{

	import entities.Entity;
	import entities.EntitySystem;
	import entities.constants.ComponentConstants;
	import entities.constants.EntityAnimationConstants;
	import entities.constants.EntityDataConstants;
	import entities.constants.EntityTypeConstants;
	import entities.vos.CreatePlayerVO;

	import flash.geom.Point;
	import flash.utils.Dictionary;

	import game.model.GameModel;
	import game.view.GameView;

	import nape.phys.Body;
	import nape.phys.Material;

	import physics.constants.CollisionConstants;
	import physics.constants.PhysicsConstants;
	import physics.contacts.ContactSet;
	import physics.model.PhysicsModel;
	import physics.utils.PhysicsShapeUtil;

	import robotlegs.bender.bundles.mvcs.Command;

	import starling.display.MovieClip;
	import starling.display.Sprite;
	import starling.textures.TextureSmoothing;

	public class CreatePlayerCommand extends Command
	{
		[Inject]
		public var physicsModel:PhysicsModel;

		[Inject]
		public var gameView:GameView;

		[Inject]
		public var gameModel:GameModel;

		[Inject]
		public var entitySystem:EntitySystem;

		[Inject]
		public var vo:CreatePlayerVO;

		public static const PLAYER_WIDTH:int = 4;
		public static const PLAYER_HEIGHT:int = 8;

		override public function execute():void
		{
			trace( "Creating player..." );
			var entity:Entity = entitySystem.createEntity();

			// Create animations set
			var animations:Dictionary = new Dictionary();

			var jumpClip:MovieClip = new MovieClip( Assets.getAtlasGame().getTextures( "BitThiefJump1" ), 1 );
			animations[EntityAnimationConstants.ENTITY_ANIMATION_JUMPING] = jumpClip;
			jumpClip.smoothing = TextureSmoothing.NONE;
			jumpClip.alignPivot();

			var diveClip:MovieClip = new MovieClip( Assets.getAtlasGame().getTextures( "BitThiefDive" ), 8 );
			animations[EntityAnimationConstants.ENTITY_ANIMATION_DIVING] = diveClip;
			diveClip.smoothing = TextureSmoothing.NONE;
			diveClip.alignPivot();

			var standClip:MovieClip = new MovieClip( Assets.getAtlasGame().getTextures( "BitThiefStand" ), 2 );
			animations[EntityAnimationConstants.ENTITY_ANIMATION_IDLE] = standClip;
			standClip.smoothing = TextureSmoothing.NONE;
			standClip.alignPivot();

			var walkClip:MovieClip = new MovieClip( Assets.getAtlasGame().getTextures( "BitThiefWalk" ), 8 );
			walkClip.addFrame( Assets.getAtlasGame().getTexture( "BitThiefStand1" ) );
			animations[EntityAnimationConstants.ENTITY_ANIMATION_WALKING] = walkClip;
			walkClip.smoothing = TextureSmoothing.NONE;
			walkClip.alignPivot();

			// Player death animation - coming soon to a screen near you, possibly
//			var deathClip:MovieClip = new MovieClip( Assets.getAtlasGame().getTextures( "BitThiefDeath" ), 2 );
//			animations[EntityAnimationConstants.ENTITY_ANIMATION_DEATH] = deathClip;
//			deathClip.smoothing = TextureSmoothing.NONE;
//			deathClip.loop = false;
//			deathClip.x = Math.ceil( -deathClip.width / 2 );
//			deathClip.y = Math.ceil( -deathClip.height / 2 );

			// Add sprite to game view
			var heroSprite:Sprite = new Sprite();
			gameView.addChild( heroSprite );

			// Create a box shape
			var body:Body = PhysicsShapeUtil.createBox( physicsModel.space, vo.playerPosition, PLAYER_WIDTH, PLAYER_HEIGHT, 0, new Point( 0, 0 ), 0, PhysicsConstants.DYNAMIC, CollisionConstants.COLLISION_TYPE_PLAYER, entity, 1.0, 0.5, 0.0 );
			// Alternate player shape, it's rounder
//			var body:Body = physicsModel.createCircle(vo.playerPosition, PLAYER_HEIGHT, PhysicsConstants.DYNAMIC, entity, CollisionConstants.COLLISION_TYPE_PLAYER, 0.5, 0.5, 0.0);
			body.shapes.at( 0 ).filter.collisionGroup = CollisionConstants.COLLISION_MASK_PLAYER1; // Belong to player collision group
			body.shapes.at( 0 ).filter.collisionMask = ~CollisionConstants.COLLISION_MASK_PLAYER1; // Collide with everything but player
			var material:Material = new Material( Number.NEGATIVE_INFINITY, .1, 2, 1 );
			body.shapes.at( 0 ).material = material;
			body.allowRotation = false;

			// Entity type data
			entity.setDataForKey( EntityTypeConstants.ENTITY_TYPE_PLAYER, EntityDataConstants.ENTITY_DATA_TYPE );

			// Sprite data
			entity.setDataForKey( heroSprite, EntityDataConstants.ENTITY_DATA_SPRITE );

			// Animation data
			entity.setDataForKey( EntityAnimationConstants.ENTITY_ANIMATION_IDLE, EntityDataConstants.ENTITY_DATA_ANIMATION_STATE );
			entity.setDataForKey( EntityAnimationConstants.ENTITY_ANIMATION_UNINITIALIZED, EntityDataConstants.ENTITY_DATA_ANIMATION_PREVIOUS );
			entity.setDataForKey( 1.0, EntityDataConstants.ENTITY_DATA_ANIMATION_SPEED );
			entity.setDataForKey( animations, EntityDataConstants.ENTITY_DATA_ANIMATION_SET );

			// Inventory
			entity.setDataForKey( new Vector.<Entity>, EntityDataConstants.ENTITY_DATA_INVENTORY );

			// Position, angle velocity
			entity.setDataForKey( new Point( 0, 0 ), EntityDataConstants.ENTITY_DATA_POSITION );
			entity.setDataForKey( 0.0, EntityDataConstants.ENTITY_DATA_ANGLE );
			entity.setDataForKey( new Point( 0, 0 ), EntityDataConstants.ENTITY_DATA_VELOCITY );

			// Health
			entity.setDataForKey( 100, EntityDataConstants.ENTITY_DATA_HEALTH );

			// Input data
			entity.setDataForKey( new Point( 0, 0 ), EntityDataConstants.ENTITY_DATA_INPUT_MOVEMENT );
			entity.setDataForKey( false, EntityDataConstants.ENTITY_DATA_INPUT_JUMP );
			entity.setDataForKey( 0.0, EntityDataConstants.ENTITY_DATA_INPUT_JUMP_BOOST );
			entity.setDataForKey( false, EntityDataConstants.ENTITY_DATA_INPUT_RUN );
			entity.setDataForKey( new Point( 0, 0 ), EntityDataConstants.ENTITY_DATA_INPUT_WORLD_AIM_POSITION );
			entity.setDataForKey( false, EntityDataConstants.ENTITY_DATA_INPUT_FIRE );
			entity.setDataForKey( false, EntityDataConstants.ENTITY_DATA_INPUT_THROW );

			// Physics data
			entity.setDataForKey( false, EntityDataConstants.ENTITY_DATA_PHYSICS_ONGROUND );
			entity.setDataForKey( new ContactSet(), EntityDataConstants.ENTITY_DATA_PHYSICS_CONTACTS );
			entity.setDataForKey( body, EntityDataConstants.ENTITY_DATA_PHYSICS_BODY );

			// Add all the necessary components for logic
			entitySystem.getComponent( ComponentConstants.COMPONENT_HEALTH ).addEntity( entity );
			entitySystem.getComponent( ComponentConstants.COMPONENT_HUMAN ).addEntity( entity );
			entitySystem.getComponent( ComponentConstants.COMPONENT_PHYSICS ).addEntity( entity );
			entitySystem.getComponent( ComponentConstants.COMPONENT_SPRITE ).addEntity( entity );
			entitySystem.getComponent( ComponentConstants.COMPONENT_ANIMATION ).addEntity( entity );

			// Add player 1 to game model - should eventually become
			// a generalized "addPlayer" function to support more players
			gameModel.setCurrentPlayer( entity );
		}
	}
}
