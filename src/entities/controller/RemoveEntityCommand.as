/**
 * Created by eVillain on 22/03/15.
 */
package entities.controller
{

	import entities.Entity;
	import entities.EntitySystem;
	import entities.constants.EntityDataConstants;

	import game.view.GameView;

	import nape.phys.Body;

	import physics.model.PhysicsModel;

	import robotlegs.bender.bundles.mvcs.Command;

	import starling.display.Sprite;

	public class RemoveEntityCommand extends Command
	{
		[Inject]
		public var physicsModel:PhysicsModel;

		[Inject]
		public var gameView:GameView;

		[Inject]
		public var entitySystem:EntitySystem;

		[Inject]
		public var entityID:uint;

		override public function execute():void
		{
			//trace( "Destroying entity..." + entityID.toString() );

			var entity:Entity = entitySystem.getEntityForKey( entityID );

			if (entity.hasDataObjectForKey( EntityDataConstants.ENTITY_DATA_SPRITE ))
			{
				var sprite:Sprite = entity.getDataForKey( EntityDataConstants.ENTITY_DATA_SPRITE ) as Sprite;
				gameView.removeChild( sprite );
			}
			if (entity.hasDataObjectForKey( EntityDataConstants.ENTITY_DATA_PHYSICS_BODY ))
			{
				var body:Body = entity.getDataForKey( EntityDataConstants.ENTITY_DATA_PHYSICS_BODY ) as Body;
				physicsModel.removeBody( body );
			}

			entitySystem.removeEntity( entityID );
		}
	}
}
