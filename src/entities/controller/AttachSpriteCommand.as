/**
 * Created by eVillain on 25/10/14.
 */
package entities.controller
{

	import entities.Entity;
	import entities.constants.EntityDataConstants;

	import game.view.GameView;

	import robotlegs.bender.bundles.mvcs.Command;

	import starling.display.Sprite;

	public class AttachSpriteCommand extends Command
	{
		[Inject]
		public var gameView:GameView;

		[Inject]
		public var entity:Entity;	// Signal payload

		override public function execute():void
		{
			if (entity.hasDataObjectForKey( EntityDataConstants.ENTITY_DATA_SPRITE ))
			{
				var sprite:Sprite = entity.getDataForKey( EntityDataConstants.ENTITY_DATA_SPRITE ) as Sprite;
				gameView.addChild( sprite );
			}
		}
	}
}
