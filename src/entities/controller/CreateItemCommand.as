/**
 * Created by eVillain on 21/09/14.
 */
package entities.controller
{

	import entities.Entity;
	import entities.EntitySystem;
	import entities.constants.ComponentConstants;
	import entities.constants.EntityDataConstants;
	import entities.constants.EntityItemConstants;
	import entities.constants.EntityProjectileConstants;
	import entities.constants.EntityTypeConstants;
	import entities.vos.CreateItemVO;

	import flash.geom.Point;
	import flash.geom.Rectangle;

	import game.view.GameView;

	import nape.phys.Body;

	import physics.constants.CollisionConstants;
	import physics.constants.PhysicsConstants;
	import physics.model.PhysicsModel;
	import physics.utils.PhysicsShapeUtil;

	import robotlegs.bender.bundles.mvcs.Command;

	import starling.display.Image;
	import starling.display.Sprite;
	import starling.textures.TextureSmoothing;

	public class CreateItemCommand extends Command
	{
		[Inject]
		public var physicsModel:PhysicsModel;

		[Inject]
		public var gameView:GameView;

		[Inject]
		public var entitySystem:EntitySystem;

		[Inject]
		public var vo:CreateItemVO;

		override public function execute():void
		{
			var entity:Entity = entitySystem.createEntity();
			var entityID:uint = entity.getDataForKey( EntityDataConstants.ENTITY_DATA_ID ) as uint;

			trace( "Creating entity " + entityID.toString() + " of type item " + vo.itemID.toString() );

			setBaseItemDataForEntity( entity );

			// Set necessary components for logic
			setPhysicsDataForEntity( entity );

			setSpriteDataForEntity( entity );

			setProjectileDataForEntity( entity );

			entitySystem.getComponent( ComponentConstants.COMPONENT_HEALTH ).addEntity( entity );
		}

		private function setBaseItemDataForEntity( entity:Entity ):void
		{
			entity.setDataForKey( EntityTypeConstants.ENTITY_TYPE_ITEM, EntityDataConstants.ENTITY_DATA_TYPE );
			entity.setDataForKey( vo.itemID, EntityDataConstants.ENTITY_DATA_ITEM_ID );

			entity.setDataForKey( new Point( vo.position.x, vo.position.y ), EntityDataConstants.ENTITY_DATA_POSITION );
			entity.setDataForKey( 0.0, EntityDataConstants.ENTITY_DATA_ANGLE );
			entity.setDataForKey( new Point( 0, 0 ), EntityDataConstants.ENTITY_DATA_VELOCITY );
			entity.setDataForKey( 50, EntityDataConstants.ENTITY_DATA_HEALTH );
		}

		private function setPhysicsDataForEntity( entity:Entity ):void
		{
			var itemRect:Rectangle = rectangleForItem( vo );
			if (itemRect.width > 0 && itemRect.height > 0)
			{
				var itemBody:Body = PhysicsShapeUtil.createBox( physicsModel.space, vo.position, itemRect.width, itemRect.height, vo.rotation, new Point( 0, 0 ), 0.0, PhysicsConstants.DYNAMIC, CollisionConstants.COLLISION_TYPE_ITEM, entity, 1.0, 0.1, 0.0 );
				itemBody.shapes.at( 0 ).filter.collisionGroup = CollisionConstants.COLLISION_MASK_ITEM;
				entity.setDataForKey( itemBody, EntityDataConstants.ENTITY_DATA_PHYSICS_BODY );
				// Add physics component to entity
				entitySystem.getComponent( ComponentConstants.COMPONENT_PHYSICS ).addEntity( entity );
			}
		}

		private function setSpriteDataForEntity( entity:Entity ):void
		{
			var itemSpriteName:String = spriteForItemID( vo.itemID );
			if (itemSpriteName != "")
			{
				var itemImage:Image = new Image( Assets.getAtlasGame().getTexture( itemSpriteName ) );
				itemImage.smoothing = TextureSmoothing.NONE;
				itemImage.x = Math.ceil( -itemImage.width / 2 ) - 1.5;
				itemImage.y = Math.ceil( -itemImage.height / 2 );

				// Add sprite to game view
				var itemSprite:Sprite = new Sprite();
				itemSprite.addChild( itemImage );
				gameView.addChild( itemSprite );

				// Add sprite to entity data
				entity.setDataForKey( itemSprite, EntityDataConstants.ENTITY_DATA_SPRITE );
				// Add sprite component to entity
				entitySystem.getComponent( ComponentConstants.COMPONENT_SPRITE ).addEntity( entity );
			}
		}

		private function setProjectileDataForEntity( entity:Entity ):void
		{
			var projectileAmount:uint = vo.itemID == EntityItemConstants.ITEM_ID_SHOTGUN ? 8 : 1;
			var projectileType:String = projectileTypeForItemID( vo.itemID );
			var projectileOffset:Point = projectileOffsetForItemID( vo.itemID );

			var fireRate:Number = fireRateForItemID( vo.itemID );
			if (projectileType != "" && fireRate != 0.0)
			{
				entity.setDataForKey( projectileType, EntityDataConstants.ENTITY_DATA_ITEM_PROJECTILE_TYPE );
				entity.setDataForKey( projectileAmount, EntityDataConstants.ENTITY_DATA_ITEM_PROJECTILE_AMOUNT );
				entity.setDataForKey( projectileOffset, EntityDataConstants.ENTITY_DATA_ITEM_PROJECTILE_OFFSET );
				entity.setDataForKey( false, EntityDataConstants.ENTITY_DATA_ITEM_TRIGGER );
				entity.setDataForKey( fireRate, EntityDataConstants.ENTITY_DATA_ITEM_TRIGGER_RATE );
				entity.setDataForKey( 0.0, EntityDataConstants.ENTITY_DATA_ITEM_TRIGGER_ACCUMULATOR );
				// Add projectile spawner component to entity
				entitySystem.getComponent( ComponentConstants.COMPONENT_PROJECTILE_SPAWNER ).addEntity( entity );
			}
		}

		private static function spriteForItemID( itemID:uint ):String
		{
			switch (itemID)
			{
				case EntityItemConstants.ITEM_ID_PISTOL:
					return "BitPistol";
				case EntityItemConstants.ITEM_ID_MACHINEGUN:
					return "BitMG";
				case EntityItemConstants.ITEM_ID_SHOTGUN:
					return "BitShotgun";
				case EntityItemConstants.ITEM_ID_RPG:
					return "BitRocketL";
				case EntityItemConstants.ITEM_ID_GRENADE:
					return "BitGrenade";
				case EntityItemConstants.ITEM_ID_BATON:
					return "BitBaton";
				case EntityItemConstants.ITEM_ID_RIOT_SHIELD:
					return "BitShield";
				case EntityItemConstants.ITEM_ID_DAGGER:
					return "Dagger";
				default:
					return "";
			}
		}

		private static function rectangleForItem( itemVO:CreateItemVO ):Rectangle
		{
			switch (itemVO.itemID)
			{
				case EntityItemConstants.ITEM_ID_PISTOL:
					return new Rectangle( itemVO.position.x, itemVO.position.y, 2, 2 );
				case EntityItemConstants.ITEM_ID_MACHINEGUN:
					return new Rectangle( itemVO.position.x, itemVO.position.y, 5, 2 );
				case EntityItemConstants.ITEM_ID_SHOTGUN:
					return new Rectangle( itemVO.position.x, itemVO.position.y, 6, 2 );
				case EntityItemConstants.ITEM_ID_RPG:
					return new Rectangle( itemVO.position.x, itemVO.position.y, 7, 3 );
				case EntityItemConstants.ITEM_ID_GRENADE:
					return new Rectangle( itemVO.position.x, itemVO.position.y, 2, 2 );
				case EntityItemConstants.ITEM_ID_BATON:
					return new Rectangle( itemVO.position.x, itemVO.position.y, 3, 2 );
				case EntityItemConstants.ITEM_ID_RIOT_SHIELD:
					return new Rectangle( itemVO.position.x, itemVO.position.y, 2, 6 );
				case EntityItemConstants.ITEM_ID_DAGGER:
					return new Rectangle( itemVO.position.x, itemVO.position.y, 3, 2 );
				default:
					return new Rectangle();
			}
		}

		private static function projectileTypeForItemID( itemID:uint ):String
		{
			switch (itemID)
			{
				case EntityItemConstants.ITEM_ID_PISTOL:
				case EntityItemConstants.ITEM_ID_MACHINEGUN:
				case EntityItemConstants.ITEM_ID_SHOTGUN:
					return EntityProjectileConstants.PROJECTILE_TYPE_BULLET;
				case EntityItemConstants.ITEM_ID_RPG:
					return EntityProjectileConstants.PROJECTILE_TYPE_ROCKET;
				default:
					return "";
			}
		}

		private static function fireRateForItemID( itemID:uint ):Number
		{
			switch (itemID)
			{
				case EntityItemConstants.ITEM_ID_PISTOL:
					return 0.75;
				case EntityItemConstants.ITEM_ID_MACHINEGUN:
					return 0.15;
				case EntityItemConstants.ITEM_ID_RPG:
					return 1.25;
				case EntityItemConstants.ITEM_ID_GRENADE:
					return 1.25;
				case EntityItemConstants.ITEM_ID_BATON:
					return 1.25;
				case EntityItemConstants.ITEM_ID_RIOT_SHIELD:
					return 1.25;
				case EntityItemConstants.ITEM_ID_DAGGER:
					return 1.25;
				default:
					return 0.0;
			}
		}

		private static function projectileOffsetForItemID( itemID:uint ):Point
		{
			switch (itemID)
			{
				case EntityItemConstants.ITEM_ID_PISTOL:
					return new Point( 1, 0.5 );
				case EntityItemConstants.ITEM_ID_MACHINEGUN:
					return new Point( 2.5, 0.5 );
				case EntityItemConstants.ITEM_ID_SHOTGUN:
					return new Point( 3, 0.5 );
				case EntityItemConstants.ITEM_ID_RPG:
					return new Point( 4, 1 );
				default:
					return new Point();
			}
		}
	}
}
