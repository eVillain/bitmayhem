/**
 * Created by eVillain on 01/10/14.
 */
package entities.controller
{

	import entities.Entity;
	import entities.EntitySystem;
	import entities.constants.EntityAnimationConstants;
	import entities.constants.EntityDataConstants;
	import entities.constants.EntityTypeConstants;
	import entities.signals.RemoveEntitySignal;

	import robotlegs.bender.bundles.mvcs.Command;

	public class DestroyEntityCommand extends Command
	{
		[Inject]
		public var entitySystem:EntitySystem;

		[Inject]
		public var removeEntitySignal:RemoveEntitySignal;

		[Inject]
		public var entityID:uint;

		override public function execute():void
		{
			//trace( "Destroying entity..." + entityID.toString() );

			var entity:Entity = entitySystem.getEntityForKey( entityID );

			switch (entity.getDataForKey( EntityDataConstants.ENTITY_DATA_TYPE ))
			{
				case EntityTypeConstants.ENTITY_TYPE_NPC_POLICEMAN:
					// TODO: We should probably have this command be a macro or at least set a hook to spawn debris
					entity.setDataForKey( EntityAnimationConstants.ENTITY_ANIMATION_DEATH, EntityDataConstants.ENTITY_DATA_ANIMATION_STATE );
					entity.setDataForKey( 6, EntityDataConstants.ENTITY_DATA_ANIMATION_SPEED );
					break;
				default:
					removeEntitySignal.dispatch( entityID );
					break;
			}
		}
	}
}
