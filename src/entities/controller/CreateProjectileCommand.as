/**
 * Created by eVillain on 01/10/14.
 */
package entities.controller
{

	import entities.Entity;
	import entities.EntitySystem;
	import entities.constants.ComponentConstants;
	import entities.constants.EntityDataConstants;
	import entities.constants.EntityTypeConstants;
	import entities.vos.CreateProjectileVO;

	import flash.geom.Point;

	import game.view.GameView;

	import nape.geom.Vec2;
	import nape.phys.Body;

	import physics.constants.CollisionConstants;
	import physics.constants.PhysicsConstants;
	import physics.contacts.ContactSet;
	import physics.model.PhysicsModel;
	import physics.utils.PhysicsShapeUtil;

	import robotlegs.bender.bundles.mvcs.Command;

	import starling.display.Image;
	import starling.display.Sprite;
	import starling.textures.TextureSmoothing;

	public class CreateProjectileCommand extends Command
	{
		[Inject]
		public var physicsModel:PhysicsModel;

		[Inject]
		public var gameView:GameView;

		[Inject]
		public var entitySystem:EntitySystem;

		[Inject]
		public var vo:CreateProjectileVO;

		public static const BULLET_WIDTH:int = 2;
		public static const BULLET_HEIGHT:int = 1;
		public static const BULLET_LIFETIME:int = 10;

		override public function execute():void
		{
			var entity:Entity = entitySystem.createEntity();

			var itemImage:Image = new Image( Assets.getAtlasGame().getTexture( "BitBullet" ) );
			itemImage.smoothing = TextureSmoothing.NONE;
			itemImage.scaleX = BULLET_WIDTH / 2;
			itemImage.scaleY = BULLET_HEIGHT / 2;
			itemImage.x = Math.ceil( -itemImage.width / 2 );
			itemImage.y = Math.ceil( -itemImage.height / 2 );

			// Add sprite to game view
			var itemSprite:Sprite = new Sprite();
			itemSprite.addChild( itemImage );
			itemSprite.x = vo.projectilePosition.x;
			itemSprite.y = vo.projectilePosition.y;
			itemSprite.rotation = vo.projectileAngle;
			gameView.addChild( itemSprite );

			var itemBody:Body = PhysicsShapeUtil.createBox( physicsModel.space, vo.projectilePosition, BULLET_WIDTH, BULLET_HEIGHT, 0.0, new Point( 0, 0 ), 0.0, PhysicsConstants.DYNAMIC, CollisionConstants.COLLISION_TYPE_BULLET, entity, 1.0, 0.1, 0.0 );
			itemBody.velocity = new Vec2( vo.projectileVelocity.x, vo.projectileVelocity.y );
			itemBody.rotation = vo.projectileAngle;

			// Enable continuous collision detection
			itemBody.isBullet = true;

			entity.setDataForKey( EntityTypeConstants.ENTITY_TYPE_PROJECTILE, EntityDataConstants.ENTITY_DATA_TYPE );
			entity.setDataForKey( false, EntityDataConstants.ENTITY_DATA_ITEM_TRIGGER );
			entity.setDataForKey( BULLET_LIFETIME, EntityDataConstants.ENTITY_DATA_LIFETIME );

			entity.setDataForKey( new Point( vo.projectilePosition.x, vo.projectilePosition.y ), EntityDataConstants.ENTITY_DATA_POSITION );
			entity.setDataForKey( 0.0, EntityDataConstants.ENTITY_DATA_ANGLE );
			entity.setDataForKey( new Point( vo.projectileVelocity.x, vo.projectileVelocity.y ), EntityDataConstants.ENTITY_DATA_VELOCITY );

			entity.setDataForKey( itemSprite, EntityDataConstants.ENTITY_DATA_SPRITE );
			entity.setDataForKey( itemBody, EntityDataConstants.ENTITY_DATA_PHYSICS_BODY );
			entity.setDataForKey( new ContactSet(), EntityDataConstants.ENTITY_DATA_PHYSICS_CONTACTS );

			// Components for logic
			entitySystem.getComponent( ComponentConstants.COMPONENT_PHYSICS ).addEntity( entity );
			entitySystem.getComponent( ComponentConstants.COMPONENT_SPRITE ).addEntity( entity );
			entitySystem.getComponent( ComponentConstants.COMPONENT_PROJECTILE ).addEntity( entity );
		}
	}
}
