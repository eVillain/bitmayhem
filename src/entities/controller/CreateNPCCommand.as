/**
 * Created by eVillain on 07/10/14.
 */
package entities.controller
{

	import entities.Entity;
	import entities.EntitySystem;
	import entities.constants.ComponentConstants;
	import entities.constants.EntityAnimationConstants;
	import entities.constants.EntityDataConstants;
	import entities.constants.EntityTypeConstants;
	import entities.vos.CreateNPCVO;

	import flash.geom.Point;
	import flash.utils.Dictionary;

	import game.model.GameModel;
	import game.view.GameView;

	import nape.phys.Body;

	import physics.constants.CollisionConstants;
	import physics.constants.PhysicsConstants;
	import physics.contacts.ContactSet;
	import physics.model.PhysicsModel;
	import physics.utils.PhysicsShapeUtil;

	import robotlegs.bender.bundles.mvcs.Command;

	import starling.display.MovieClip;
	import starling.display.Sprite;
	import starling.textures.TextureSmoothing;

	public class CreateNPCCommand extends Command
	{
		[Inject]
		public var physicsModel:PhysicsModel;

		[Inject]
		public var gameView:GameView;

		[Inject]
		public var gameModel:GameModel;

		[Inject]
		public var entitySystem:EntitySystem;

		[Inject]
		public var vo:CreateNPCVO;

		public static const NPC_WIDTH:int = 4;
		public static const NPC_HEIGHT:int = 8;

		override public function execute():void
		{
			trace( "Creating NPC..." );
			var entity:Entity = entitySystem.createEntity();
			// The type here should come from the VO eventually
			entity.setDataForKey( EntityTypeConstants.ENTITY_TYPE_NPC_POLICEMAN, EntityDataConstants.ENTITY_DATA_TYPE );

			setSpriteDataForNPC( entity );

			var body:Body = PhysicsShapeUtil.createBox( physicsModel.space, vo.position, NPC_WIDTH, NPC_HEIGHT, 0.0, new Point( 0, 0 ), 0.0, PhysicsConstants.DYNAMIC, CollisionConstants.COLLISION_TYPE_NPC, entity, 1.0, 1.0, 0.0 );
			body.shapes.at( 0 ).filter.collisionGroup = CollisionConstants.COLLISION_MASK_NPC;
			body.shapes.at( 0 ).filter.collisionMask = ~CollisionConstants.COLLISION_MASK_NPC;
			body.allowRotation = false;

			entity.setDataForKey( new Point( 0, 0 ), EntityDataConstants.ENTITY_DATA_POSITION );
			entity.setDataForKey( 0.0, EntityDataConstants.ENTITY_DATA_ANGLE );
			entity.setDataForKey( new Point( 0, 0 ), EntityDataConstants.ENTITY_DATA_VELOCITY );
			entity.setDataForKey( 100, EntityDataConstants.ENTITY_DATA_HEALTH );
			entity.setDataForKey( new Point( 0, 0 ), EntityDataConstants.ENTITY_DATA_INPUT_MOVEMENT );
			entity.setDataForKey( false, EntityDataConstants.ENTITY_DATA_INPUT_JUMP );
			entity.setDataForKey( 0.0, EntityDataConstants.ENTITY_DATA_INPUT_JUMP_BOOST );
			entity.setDataForKey( false, EntityDataConstants.ENTITY_DATA_INPUT_RUN );
			entity.setDataForKey( new Point( 0, 0 ), EntityDataConstants.ENTITY_DATA_INPUT_WORLD_AIM_POSITION );
			entity.setDataForKey( false, EntityDataConstants.ENTITY_DATA_INPUT_FIRE );
			entity.setDataForKey( false, EntityDataConstants.ENTITY_DATA_INPUT_THROW );
			entity.setDataForKey( false, EntityDataConstants.ENTITY_DATA_PHYSICS_ONGROUND );
			entity.setDataForKey( new ContactSet(), EntityDataConstants.ENTITY_DATA_PHYSICS_CONTACTS );

			entity.setDataForKey( body, EntityDataConstants.ENTITY_DATA_PHYSICS_BODY );

			// Add all the necessary components for logic
			//entitySystem.getComponent( ComponentConstants.COMPONENT_AI ).addEntity( entity );
			entitySystem.getComponent( ComponentConstants.COMPONENT_HEALTH ).addEntity( entity );
			entitySystem.getComponent( ComponentConstants.COMPONENT_PHYSICS ).addEntity( entity );
			entitySystem.getComponent( ComponentConstants.COMPONENT_SPRITE ).addEntity( entity );
			entitySystem.getComponent( ComponentConstants.COMPONENT_ANIMATION ).addEntity( entity );
		}

		private function setSpriteDataForNPC( entity:Entity ):void
		{
			// Get animations set
			var animations:Dictionary = new Dictionary();

			var jumpClip:MovieClip = new MovieClip( Assets.getAtlasGame().getTextures( "BitPoliceHurt" ), 1 );
			animations[EntityAnimationConstants.ENTITY_ANIMATION_JUMPING] = jumpClip;
			jumpClip.smoothing = TextureSmoothing.NONE;
			jumpClip.x = Math.ceil( -jumpClip.width / 2 );
			jumpClip.y = Math.ceil( -jumpClip.height / 2 );

			var standClip:MovieClip = new MovieClip( Assets.getAtlasGame().getTextures( "BitPoliceStand" ), 2 );
			animations[EntityAnimationConstants.ENTITY_ANIMATION_IDLE] = standClip;
			standClip.smoothing = TextureSmoothing.NONE;
			standClip.x = Math.ceil( -standClip.width / 2 );
			standClip.y = Math.ceil( -standClip.height / 2 );

			var walkClip:MovieClip = new MovieClip( Assets.getAtlasGame().getTextures( "BitPoliceWalk" ), 8 );
			walkClip.addFrame( Assets.getAtlasGame().getTexture( "BitThiefStand1" ) );
			animations[EntityAnimationConstants.ENTITY_ANIMATION_WALKING] = walkClip;
			walkClip.smoothing = TextureSmoothing.NONE;
			walkClip.x = Math.ceil( -walkClip.width / 2 );
			walkClip.y = Math.ceil( -walkClip.height / 2 );

			var deathId:uint = uint( Math.random() * 2 ) + 1;	// 2 different death animations
			var deathClip:MovieClip = new MovieClip( Assets.getAtlasGame().getTextures( "BitPoliceDeath" + (deathId).toString() + "-" ), 2 );
			animations[EntityAnimationConstants.ENTITY_ANIMATION_DEATH] = deathClip;
			deathClip.smoothing = TextureSmoothing.NONE;
			deathClip.loop = false;
			deathClip.x = Math.ceil( -deathClip.width / 2 );
			deathClip.y = Math.ceil( -deathClip.height / 2 );

			// Add sprite to game view
			var heroSprite:Sprite = new Sprite();
			gameView.addChild( heroSprite );

			// Add sprite and animation data to entity
			entity.setDataForKey( heroSprite, EntityDataConstants.ENTITY_DATA_SPRITE );
			entity.setDataForKey( EntityAnimationConstants.ENTITY_ANIMATION_IDLE, EntityDataConstants.ENTITY_DATA_ANIMATION_STATE );
			entity.setDataForKey( EntityAnimationConstants.ENTITY_ANIMATION_UNINITIALIZED, EntityDataConstants.ENTITY_DATA_ANIMATION_PREVIOUS );
			entity.setDataForKey( 1.0, EntityDataConstants.ENTITY_DATA_ANIMATION_SPEED );
			entity.setDataForKey( animations, EntityDataConstants.ENTITY_DATA_ANIMATION_SET );

		}
	}
}
