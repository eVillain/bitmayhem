/**
 * Created by eVillain on 02/09/14.
 */
package entities.constants
{

	public class ComponentConstants
	{
		public static const COMPONENT_SPRITE:String = "COMPONENT_SPRITE";
		public static const COMPONENT_HUMAN:String = "COMPONENT_HUMAN";
		public static const COMPONENT_PHYSICS:String = "COMPONENT_PHYSICS";
		public static const COMPONENT_ANIMATION:String = "COMPONENT_ANIMATION";
		public static const COMPONENT_PROJECTILE_SPAWNER:String = "COMPONENT_PROJECTILE_SPAWNER";
		public static const COMPONENT_PROJECTILE:String = "COMPONENT_PROJECTILE";
		public static const COMPONENT_HEALTH:String = "COMPONENT_HEALTH";
	}
}
