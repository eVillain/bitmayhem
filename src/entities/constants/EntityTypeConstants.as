/**
 * Created by eVillain on 07/09/14.
 */
package entities.constants
{

	public class EntityTypeConstants
	{
		public static const ENTITY_TYPE_PLAYER:String = "ENTITY_TYPE_PLAYER";
		public static const ENTITY_TYPE_ITEM:String = "ENTITY_TYPE_ITEM";
		public static const ENTITY_TYPE_PROJECTILE:String = "ENTITY_TYPE_PROJECTILE";

		public static const ENTITY_TYPE_NPC_POLICEMAN:String = "ENTITY_TYPE_NPC_POLICEMAN";
		public static const ENTITY_TYPE_NPC_DOG:String = "ENTITY_TYPE_NPC_DOG";
	}
}
