/**
 * Created by eVillain on 09/09/14.
 */
package entities.constants
{

	public class EntityAnimationConstants
	{
		public static const ENTITY_ANIMATION_UNINITIALIZED:uint = 0;
		public static const ENTITY_ANIMATION_IDLE:uint = 1;
		public static const ENTITY_ANIMATION_WALKING:uint = 2;
		public static const ENTITY_ANIMATION_JUMPING:uint = 3;
		public static const ENTITY_ANIMATION_DIVING:uint = 4;
		public static const ENTITY_ANIMATION_DEATH:uint = 5;
	}
}
