/**
 * Created by eVillain on 01/10/14.
 */
package entities.constants
{

	public class EntityProjectileConstants
	{
		public static const PROJECTILE_TYPE_ROCK:String = "PROJECTILE_TYPE_ROCK";
		public static const PROJECTILE_TYPE_BULLET:String = "PROJECTILE_TYPE_BULLET";
		public static const PROJECTILE_TYPE_ROCKET:String = "PROJECTILE_TYPE_ROCKET";

	}
}
