/**
 * Created by eVillain on 05/10/14.
 */
package entities.constants
{

	public class EntityItemConstants
	{
		public static const ITEM_ID_PISTOL:uint = 2;
		public static const ITEM_ID_MACHINEGUN:uint = 3;
		public static const ITEM_ID_SHOTGUN:uint = 4;
		public static const ITEM_ID_RPG:uint = 5;
		public static const ITEM_ID_GRENADE:uint = 6;
		public static const ITEM_ID_BATON:uint = 7;
		public static const ITEM_ID_RIOT_SHIELD:uint = 8;
		public static const ITEM_ID_DAGGER:uint = 9;

	}
}
