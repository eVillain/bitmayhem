/**
 * Created by eVillain on 31/08/14.
 */
package entities.constants
{

	public class EntityDataConstants
	{
		// General entity data
		public static const ENTITY_DATA_ID:String = "ENTITY_DATA_ID";	// uint
		public static const ENTITY_DATA_TYPE:String = "ENTITY_DATA_TYPE";	// String
		public static const ENTITY_DATA_NAME:String = "ENTITY_DATA_NAME";	// String
		public static const ENTITY_DATA_HEALTH:String = "ENTITY_DATA_HEALTH";	// uint
		public static const ENTITY_DATA_WEIGHT:String = "ENTITY_DATA_WEIGHT";	// Number
		public static const ENTITY_DATA_HOLDING:String = "ENTITY_DATA_HOLDING";	// Entity	(Item/Entity which is being held by this)
		public static const ENTITY_DATA_HOLDER:String = "ENTITY_DATA_HOLDER";	// Entity	(Item/Entity which is holding this)
		public static const ENTITY_DATA_INVENTORY:String = "ENTITY_DATA_INVENTORY";	// Vector.<Entity>	(Entities which are in inventory)
		public static const ENTITY_DATA_LIFETIME:String = "ENTITY_DATA_LIFETIME";	// Number (in seconds until entity dies/expires)

		// Animation data
		public static const ENTITY_DATA_ANIMATION_SET:String = "ENTITY_DATA_ANIMATION_SET";	// Dictionary (key EntityAnimationConstants, value MovieClip)
		public static const ENTITY_DATA_ANIMATION_STATE:String = "ENTITY_DATA_ANIMATION_STATE";	// uint (EntityAnimationConstants)
		public static const ENTITY_DATA_ANIMATION_PREVIOUS:String = "ENTITY_DATA_ANIMATION_PREVIOUS";// uint (EntityAnimationConstants)
		public static const ENTITY_DATA_ANIMATION_SPEED:String = "ENTITY_DATA_ANIMATION_SPEED";// Number (FPS)

		// Sprite/MovieClip
		public static const ENTITY_DATA_SPRITE:String = "ENTITY_DATA_SPRITE";	// starling.display.Sprite
		public static const ENTITY_DATA_MOVIECLIP:String = "ENTITY_DATA_MOVIECLIP";	// starling.display.MovieClip

		// Location and velocity data
		public static const ENTITY_DATA_POSITION:String = "ENTITY_DATA_POSITION";	// Point
		public static const ENTITY_DATA_ANGLE:String = "ENTITY_DATA_ANGLE";			// Number
		public static const ENTITY_DATA_VELOCITY:String = "ENTITY_DATA_VELOCITY";	// Point
		//public static const ENTITY_DATA_ANGULAR_VELOCITY:String = "ENTITY_DATA_ANGULAR_VELOCITY";	// Number

		// Physics data
		public static const ENTITY_DATA_PHYSICS_BODY:String = "ENTITY_DATA_PHYSICS_BODY";	// Body
		public static const ENTITY_DATA_PHYSICS_CONTACTS:String = "ENTITY_DATA_PHYSICS_CONTACTS";	// ContactSet
		public static const ENTITY_DATA_PHYSICS_ONGROUND:String = "ENTITY_DATA_PHYSICS_ONGROUND";	// Boolean

		// Input data
		public static const ENTITY_DATA_INPUT_MOVEMENT:String = "ENTITY_DATA_INPUT_MOVEMENT";	// Point
		public static const ENTITY_DATA_INPUT_JUMP:String = "ENTITY_DATA_INPUT_JUMP";	// Boolean
		public static const ENTITY_DATA_INPUT_JUMP_BOOST:String = "ENTITY_DATA_INPUT_JUMP_BOOST";	// Number
		public static const ENTITY_DATA_INPUT_RUN:String = "ENTITY_DATA_INPUT_RUN";	// Boolean
		public static const ENTITY_DATA_INPUT_WORLD_AIM_POSITION:String = "ENTITY_DATA_INPUT_WORLD_AIM_POSITION";	// Point
		public static const ENTITY_DATA_INPUT_FIRE:String = "ENTITY_DATA_INPUT_FIRE";	// Boolean
		public static const ENTITY_DATA_INPUT_THROW:String = "ENTITY_DATA_INPUT_THROW";	// Boolean

		// Item data
		public static const ENTITY_DATA_ITEM_ID:String = "ENTITY_DATA_ITEM_ID";	// uint (EntityItemConstants)
		public static const ENTITY_DATA_ITEM_TRIGGER:String = "ENTITY_DATA_ITEM_TRIGGER";	// Boolean
		public static const ENTITY_DATA_ITEM_TRIGGER_RATE:String = "ENTITY_DATA_ITEM_TRIGGER_RATE";	// Number (in seconds, how often to trigger)
		public static const ENTITY_DATA_ITEM_TRIGGER_ACCUMULATOR:String = "ENTITY_DATA_ITEM_TRIGGER_ACCUMULATOR";	// Number (in seconds. since we last triggered)
		public static const ENTITY_DATA_ITEM_AMOUNT:String = "ENTITY_DATA_ITEM_AMOUNT";	// uint
		public static const ENTITY_DATA_ITEM_PROJECTILE_TYPE:String = "ENTITY_DATA_ITEM_PROJECTILE_TYPE";	// String (EntityProjectileConstants)
		public static const ENTITY_DATA_ITEM_PROJECTILE_AMOUNT:String = "ENTITY_DATA_ITEM_PROJECTILE_AMOUNT";	// uint
		public static const ENTITY_DATA_ITEM_PROJECTILE_OFFSET:String = "ENTITY_DATA_ITEM_PROJECTILE_OFFSET";	// Point

	}
}
