/**
 * Created by eVillain on 07/09/14.
 */
package entities.signals
{

	import entities.vos.CreatePlayerVO;

	import org.osflash.signals.Signal;

	public class CreatePlayerSignal extends Signal
	{
		public function CreatePlayerSignal()
		{
			super( CreatePlayerVO );
		}
	}
}
