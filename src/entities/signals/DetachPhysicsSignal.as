/**
 * Created by eVillain on 25/10/14.
 */
package entities.signals
{

	import entities.Entity;

	import org.osflash.signals.Signal;

	public class DetachPhysicsSignal extends Signal
	{
		public function DetachPhysicsSignal()
		{
			super( Entity );
		}
	}
}
