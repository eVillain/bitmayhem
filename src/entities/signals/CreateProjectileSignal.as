/**
 * Created by eVillain on 01/10/14.
 */
package entities.signals
{

	import entities.vos.CreateProjectileVO;

	import org.osflash.signals.Signal;

	public class CreateProjectileSignal extends Signal
	{
		public function CreateProjectileSignal()
		{
			super( CreateProjectileVO );
		}
	}
}
