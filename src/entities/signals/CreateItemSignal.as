/**
 * Created by eVillain on 21/09/14.
 */
package entities.signals
{

	import entities.vos.CreateItemVO;

	import org.osflash.signals.Signal;

	public class CreateItemSignal extends Signal
	{
		public function CreateItemSignal()
		{
			super( CreateItemVO );
		}
	}
}
