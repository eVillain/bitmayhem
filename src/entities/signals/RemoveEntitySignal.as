/**
 * Created by eVillain on 22/03/15.
 */
package entities.signals
{

	import org.osflash.signals.Signal;

	public class RemoveEntitySignal extends Signal
	{
		public function RemoveEntitySignal()
		{
			super( uint );
		}
	}
}
