/**
 * Created by eVillain on 07/10/14.
 */
package entities.signals
{

	import entities.vos.CreateNPCVO;

	import org.osflash.signals.Signal;

	public class CreateNPCSignal extends Signal
	{
		public function CreateNPCSignal()
		{
			super( CreateNPCVO );
		}
	}
}
