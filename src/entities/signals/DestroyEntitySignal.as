/**
 * Created by eVillain on 01/10/14.
 */
package entities.signals
{

	import org.osflash.signals.Signal;

	public class DestroyEntitySignal extends Signal
	{
		public function DestroyEntitySignal()
		{
			super( uint );
		}
	}
}
