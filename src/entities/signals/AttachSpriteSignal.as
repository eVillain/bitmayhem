/**
 * Created by eVillain on 25/10/14.
 */
package entities.signals
{

	import entities.Entity;

	import org.osflash.signals.Signal;

	public class AttachSpriteSignal extends Signal
	{
		public function AttachSpriteSignal()
		{
			super( Entity );
		}
	}
}
