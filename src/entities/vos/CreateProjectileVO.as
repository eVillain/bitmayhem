/**
 * Created by eVillain on 01/10/14.
 */
package entities.vos
{

	import flash.geom.Point;

	public class CreateProjectileVO
	{
		private var _projectileType:String;
		private var _projectilePosition:Point;
		private var _projectileVelocity:Point;
		private var _projectileAngle:Number;

		public function CreateProjectileVO( projectileType:String, projectilePosition:Point, projectileVelocity:Point, projectileAngle:Number )
		{
			this._projectileType = projectileType;
			this._projectilePosition = projectilePosition;
			this._projectileVelocity = projectileVelocity;
			this._projectileAngle = projectileAngle;
		}

		public function get projectileType():String
		{
			return _projectileType;
		}

		public function get projectilePosition():Point
		{
			return _projectilePosition;
		}

		public function get projectileVelocity():Point
		{
			return _projectileVelocity;
		}

		public function get projectileAngle():Number
		{
			return _projectileAngle;
		}
	}
}
