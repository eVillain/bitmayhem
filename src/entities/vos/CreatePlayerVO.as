/**
 * Created by eVillain on 07/09/14.
 */
package entities.vos
{

	import flash.geom.Point;

	public class CreatePlayerVO
	{
		private var _playerPosition:Point;

		public function CreatePlayerVO( playerPosition:Point )
		{
			this._playerPosition = playerPosition;
		}

		public function get playerPosition():Point
		{
			return _playerPosition;
		}
	}
}
