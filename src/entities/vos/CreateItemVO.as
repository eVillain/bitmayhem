/**
 * Created by eVillain on 21/09/14.
 */
package entities.vos
{

	import flash.geom.Point;

	public class CreateItemVO
	{
		public var itemID:uint;
		public var position:Point;
		public var rotation:Number;

		public function CreateItemVO( itemID:uint, position:Point, rotation:Number )
		{
			this.itemID = itemID;
			this.position = position;
			this.rotation = rotation;
		}

	}
}
