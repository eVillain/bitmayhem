/**
 * Created by eVillain on 07/10/14.
 */
package entities.vos
{

	import flash.geom.Point;

	public class CreateNPCVO
	{
		private var _position:Point;
		// Type of NPC will go here also

		public function CreateNPCVO( position:Point )
		{
			this._position = position;
		}

		public function get position():Point
		{
			return _position;
		}
	}
}
