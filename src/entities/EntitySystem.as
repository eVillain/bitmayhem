/**
 * Created by eVillain on 31/08/14.
 */
package entities
{

	import entities.components.AnimationComponent;
	import entities.components.HealthComponent;
	import entities.components.HumanComponent;
	import entities.components.IComponent;
	import entities.components.PhysicsComponent;
	import entities.components.ProjectileComponent;
	import entities.components.ProjectileSpawnerComponent;
	import entities.components.SpriteComponent;

	import flash.utils.Dictionary;

	import gametick.vos.GameTickVO;

	public class EntitySystem
	{
		[Inject]
		public var humanComponent:HumanComponent;

		[Inject]
		public var physicsComponent:PhysicsComponent;

		[Inject]
		public var spriteComponent:SpriteComponent;

		[Inject]
		public var animationComponent:AnimationComponent;

		[Inject]
		public var projectileSpawnerComponent:ProjectileSpawnerComponent;

		[Inject]
		public var projectileComponent:ProjectileComponent;

		[Inject]
		public var healthComponent:HealthComponent;

		private var m_nextEntityID:uint = 0;

		protected var entities:Dictionary = new Dictionary();
		protected var components:Vector.<IComponent> = new Vector.<IComponent>();

		public function EntitySystem()
		{
			trace( "-------> EntitySystem constructed" );
		}

		[PostConstruct]
		public function init()
		{
			trace( "-------> EntitySystem Post construct init" );
			// These have to be in a rather specific order so don't go messing around here willy-nilly
			components.push( physicsComponent );			// Reads physics data then updates position, angle and velocity data
			components.push( humanComponent );				// Reads input and applies player logic (manipulates physics)
			components.push( spriteComponent );				// Updates sprite position and angle
			components.push( animationComponent );			// Updates animation
			components.push( projectileSpawnerComponent );	// Spawns projectiles when triggered
			components.push( projectileComponent );			// Checks collisions, does damage and expires over time
			components.push( healthComponent );				// If we ran out removes entity
		}

		public function createEntity():Entity
		{
			// Create a new unique ID and use that to create the new entity
			var newEntityID:uint = ++m_nextEntityID;
			var newEntity:Entity = new Entity( newEntityID );

			// Store the newly created entity in our dictionary
			entities[newEntityID] = newEntity;

			return newEntity;
		}

		public function getEntityForKey( key:uint ):Entity
		{
			return entities[key];
		}

		public function getComponent( componentType:String ):IComponent
		{
			for each (var component:IComponent in components)
			{
				if (component.getType() == componentType)
				{
					return component;
				}
			}
			return null;
		}

		public function update( gameTick:GameTickVO ):void
		{
			if (!gameTick)
			{
				return;
			}

			for each (var component:IComponent in components)
			{
				//trace( "Updating component: " + component.getType() );
				component.update( gameTick.deltaTime );
			}
		}

		public function removeEntity( key:uint ):void
		{
			if (entities[key])
			{
				for each (var component:IComponent in components)
				{
					component.removeEntity( key );
				}
				delete entities[key];
			}
		}
	}
}
