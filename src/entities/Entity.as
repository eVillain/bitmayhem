/**
 * Created by eVillain on 31/08/14.
 */
package entities
{

	import entities.constants.EntityDataConstants;

	import flash.utils.Dictionary;

	public class Entity implements IEntity
	{
		private var _dataBucket:Dictionary = new Dictionary();

		public function Entity( entityID:uint )
		{
			setDataForKey( entityID, EntityDataConstants.ENTITY_DATA_ID );
		}

		public function hasDataObjectForKey( key:String ):Boolean
		{
			return _dataBucket.hasOwnProperty( key );
		}

		public function getDataForKey( key:String ):Object
		{
			if (!_dataBucket.hasOwnProperty( key ))
			{
				throw new Error( "Entity " + getID().toString() + " did not have a " + key + " data object." );
			}
			return _dataBucket[key];
		}

		public function setDataForKey( data:Object, key:String ):void
		{
			_dataBucket[key] = data;
		}

		public function removeDataForKey( key:String ):void
		{
			if (_dataBucket.hasOwnProperty( key ))
			{
				delete _dataBucket[key];
			}
		}

		public function getID():uint
		{
			return getDataForKey( EntityDataConstants.ENTITY_DATA_ID ) as uint;
		}
	}
}
