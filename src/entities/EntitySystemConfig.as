/**
 * Created by eVillain on 31/08/14.
 */
package entities
{

	import entities.components.AnimationComponent;
	import entities.components.HealthComponent;
	import entities.components.HumanComponent;
	import entities.components.PhysicsComponent;
	import entities.components.ProjectileComponent;
	import entities.components.ProjectileSpawnerComponent;
	import entities.components.SpriteComponent;
	import entities.controller.AttachPhysicsCommand;
	import entities.controller.AttachSpriteCommand;
	import entities.controller.CreateItemCommand;
	import entities.controller.CreateNPCCommand;
	import entities.controller.CreatePlayerCommand;
	import entities.controller.CreateProjectileCommand;
	import entities.controller.DestroyEntityCommand;
	import entities.controller.DetachPhysicsCommand;
	import entities.controller.DetachSpriteCommand;
	import entities.controller.RemoveEntityCommand;
	import entities.signals.AttachPhysicsSignal;
	import entities.signals.AttachSpriteSignal;
	import entities.signals.CreateItemSignal;
	import entities.signals.CreateNPCSignal;
	import entities.signals.CreatePlayerSignal;
	import entities.signals.CreateProjectileSignal;
	import entities.signals.DestroyEntitySignal;
	import entities.signals.DetachPhysicsSignal;
	import entities.signals.DetachSpriteSignal;
	import entities.signals.RemoveEntitySignal;

	import robotlegs.bender.extensions.signalCommandMap.api.ISignalCommandMap;
	import robotlegs.bender.framework.api.IConfig;
	import robotlegs.bender.framework.api.IInjector;

	public class EntitySystemConfig implements IConfig
	{
		[Inject]
		public var injector:IInjector;

		[Inject]
		public var signalCommandMap:ISignalCommandMap;

		public function configure():void
		{
			// Map entity creation controller
			signalCommandMap.map( DestroyEntitySignal ).toCommand( DestroyEntityCommand );
			signalCommandMap.map( RemoveEntitySignal ).toCommand( RemoveEntityCommand );
			signalCommandMap.map( CreatePlayerSignal ).toCommand( CreatePlayerCommand );
			signalCommandMap.map( CreateItemSignal ).toCommand( CreateItemCommand );
			signalCommandMap.map( CreateProjectileSignal ).toCommand( CreateProjectileCommand );
			signalCommandMap.map( CreateNPCSignal ).toCommand( CreateNPCCommand );

			// Entity attaching and detaching from world controller
			signalCommandMap.map( AttachPhysicsSignal ).toCommand( AttachPhysicsCommand );
			signalCommandMap.map( DetachPhysicsSignal ).toCommand( DetachPhysicsCommand );
			signalCommandMap.map( AttachSpriteSignal ).toCommand( AttachSpriteCommand );
			signalCommandMap.map( DetachSpriteSignal ).toCommand( DetachSpriteCommand );

			// Map components in injector
			injector.map( HumanComponent ).asSingleton();
			injector.map( PhysicsComponent ).asSingleton();
			injector.map( SpriteComponent ).asSingleton( true );
			injector.map( AnimationComponent ).asSingleton( true );
			injector.map( ProjectileSpawnerComponent ).asSingleton( true );
			injector.map( ProjectileComponent ).asSingleton( true );
			injector.map( HealthComponent ).asSingleton( true );

			// Map EntitySystem singleton
			injector.map( EntitySystem ).asSingleton();
		}
	}
}
