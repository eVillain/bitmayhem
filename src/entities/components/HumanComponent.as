/**
 * Created by eVillain on 31/08/14.
 */
package entities.components
{

	import entities.Entity;
	import entities.constants.ComponentConstants;
	import entities.constants.EntityAnimationConstants;
	import entities.constants.EntityDataConstants;
	import entities.constants.EntityTypeConstants;
	import entities.signals.DetachPhysicsSignal;
	import entities.signals.DetachSpriteSignal;

	import flash.geom.Point;

	import nape.geom.Vec2;
	import nape.phys.Body;

	import physics.constants.CollisionConstants;
	import physics.contacts.ContactSet;
	import physics.model.PhysicsModel;

	import starling.display.Sprite;

	public class HumanComponent extends BaseComponent
	{
		[Inject]
		public var physicsModel:PhysicsModel;

		[Inject]
		public var detachSpriteSignal:DetachSpriteSignal;

		[Inject]
		public var detachPhysicsSignal:DetachPhysicsSignal;

		public static const MAX_RUN_SPEED:int = 80;
		public static const MAX_WALK_SPEED:int = 40;

		public static const JUMP_SPEED:int = 40;
		public static const JUMP_BOOST:Number = 0.15;
		public static const WALK_FPS:int = 12.0;
		public static const STAND_FPS:int = 2.0;
		public static const THROW_VELOCITY:Number = 40.0;
		public static const THROW_GAP:Number = 8.0;

		override public function getType():String
		{
			return ComponentConstants.COMPONENT_HUMAN;
		}

		override public function update( deltaTime:Number ):void
		{
			if (!physicsModel.updated)
			{
				return;
			}

			for each (var entity:Entity in _entities)
			{
				execute( entity, deltaTime );
			}
		}

		override public function execute( entity:Entity, deltaTime:Number ):void
		{
			// Get input and physics body data
			var inputMovement:Point = entity.getDataForKey( EntityDataConstants.ENTITY_DATA_INPUT_MOVEMENT ) as Point;
			var inputJump:Boolean = entity.getDataForKey( EntityDataConstants.ENTITY_DATA_INPUT_JUMP ) as Boolean;
			var inputJumpBoost:Number = entity.getDataForKey( EntityDataConstants.ENTITY_DATA_INPUT_JUMP_BOOST ) as Number;
			var inputRun:Boolean = entity.getDataForKey( EntityDataConstants.ENTITY_DATA_INPUT_RUN ) as Boolean;
			var body:Body = entity.getDataForKey( EntityDataConstants.ENTITY_DATA_PHYSICS_BODY ) as Body;

			// Check if body is grounded
			checkContacts( entity );

			// Get ground collision status
			var onGround:Boolean = entity.getDataForKey( EntityDataConstants.ENTITY_DATA_PHYSICS_ONGROUND ) as Boolean;

			// Get previous body velocity
			var previousVelocity:Vec2 = body.velocity;

			// Figure out new velocity
			var newVelocity:Vec2 = new Vec2( previousVelocity.x, previousVelocity.y );
			if (( inputMovement.x < 0 && previousVelocity.x > 0 ) || (inputMovement.x > 0 && previousVelocity.x < 0 ))
			{
				// Player switched direction of movement, we should help with that a bit
				newVelocity.x *= -0.25;
			}

			// Check if there was horizontal input
			var horizontalInput:Boolean = Math.abs( inputMovement.x ) > 0;

			if (onGround)
			{
				//trace( "On ground: " + previousVelocity.x + ", " + previousVelocity.y);
				// -------- Ground movement --------
				if (horizontalInput)
				{
					newVelocity.x = inputMovement.x * (inputRun ? MAX_RUN_SPEED : MAX_WALK_SPEED);
					body.shapes.at( 0 ).material.staticFriction = 0;
					body.shapes.at( 0 ).material.dynamicFriction = 0;
				}
				else
				{
					body.shapes.at( 0 ).material.staticFriction = 1;
					body.shapes.at( 0 ).material.dynamicFriction = 1;
				}

				if (inputJump)
				{
					// Player jumped, check if we are running for a dive
					// This is commented out for now because the dive mechanic is going to require some extra ugly code (probably)
//					if ( inputRun && horizontalInput )
//					{
//						// Player diving
//						entity.setDataForKey( EntityAnimationConstants.ENTITY_ANIMATION_DIVING, EntityDataConstants.ENTITY_DATA_ANIMATION_STATE );
//						newVelocity.y = -JUMP_SPEED/2;
//					}
//					else
//					{
					entity.setDataForKey( JUMP_BOOST, EntityDataConstants.ENTITY_DATA_INPUT_JUMP_BOOST );
					newVelocity.y = -JUMP_SPEED;
//					}
				}
			}
			else
			{
				//  -------- In-air movement --------
				if (inputJump)
				{
					if (inputJumpBoost > 0.0)
					{
						// Already jumped and still jumping upward
						newVelocity.y = -JUMP_SPEED;
						inputJumpBoost -= deltaTime;
						entity.setDataForKey( inputJumpBoost, EntityDataConstants.ENTITY_DATA_INPUT_JUMP_BOOST );
					}
				}
				else if (inputJumpBoost != 0.0)
				{
					// Stopped jump but still could have held it longer, reset jump boost
					// In the future we will have to check for double jump ability here and mark that we jumped once already
					//trace("Jump boost reset: " + inputJumpBoost );
					entity.setDataForKey( 0.0, EntityDataConstants.ENTITY_DATA_INPUT_JUMP_BOOST );
				}

				if (horizontalInput)
				{
					newVelocity.x = inputMovement.x * (inputRun ? MAX_RUN_SPEED : MAX_WALK_SPEED);
				}
			}

			// Flip sprite according to movement direction (if changed)
			if (newVelocity.x != previousVelocity.x)
			{
				// Get sprite and flip according to velocity
				var sprite:Sprite = entity.getDataForKey( EntityDataConstants.ENTITY_DATA_SPRITE ) as Sprite;
				if (newVelocity.x < 0.0)
				{
					sprite.scaleX = sprite.scaleX < 0 ? sprite.scaleX : -sprite.scaleX;
				}
				else
				{
					sprite.scaleX = sprite.scaleX > 0 ? sprite.scaleX : -sprite.scaleX;
				}
			}

			// Clamp maximum velocity
			/*			if (inputRun)
			 {
			 newVelocity.x = Math.min( newVelocity.x, MAX_RUN_SPEED );
			 newVelocity.x = Math.max( newVelocity.x, -MAX_RUN_SPEED );
			 }
			 else
			 {
			 newVelocity.x = Math.min( newVelocity.x, MAX_WALK_SPEED );
			 newVelocity.x = Math.max( newVelocity.x, -MAX_WALK_SPEED );
			 }*/

			// Update the item we're holding
			if (entity.hasDataObjectForKey( EntityDataConstants.ENTITY_DATA_HOLDING ))
			{
				updateHeldItemForEntity( entity );
			}

			// Hack to use impulse instead of setting velocity. I don't
			// particularly like it but it's here for testing different
			// methods for player motion
			//newVelocity.x *= 0.1;
			//newVelocity.y *= 0.1;
			//body.applyImpulse( newVelocity );

			// Set the final velocity
			body.velocity = newVelocity;

			// Update player animation state
			updateAnimation( entity, onGround, newVelocity );
		}

		private function checkContacts( entity:Entity ):void
		{
			// Get contacts and check if we are on the ground and whether we want to pick up items
			var contactSet:ContactSet = entity.getDataForKey( EntityDataConstants.ENTITY_DATA_PHYSICS_CONTACTS ) as ContactSet;
			var inputMovement:Point = entity.getDataForKey( EntityDataConstants.ENTITY_DATA_INPUT_MOVEMENT ) as Point;
			var inventory:Vector.<Entity> = entity.getDataForKey( EntityDataConstants.ENTITY_DATA_INVENTORY ) as Vector.<Entity>;

			var pickUpLoot:Boolean = (inputMovement.y == -1);	// Pick up items by holding crouch (down-arrow)
			var onGround:Boolean = false;

			for each(var body:Body in contactSet.contacts)
			{
				var ignoredItem:Boolean = false;
				if (body.userData.data)
				{
					var contactEntity:Entity = body.userData.data as Entity;

					var entityType:String = contactEntity.getDataForKey( EntityDataConstants.ENTITY_DATA_TYPE ) as String;
					if (entityType == EntityTypeConstants.ENTITY_TYPE_ITEM)
					{
						const COLLISION_VELOCITY_HURT_THRESHOLD:Number = 10;
						if (body.velocity.length > COLLISION_VELOCITY_HURT_THRESHOLD)
						{
							// Item should collide and hurt human, then stop moving
							// TODO: Dispatch collision hurt entity signal here
							body.velocity = new Vec2();
						}
						else if (pickUpLoot)
						{
							// We want to pick up the item
							if (entity.hasDataObjectForKey( EntityDataConstants.ENTITY_DATA_HOLDING ))
							{
								// We are already holding some kind of entity
								if (contactEntity == entity.getDataForKey( EntityDataConstants.ENTITY_DATA_HOLDING ))
								{
									// This is just the item we are holding, for some reason it collided with us :(
									// This check shouldn't be necessary in the future
									trace( "HumanComponent WARNING: Entity " + entity.getID().toString() + " collided with object it was holding!" );
									ignoredItem = true;
									continue;
								}
								else if (inventory)
								{
									// It's a new item and we want to pick it up, put it in inventory
									inventory.push( contactEntity );
									// Remove contactEntity Body from physics and Sprite from view
									detachSpriteSignal.dispatch( contactEntity );
									detachPhysicsSignal.dispatch( contactEntity );
								}
							}
							else
							{
								// Grab item
								contactEntity.setDataForKey( entity, EntityDataConstants.ENTITY_DATA_HOLDER );
								entity.setDataForKey( contactEntity, EntityDataConstants.ENTITY_DATA_HOLDING );
								// Filter out collisions, this is sort of a weird one :D
								// Everything with the same group value will not collide
								var length:int = body.shapes.length;
								for (var i:uint = 0; i < length; i++)
								{
									body.shapes.at( i ).filter.collisionGroup = CollisionConstants.COLLISION_MASK_PLAYER1; // Belong to player group
									body.shapes.at( i ).filter.collisionMask = ~CollisionConstants.COLLISION_MASK_PLAYER1; // Collide with everything but player
								}
							}
						}
					}
				}
				if (!ignoredItem)
				{
					onGround = true;
				}
			}
			// Save grounded data
			entity.setDataForKey( onGround, EntityDataConstants.ENTITY_DATA_PHYSICS_ONGROUND );
		}

		private function updateHeldItemForEntity( entity:Entity ):void
		{
			var heldThing:Entity = entity.getDataForKey( EntityDataConstants.ENTITY_DATA_HOLDING ) as Entity;
			var inputThrow:Boolean = entity.getDataForKey( EntityDataConstants.ENTITY_DATA_INPUT_THROW ) as Boolean;
			var position:Point = entity.getDataForKey( EntityDataConstants.ENTITY_DATA_POSITION ) as Point;
			var velocity:Point = entity.getDataForKey( EntityDataConstants.ENTITY_DATA_VELOCITY ) as Point;
			var inputAim:Point = entity.getDataForKey( EntityDataConstants.ENTITY_DATA_INPUT_WORLD_AIM_POSITION ) as Point;
			var inputFire:Boolean = entity.getDataForKey( EntityDataConstants.ENTITY_DATA_INPUT_FIRE ) as Boolean;

			// The angle of our held object - we still need to check if we are pointing to the left or right and flip the sprite
			var aimAngle:Number = Math.atan2( inputAim.y - position.y, inputAim.x - position.x );
			//trace( "Aim angle: " + aimAngle.toString() );

			if (inputThrow)
			{
				throwHeldThing( entity, heldThing, aimAngle, position );
			}
			else
			{
				// Update position, velocity and angle
				heldThing.setDataForKey( position, EntityDataConstants.ENTITY_DATA_POSITION );
				heldThing.setDataForKey( aimAngle, EntityDataConstants.ENTITY_DATA_ANGLE );
				heldThing.setDataForKey( velocity, EntityDataConstants.ENTITY_DATA_VELOCITY );
				// Trigger the thing if we are holding fire
				heldThing.setDataForKey( inputFire, EntityDataConstants.ENTITY_DATA_ITEM_TRIGGER );
				// Update physics if available
				if (physicsModel.updated && heldThing.hasDataObjectForKey( EntityDataConstants.ENTITY_DATA_PHYSICS_BODY ))
				{
					var heldThingBody:Body = heldThing.getDataForKey( EntityDataConstants.ENTITY_DATA_PHYSICS_BODY ) as Body;
					heldThingBody.position = new Vec2( position.x, position.y );
					heldThingBody.velocity = new Vec2( velocity.x, velocity.y );
					heldThingBody.rotation = aimAngle;
					heldThingBody.angularVel = 0.0;
				}
			}
		}

		static public function throwHeldThing( entity:Entity, heldThing:Entity, aimAngle:Number, position:Point ):void
		{
			// Release trigger just in case
			heldThing.setDataForKey( false, EntityDataConstants.ENTITY_DATA_ITEM_TRIGGER );

			// Grab item
			heldThing.removeDataForKey( EntityDataConstants.ENTITY_DATA_HOLDER );

			entity.removeDataForKey( EntityDataConstants.ENTITY_DATA_HOLDING );

			// Update physics data if available
			if (heldThing.hasDataObjectForKey( EntityDataConstants.ENTITY_DATA_PHYSICS_BODY ))
			{
				var heldThingBody:Body = heldThing.getDataForKey( EntityDataConstants.ENTITY_DATA_PHYSICS_BODY ) as Body;

				// Remove collision filter, this still needs some improvement
				var length:int = heldThingBody.shapes.length;
				for (var i:int = 0; i < length; i++)
				{
					heldThingBody.shapes.at( i ).filter.collisionGroup = CollisionConstants.COLLISION_MASK_ITEM;
					heldThingBody.shapes.at( i ).filter.collisionMask = -1; // Collide with everything
				}

				// Add some speed
				var holderVelocity:Point = entity.getDataForKey( EntityDataConstants.ENTITY_DATA_VELOCITY ) as Point;
				var throwDirectionX:Number = Math.cos( aimAngle );
				var throwDirectionY:Number = Math.sin( aimAngle );
				var heldThingPosition:Point = new Point( position.x + throwDirectionX * THROW_GAP, position.y + throwDirectionY * THROW_GAP );
				var throwVelocity:Point = new Point( holderVelocity.x + throwDirectionX * THROW_VELOCITY, holderVelocity.y + throwDirectionY * THROW_VELOCITY );

				heldThingBody.position = new Vec2( heldThingPosition.x, heldThingPosition.y );
				heldThingBody.velocity = new Vec2( throwVelocity.x, throwVelocity.y );

				// Add some spin
				heldThingBody.angularVel = 5 + Math.random() * 2;
			}
		}

		private function updateAnimation( entity:Entity, onGround:Boolean, velocity:Vec2 ):void
		{
			if (onGround)
			{
				// Player is on ground
				if (Math.abs( velocity.x ) < 0.01)	// horizontal velocity of 0.01 is the magic threshold for standing/moving
				{
					// Player is just standing around idle
					entity.setDataForKey( STAND_FPS, EntityDataConstants.ENTITY_DATA_ANIMATION_SPEED );
					entity.setDataForKey( EntityAnimationConstants.ENTITY_ANIMATION_IDLE, EntityDataConstants.ENTITY_DATA_ANIMATION_STATE );
				}
				else
				{
					entity.setDataForKey( EntityAnimationConstants.ENTITY_ANIMATION_WALKING, EntityDataConstants.ENTITY_DATA_ANIMATION_STATE );
					// Set animation speed to relative to horizontal velocity
					var relativeSpeed:Number = Math.abs( velocity.x / MAX_WALK_SPEED );
					entity.setDataForKey( relativeSpeed * WALK_FPS, EntityDataConstants.ENTITY_DATA_ANIMATION_SPEED );
				}
			}
			else
			{
				// Player is jumping
				entity.setDataForKey( 1, EntityDataConstants.ENTITY_DATA_ANIMATION_SPEED );
				entity.setDataForKey( EntityAnimationConstants.ENTITY_ANIMATION_JUMPING, EntityDataConstants.ENTITY_DATA_ANIMATION_STATE );
			}
		}
	}
}
