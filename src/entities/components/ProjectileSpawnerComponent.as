/**
 * Created by eVillain on 01/10/14.
 */
package entities.components
{

	import audio.constants.AudioConstants;
	import audio.signals.PlayAudioSignal;
	import audio.vos.PlayAudioVO;

	import entities.Entity;
	import entities.constants.ComponentConstants;
	import entities.constants.EntityDataConstants;
	import entities.signals.CreateProjectileSignal;
	import entities.vos.CreateProjectileVO;

	import flash.geom.Point;

	import particles.constants.ParticleConstants;
	import particles.signals.AddParticlesSignal;
	import particles.vos.AddParticleSystemVO;

	public class ProjectileSpawnerComponent extends BaseComponent
	{
		[Inject]
		public var createProjectileSignal:CreateProjectileSignal;

		[Inject]
		public var addParticlesSignal:AddParticlesSignal;

		[Inject]
		public var playAudioSignal:PlayAudioSignal;

		public static const BULLET_VELOCITY:Number = 300.0;
		public static const TOLERANCE_GAP:Number = 4;

		override public function getType():String
		{
			return ComponentConstants.COMPONENT_PROJECTILE_SPAWNER;
		}

		override public function addEntity( entity:Entity ):void
		{
			super.addEntity( entity );
		}

		override public function execute( entity:Entity, deltaTime:Number ):void
		{
			var trigger:Boolean = entity.getDataForKey( EntityDataConstants.ENTITY_DATA_ITEM_TRIGGER ) as Boolean;
			if (trigger)
			{
				// The trigger is on, check time from last trigger
				var triggerRate:Number = entity.getDataForKey( EntityDataConstants.ENTITY_DATA_ITEM_TRIGGER_RATE ) as Number;
				var triggerAccumulator:Number = entity.getDataForKey( EntityDataConstants.ENTITY_DATA_ITEM_TRIGGER_ACCUMULATOR ) as Number;

				if (triggerAccumulator == 0.0)
				{
					fire( entity );
				}

				triggerAccumulator += deltaTime;

				if (triggerAccumulator >= triggerRate)
				{
					triggerAccumulator = 0.0;
				}

				entity.setDataForKey( triggerAccumulator, EntityDataConstants.ENTITY_DATA_ITEM_TRIGGER_ACCUMULATOR );
			}
			else
			{
				entity.setDataForKey( 0.0, EntityDataConstants.ENTITY_DATA_ITEM_TRIGGER_ACCUMULATOR );
			}
		}

		private function fire( entity:Entity ):void
		{
			var position:Point = entity.getDataForKey( EntityDataConstants.ENTITY_DATA_POSITION ) as Point;
			var velocity:Point = entity.getDataForKey( EntityDataConstants.ENTITY_DATA_VELOCITY ) as Point;
			var angle:Number = entity.getDataForKey( EntityDataConstants.ENTITY_DATA_ANGLE ) as Number;
			var projectileType:String = entity.getDataForKey( EntityDataConstants.ENTITY_DATA_ITEM_PROJECTILE_TYPE ) as String;
			var projectileAmount:uint = entity.getDataForKey( EntityDataConstants.ENTITY_DATA_ITEM_PROJECTILE_AMOUNT ) as uint;
			var projectileOffset:Point = entity.getDataForKey( EntityDataConstants.ENTITY_DATA_ITEM_PROJECTILE_OFFSET ) as Point;

			var projectileDirectionX:Number;
			var projectileDirectionY:Number;
			var projectilePosition:Point;
			var projectileVelocity:Point;

			// Add some fancy muzzle flash to our gun
			addParticlesSignal.dispatch( new AddParticleSystemVO( ParticleConstants.PARTICLES_MUZZLE_FLASH, position.x, position.y, projectileOffset.x, projectileOffset.y, angle ) );

			playAudioSignal.dispatch( new PlayAudioVO( AudioConstants.SOUND_PISTOL ) );

			if (projectileAmount == 1)
			{
				projectileDirectionX = Math.cos( angle );
				projectileDirectionY = Math.sin( angle );
				projectilePosition = new Point( position.x + projectileDirectionX * TOLERANCE_GAP, position.y + projectileDirectionY * TOLERANCE_GAP );
				projectileVelocity = new Point( velocity.x + projectileDirectionX * BULLET_VELOCITY, velocity.y + projectileDirectionY * BULLET_VELOCITY );

				createProjectileSignal.dispatch( new CreateProjectileVO( projectileType, projectilePosition, projectileVelocity, angle ) );
			}
			else
			{
				for (var p:uint = 0; p < projectileAmount; p++)
				{
					var fudge:Number = Math.random() * 0.1;
					projectileDirectionX = Math.cos( angle + fudge );
					projectileDirectionY = Math.sin( angle + fudge );
					projectilePosition = new Point( position.x + projectileDirectionX * TOLERANCE_GAP, position.y + projectileDirectionY * TOLERANCE_GAP );
					projectileVelocity = new Point( velocity.x + projectileDirectionX * BULLET_VELOCITY, velocity.y + projectileDirectionY * BULLET_VELOCITY );

					createProjectileSignal.dispatch( new CreateProjectileVO( projectileType, projectilePosition, projectileVelocity, angle ) );
				}
			}
		}
	}
}
