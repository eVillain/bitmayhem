/**
 * Created by eVillain on 31/08/14.
 *
 * These components contain the logic for our entities.
 * Each component registers a set of entities
 * and performs some manipulations on it's data
 */
package entities.components
{

	import entities.Entity;

	public interface IComponent
	{
		function getType():String;

		function addEntity( entity:Entity ):void;

		function removeEntity( entityID:uint ):void;

		function update( deltaTime:Number ):void;

		function execute( entity:Entity, deltaTime:Number ):void;
	}
}
