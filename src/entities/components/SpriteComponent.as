/**
 * Created by eVillain on 01/09/14.
 */
package entities.components
{

	import entities.Entity;
	import entities.constants.ComponentConstants;
	import entities.constants.EntityDataConstants;

	import flash.geom.Point;

	import physics.model.PhysicsModel;

	import starling.display.Sprite;

	public class SpriteComponent extends BaseComponent
	{
		[Inject]
		public var physicsModel:PhysicsModel;

		static const PREDICT_MOVEMENT:Boolean = true;

		override public function getType():String
		{
			return ComponentConstants.COMPONENT_SPRITE;
		}

		override public function execute( entity:Entity, deltaTime:Number ):void
		{
			// Get Sprite instance, position and angle
			var sprite:Sprite = entity.getDataForKey( EntityDataConstants.ENTITY_DATA_SPRITE ) as Sprite;
			var position:Point = entity.getDataForKey( EntityDataConstants.ENTITY_DATA_POSITION ) as Point;
			var velocity:Point = entity.getDataForKey( EntityDataConstants.ENTITY_DATA_VELOCITY ) as Point;
			var angle:Number = entity.getDataForKey( EntityDataConstants.ENTITY_DATA_ANGLE ) as Number;

			if (PREDICT_MOVEMENT)
			{
				// Calculate new (predicted) position based on time accumulated in physics model
				var predictedMovement:Point = new Point( velocity.x * physicsModel.timeAccumulator, velocity.y * physicsModel.timeAccumulator );

				// Update sprite with new values
				sprite.x = position.x + predictedMovement.x;
				sprite.y = position.y + predictedMovement.y;
			}
			else
			{
				// Update sprite with new values
				sprite.x = position.x;
				sprite.y = position.y;
			}

			sprite.rotation = angle;
		}
	}
}
