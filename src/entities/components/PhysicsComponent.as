/**
 * Created by eVillain on 31/08/14.
 */
package entities.components
{

	import entities.Entity;
	import entities.constants.ComponentConstants;
	import entities.constants.EntityDataConstants;

	import flash.geom.Point;

	import nape.geom.Vec2;
	import nape.phys.Body;

	import physics.model.PhysicsModel;

	public class PhysicsComponent extends BaseComponent
	{
		[Inject]
		public var physicsModel:PhysicsModel;

		override public function getType():String
		{
			return ComponentConstants.COMPONENT_PHYSICS;
		}

		override public function addEntity( entity:Entity ):void
		{
			if (!entity.hasDataObjectForKey( EntityDataConstants.ENTITY_DATA_PHYSICS_BODY ))
			{
				var entityID:uint = entity.getDataForKey( EntityDataConstants.ENTITY_DATA_ID ) as uint;
				trace( "PhysicsComponent ERROR adding entity " + entityID.toString() + ", it has no physics body!" );
				return;
			}

			super.addEntity( entity );
		}

		override public function update( deltaTime:Number ):void
		{
			if (!physicsModel.updated)
			{
				return;
			}

			for each (var entity:Entity in _entities)
			{
				execute( entity, deltaTime );
			}
		}

		override public function execute( entity:Entity, deltaTime:Number ):void
		{
			// Get physics body and values
			var body:Body = entity.getDataForKey( EntityDataConstants.ENTITY_DATA_PHYSICS_BODY ) as Body;
			var position:Vec2 = body.position;
			var angle:Number = body.rotation;
			var velocity:Vec2 = body.velocity;

			// Get contacts - this is here just as a reference in case we want to do something with it later...
			//var contactSet:ContactSet = entity.getDataForKey( EntityDataConstants.ENTITY_DATA_PHYSICS_CONTACTS ) as ContactSet;

			// Calculate new (predicted) position based on time accumulated in physics model
			//var predictedPosition:Point = new Point( position.x + velocity.x * deltaTime, position.y + velocity.y * deltaTime );
			var predictedPosition:Point = new Point( position.x, position.y );
			//  Save predicted position, angle and velocity
			entity.setDataForKey( predictedPosition, EntityDataConstants.ENTITY_DATA_POSITION );
			entity.setDataForKey( angle, EntityDataConstants.ENTITY_DATA_ANGLE );
			entity.setDataForKey( new Point( velocity.x, velocity.y ), EntityDataConstants.ENTITY_DATA_VELOCITY );

			//trace( "PhysicsComponent pos: " + position.x + ", " + position.y + " angle: " + angle + " vel: " + velocity.x + ", " + velocity.y )
		}
	}
}
