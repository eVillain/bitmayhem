/**
 * Created by eVillain on 02/09/14.
 */
package entities.components
{

	import entities.Entity;
	import entities.constants.EntityDataConstants;

	import flash.utils.Dictionary;

	public class BaseComponent implements IComponent
	{
		protected var _entities:Dictionary = new Dictionary();

		public function getType():String
		{
			return "OVERRIDE_ME!";
		}

		public function addEntity( entity:Entity ):void
		{
			var entityID:uint = entity.getDataForKey( EntityDataConstants.ENTITY_DATA_ID ) as uint;
			_entities[entityID] = entity;
		}

		public function removeEntity( entityID:uint ):void
		{
			if (_entities[entityID])
			{
				delete _entities[entityID];
			}
		}

		public function update( deltaTime:Number ):void
		{
			for each (var entity:Entity in _entities)
			{
				execute( entity, deltaTime );
			}
		}

		public function execute( entity:Entity, deltaTime:Number ):void
		{
			// Take the entity data and process it here
			trace( "OVERRIDE ME! BaseComponent executed..." );
		}

		public function get entities():Dictionary
		{
			return _entities;
		}
	}
}
