/**
 * Created by eVillain on 01/10/14.
 */
package entities.components
{

	import entities.Entity;
	import entities.constants.ComponentConstants;
	import entities.constants.EntityDataConstants;
	import entities.signals.DestroyEntitySignal;

	import flash.geom.Point;

	import nape.phys.Body;

	import particles.constants.ParticleConstants;
	import particles.signals.AddParticlesSignal;
	import particles.vos.AddParticleSystemVO;

	import physics.contacts.ContactSet;

	public class ProjectileComponent extends BaseComponent
	{
		[Inject]
		public var destroyEntitySignal:DestroyEntitySignal;

		[Inject]
		public var addParticlesSignal:AddParticlesSignal;

		override public function getType():String
		{
			return ComponentConstants.COMPONENT_PROJECTILE;
		}

		override public function execute( entity:Entity, deltaTime:Number ):void
		{
			var lifeTime:Number = entity.getDataForKey( EntityDataConstants.ENTITY_DATA_LIFETIME ) as Number;
			lifeTime -= deltaTime;

			if (lifeTime <= 0.0)
			{
				// Entity has expired
				var entityID:uint = entity.getDataForKey( EntityDataConstants.ENTITY_DATA_ID ) as uint;
				destroyEntitySignal.dispatch( entityID );
			}
			else
			{
				entity.setDataForKey( lifeTime, EntityDataConstants.ENTITY_DATA_LIFETIME );
				checkContacts( entity );
			}
		}

		private function checkContacts( entity:Entity ):void
		{
			// This should probably be smarter but we'll just destroy the bullet on impact for now
			var contactSet:ContactSet = entity.getDataForKey( EntityDataConstants.ENTITY_DATA_PHYSICS_CONTACTS ) as ContactSet;
			if (contactSet.numContacts > 0)
			{
				var gotDestroyed:Boolean = false;

				for each(var body:Body in contactSet.contacts)
				{
					if (body.userData.data)
					{
						var contactEntity:Entity = body.userData.data as Entity;

						if (contactEntity.hasDataObjectForKey( EntityDataConstants.ENTITY_DATA_HEALTH ))
						{
							// Remove health from collided object
							var health:uint = contactEntity.getDataForKey( EntityDataConstants.ENTITY_DATA_HEALTH ) as uint;
							var position:Point = entity.getDataForKey( EntityDataConstants.ENTITY_DATA_POSITION ) as Point;
							var direction:Point = entity.getDataForKey( EntityDataConstants.ENTITY_DATA_VELOCITY ) as Point;

							var angle:Number = Math.atan2( direction.x, -direction.y );
							//var angle:Number = entity.getDataForKey( EntityDataConstants.ENTITY_DATA_ANGLE ) as Number;

							health = health >= 10 ? health - 10 : 0; // 10 damage for now, will come from dataBucket
							contactEntity.setDataForKey( health, EntityDataConstants.ENTITY_DATA_HEALTH );
							// Bullet should be destroyed
							gotDestroyed = true;
							//Add sparks or blood depending on type of entity we hit
							addParticlesSignal.dispatch( new AddParticleSystemVO( ParticleConstants.PARTICLES_BLOOD, position.x, position.y, 0.0, 0.0, angle ) );
						}
					}
					else
					{
						// Bullet should be destroyed, hit a static object
						gotDestroyed = true;
						// Add sparks
						var position:Point = entity.getDataForKey( EntityDataConstants.ENTITY_DATA_POSITION ) as Point;
						addParticlesSignal.dispatch( new AddParticleSystemVO( ParticleConstants.PARTICLES_SPARKS, position.x, position.y ) );
					}

					if (gotDestroyed)
					{
						var entityID:uint = entity.getDataForKey( EntityDataConstants.ENTITY_DATA_ID ) as uint;
						destroyEntitySignal.dispatch( entityID );
						return;
					}
				}
			}
		}
	}
}
