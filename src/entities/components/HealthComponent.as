/**
 * Created by eVillain on 12/10/14.
 */
package entities.components
{

	import entities.Entity;
	import entities.constants.ComponentConstants;
	import entities.constants.EntityDataConstants;
	import entities.signals.DestroyEntitySignal;

	public class HealthComponent extends BaseComponent
	{
		[Inject]
		public var destroyEntitySignal:DestroyEntitySignal;

		override public function getType():String
		{
			return ComponentConstants.COMPONENT_HEALTH;
		}

		override public function execute( entity:Entity, deltaTime:Number ):void
		{
			var health:uint = entity.getDataForKey( EntityDataConstants.ENTITY_DATA_HEALTH ) as uint;

			if (health == 0)
			{
				// Entity has died
				var entityID:uint = entity.getDataForKey( EntityDataConstants.ENTITY_DATA_ID ) as uint;
				destroyEntitySignal.dispatch( entityID );
			}
		}
	}
}
