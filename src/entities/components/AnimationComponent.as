/**
 * Created by eVillain on 09/09/14.
 */
package entities.components
{

	import entities.Entity;
	import entities.constants.ComponentConstants;
	import entities.constants.EntityDataConstants;

	import flash.utils.Dictionary;

	import starling.core.Starling;
	import starling.display.MovieClip;
	import starling.display.Sprite;

	public class AnimationComponent extends BaseComponent
	{
		override public function getType():String
		{
			return ComponentConstants.COMPONENT_ANIMATION;
		}

		override public function addEntity( entity:Entity ):void
		{
			super.addEntity( entity );
		}

		override public function execute( entity:Entity, deltaTime:Number ):void
		{
			var previousState:uint = entity.getDataForKey( EntityDataConstants.ENTITY_DATA_ANIMATION_PREVIOUS ) as uint;
			var currentState:uint = entity.getDataForKey( EntityDataConstants.ENTITY_DATA_ANIMATION_STATE ) as uint;
			var animations:Dictionary = entity.getDataForKey( EntityDataConstants.ENTITY_DATA_ANIMATION_SET ) as Dictionary;

			var currentAnimation:MovieClip = animations[currentState];

			if (previousState != currentState)
			{
				// Save current state as previous to prevent further updates until a change
				entity.setDataForKey( currentState, EntityDataConstants.ENTITY_DATA_ANIMATION_PREVIOUS );

				var sprite:Sprite = entity.getDataForKey( EntityDataConstants.ENTITY_DATA_SPRITE ) as Sprite;

				// Check for previous animation to remove first
				var previousAnimation:MovieClip = animations[previousState];
				if (previousAnimation)
				{
					Starling.juggler.remove( previousAnimation );
					sprite.removeChild( previousAnimation );
					//trace( "AnimationComponent: ANIMATION REMOVED " + previousState );
				}

				if (currentAnimation)
				{
					// Apply current animation
					Starling.juggler.add( currentAnimation );
					sprite.addChild( currentAnimation );
					//trace( "AnimationComponent: ANIMATION SWITCHED TO " + currentState );
				}
			}

			if (currentAnimation && entity.hasDataObjectForKey( EntityDataConstants.ENTITY_DATA_ANIMATION_SPEED ))
			{
				currentAnimation.fps = entity.getDataForKey( EntityDataConstants.ENTITY_DATA_ANIMATION_SPEED ) as Number;
			}
		}
	}
}
