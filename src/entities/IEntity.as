/**
 * Created by eVillain on 31/08/14.
 */
package entities
{

	public interface IEntity
	{
		function hasDataObjectForKey( key:String ):Boolean;

		function getDataForKey( key:String ):Object;

		function setDataForKey( data:Object, key:String ):void;

		function getID():uint;
	}
}
