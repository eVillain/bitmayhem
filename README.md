Dependencies:

Flex SDK 4.6.0.23201
http://sourceforge.net/adobe/flexsdk/wiki/Downloads/

Adobe AIR 15.0.0.249 SDK & Compiler
http://helpx.adobe.com/air/kb/archived-air-sdk-version.html

Robotlegs v2.2.1
https://github.com/robotlegs/robotlegs-framework

AS3-Signals v0.9-beta
https://github.com/robertpenner/as3-signals

SignalCommandMap
https://github.com/joelhooks/signals-extensions-CommandSignal

Robotlegs Starling View Map
https://github.com/jamieowen/robotlegs2-starling-viewmap

Macrobot
https://github.com/alebianco/robotlegs-utilities-macrobot

Starling v1.5.1
http://gamua.com/starling/download/

Feathers v2.0.0
http://feathersui.com

Nape Physics v2.0.16
http://napephys.com/downloads.html

Greensock-AS3
https://github.com/greensock/GreenSock-AS3

Starling Particle System Extension
https://github.com/Gamua/Starling-Extension-Particle-System